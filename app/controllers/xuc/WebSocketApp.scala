package controllers.xuc

import com.fasterxml.jackson.databind.JsonNode
import models.{User, XivoUser, XucUser}
import play.api.Play.current
import play.api.libs.json.JsValue
import play.api.mvc.WebSocket.FrameFormatter
import play.api.mvc.{Action, Controller, WebSocket}
import play.api.{Logger, Routes}
import play.libs.Json
import services.config.ConfigRepository
import xivo.websocket.WsActor

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

object WebSocketApp extends Controller {
  val log = Logger(getClass.getName)

  val stringFrame: FrameFormatter[String] = play.core.server.websocket.Frames.textFrame
  implicit val jsonFrame: FrameFormatter[JsonNode] = stringFrame.transform(Json.stringify, Json.parse)

  def javascriptChannel = Action { implicit request =>
    Ok(Routes.javascriptRouter("xucRoutes")(routes.javascript.WebSocketApp.ctiChannel)).as("text/javascript")
  }

  def ctiChannel(username: String, phoneNumber: Int, password: String) = WebSocket.tryAcceptWithActor[JsValue, JsonNode] { implicit request =>
    log.info(s"[$username] websocket requested")
    username match {
      case "" =>
        log.info(s"Cannot create cti websocket username empty from host ${request.remoteAddress}")
        Future.successful(Left(Forbidden("Username cannot be empty")))
      case username =>
          ConfigRepository.repo.getUser(username, password) match {
            case Some(xivoUser) => log.info(s"[$username] internal repo authentication success.")
              WsActor.props(getXucUser(xivoUser, phoneNumber)).map(f => Right(f))

            case None =>
              try {
              User.authenticate(username, password) match {
                case Some(user) =>
                  log.info(s"[$username] authentication success.")
                  ConfigRepository.repo.getUser(username) match {
                    case Some(xivoUser) => WsActor.props(getXucUser(xivoUser, phoneNumber)).map(f => Right(f))

                    case None => Future.successful(Left(Forbidden(s"[$username] Authentication failed, please check your identifiers")))
                  }
                case None =>
                  log.info(s"[$username] authentication failed.")
                  Future.successful(Left(Forbidden(s"[$username] Authentication failed, please check your identifiers")))
              }
          } catch {
          case e: Exception =>
            log.error(s"[$username] Unexpected authentication error ${e.getMessage}")
            Future.successful(Left(InternalServerError(s"[$username] Authentication error, please contact your administrator")))
          }
        }
    }
  }
  private def getXucUser(xivoUser:XivoUser, phoneNumber: Int) =
    if (phoneNumber == 0)  XucUser(xivoUser.username, xivoUser.password.getOrElse(""))
    else  XucUser(xivoUser.username, xivoUser.password.getOrElse(""), Some(phoneNumber))
}