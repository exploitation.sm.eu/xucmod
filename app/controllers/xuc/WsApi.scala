package controllers.xuc

import akka.util.Timeout.durationToTimeout
import play.api.Logger
import play.api.libs.concurrent.Execution.Implicits.defaultContext
import play.api.libs.json.Reads._
import play.api.libs.json.Writes._
import play.api.libs.json._
import play.api.mvc._
import services.config.ConfigRepository
import services.request.AgentLoginRequest
import xivo.xuc.api.{CtiApi, RequestError, RequestResult, RequestSuccess, RequestTimeout, Requester}
import play.api.libs.functional.syntax._

import scala.concurrent.Future
import scala.concurrent.duration.DurationInt

object WsApi extends Controller {
  val log = Logger(getClass.getName)
  implicit val timeout: akka.util.Timeout = 1 seconds

  def handShake(domain: String) = Action {
    implicit request =>
      log.info(s"handShake domain $domain ")
      Requester.handShake
      Ok("")
  }

  val lgfs = ((__ \ 'password).format[String])

  def connect(domain: String, username: String) = Action.async {
    implicit request =>
      log.info(s"connect $username domain $domain " + request.body.asJson)
      request.body.asJson.map { json =>
        json.validate[String](lgfs).map {
          case password: String =>
            log.debug(s"Connecting user : username")
            val result = Requester.connect(username, password)
            processResult(result, username)
        }.recoverTotal {
          e =>
            log.error(s"Error connect " + JsError.toFlatJson(e))
            Future(BadRequest(s"Error connect " + JsError.toFlatJson(e)))
        }
      }.getOrElse {
        log.error(s"Error Error connect Expecting Json data")
        Future(BadRequest(s"Error Error connect Expecting Json data"))
      }

  }
  def isForMe(username:String) = ConfigRepository.repo.getUser(username).isDefined

  case class UserRequest[A](username: String, request: Request[A]) extends WrappedRequest(request)

  case class UserFromRequest(domain: String, username: String, action:String) extends ActionBuilder[UserRequest] {
    def invokeBlock[A](request: Request[A], block: UserRequest[A] => Future[Result]): Future[Result] = {
      if (isForMe(username)) {
        block(UserRequest(username, request))
      }
      else {
        ForwardRequester.forwardRequest(domain, username, action)(request)
      }
    }
  }

  val rds = ((__ \ 'number).format[String])

  def dial(domain: String, username: String) = UserFromRequest(domain, username, "dial").async {
    implicit request =>
      log.info(s"dial $username domain $domain ${request.headers.get("Peer")}" + request.body.asJson)
      request.body.asJson.map { json =>
        json.validate[String](rds).map (number => processResult(CtiApi.dial(username, number), username)
        ).recoverTotal {
          e =>
            log.error(s"Error $username dial " + JsError.toFlatJson(e))
            Future(BadRequest(s"Error $username dial " + JsError.toFlatJson(e)))
        }
      }.getOrElse {
        log.error(s"Error $username dial Expecting Json data")
        Future(BadRequest(s"Error $username dial Expecting Json data"))
      }
  }

  val usernameReads = (__ \ 'username).format[String]

  def dialByUsername(domain: String, username: String) = UserFromRequest(domain, username, "dialByUsername").async {
    implicit request =>
      log.info(s"dial $username domain $domain ${request.headers.get("Peer")}" + request.body.asJson)
      request.body.asJson.map { json =>
        json.validate[String](usernameReads).map(user2 => ConfigRepository.repo.phoneNumberForUser(user2) match {
          case Some(number) => processResult(CtiApi.dial(username, number), username)
          case None => Future(BadRequest(s"Phone number for $user2 not found"))
        }).recoverTotal {
          e =>
            log.error(s"Error $username dial " + JsError.toFlatJson(e))
            Future(BadRequest(s"Error $username dial " + JsError.toFlatJson(e)))
        }
      }.getOrElse {
        log.error(s"Error $username dial Expecting Json data")
        Future(BadRequest(s"Error $username dial Expecting Json data"))
      }
  }


  def togglePause() = Action.async {
    implicit request =>
      log.info(s"TogglePause " + request.body.asJson)
      request.body.asJson.map { json =>
        val phoneNumber: JsResult[String] = (json \ "phoneNumber").validate[String]
        phoneNumber.map {
          case nb =>
            Requester.togglePause(nb)
            Future(Ok("TogglePause"))
        }.recoverTotal {
          e =>
            log.error(s"Error togglePause" + JsError.toFlatJson(e))
            Future(BadRequest(s"Error TogglePause " + JsError.toFlatJson(e)))
        }
      }.getOrElse {
        log.error(s"Error togglePause Expecting Json data")
        Future(BadRequest(s"Error togglePause Expecting Json data"))
      }
  }
  def dnd(domain: String, username: String) = UserFromRequest(domain, username, "dnd").async {
    implicit request =>
      log.info(s"Dnd $username domain $domain " + request.body.asJson)
      request.body.asJson.map { json =>
        val dndState: JsResult[Boolean] = (json \ "state").validate[Boolean]
        dndState.map {
          case state =>
            val result = CtiApi.dnd(username, state)
            processResult(result, username)
        }.recoverTotal {
          e =>
            log.error(s"Error $username dnd " + JsError.toFlatJson(e))
            Future(BadRequest(s"Error $username dnd " + JsError.toFlatJson(e)))
        }
      }.getOrElse {
        log.error(s"Error $username dnd Expecting Json data")
        Future(BadRequest(s"Error $username dnd Expecting Json data"))
      }
  }

  val fwdf = (__ \ 'state).format[Boolean] and (__ \ 'destination).format[String] tupled

  def uncForward(domain: String, username: String) = forward(domain, username, "uncForward", CtiApi.uncForward)
  def naForward(domain: String, username: String) = forward(domain, username,  "naForward",CtiApi.naForward)
  def busyForward(domain: String, username: String) = forward(domain, username,  "busyForward",CtiApi.busyForward)

  def agentLogin() = Action.async {
    implicit request =>
      log.info(s"agentLogin " + request.body.asJson)
      val agentLoginReq = Json.fromJson[AgentLoginRequest](request.body.asJson.get)
      log.debug(s"decoded agentLoginRequest: $agentLoginReq")
      agentLoginReq match {
        case JsSuccess(request, _) =>
          processResult(Requester.agentLogin(request))
        case jer: JsError =>
          Future(BadRequest(s"Unable to decode json body: ${jer.errors}"))
      }
  }

  def agentLogout() = Action.async {
    implicit request =>
      log.info(s"agentLogout " + request.body.asJson)
      request.body.asJson.map { json =>
        val phoneNumber: JsResult[String] = (json \ "phoneNumber").validate[String]
        phoneNumber.map {
          case nb =>
            log.debug("agentLogout from phone: $nb")
            processResult(Requester.agentLogout(nb))
        }.recoverTotal {
          e =>
            log.error(s"Error agentLogout " + JsError.toFlatJson(e))
            Future(BadRequest(s"Error agentLogout " + JsError.toFlatJson(e)))
        }
      }.getOrElse {
        log.error(s"Error agentLogout Expecting Json data")
        Future(BadRequest(s"Error agentLogout Expecting Json data"))
      }
  }

  def getRichCallHistory(domain: String, username: String, size: Int) = Action.async {
    request =>
      log.info(s"Call history request for user $username and size $size")
      Requester.richCallHistory(username, size).map(history => Ok(Json.toJson(history))).recover({
        case e: Exception => log.error(s"Error getting call history for $username: $e")
          InternalServerError(s"Error getting call history for $username: $e")
      })
  }

  private def forward(domain: String, username: String, action:String, forwardRequest: (String, Boolean, String) => Future[RequestResult]) =  UserFromRequest(domain, username, action).async {
    implicit request =>
      log.info(s"forward $username " + request.body.asJson)

      request.body.asJson.map { json =>
        json.validate[(Boolean, String)](fwdf).map {
          case (state, destination) =>
            val result = forwardRequest(username, state, destination)
            processResult(result, username)
        }.recoverTotal {
          e =>
            log.error(s"Error $username forward " + JsError.toFlatJson(e))
            Future(BadRequest(s"Error $username forward " + JsError.toFlatJson(e)))
        }
      }.getOrElse {
        log.error(s"Error $username forward Expecting Json data")
        Future(BadRequest(s"Error $username forward Expecting Json data"))
      }
  }

  private def processResult(result: Future[RequestResult], username: String = ""): Future[Result] = {
    result.map {
      case r: RequestSuccess =>
        log.info(s"Process Result ok $username")
        Ok(r.reason)
      case t: RequestTimeout =>
        log.error(s"Process Result timeout $username")
        BadRequest(t.reason)
      case e: RequestError =>
        log.info(s"Process Result Bad Request(${e.reason}) $username")
        BadRequest(e.reason)
      case e =>
        log.error(s"Process Result Internal Server Error(${e.toString}) $username")
        InternalServerError("Unexpected")
    }
  }

}
