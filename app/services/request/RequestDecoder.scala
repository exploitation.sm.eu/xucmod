package services.request

import play.api.libs.json._
import services.BrowserMessage
import services.config.ConfigDispatcher._

class RequestDecoder {

  val decoders: Map[String, JsValue => JsResult[XucRequest]] = Map(
    "moveAgentsInGroup" -> MoveAgentInGroup.validate,
    "addAgentsInGroup" -> AddAgentInGroup.validate,
    "removeAgentGroupFromQueueGroup" -> RemoveAgentGroupFromQueueGroup.validate,
    "addAgentsNotInQueueFromGroupTo" -> AddAgentsNotInQueueFromGroupTo.validate,
    "listenAgent" -> AgentListen.validate,
    "subscribeToAgentEvents" -> SubscribeToAgentEvents.validate,
    "getConfig" -> GetConfig.validate,
    "getList" -> GetList.validate,
    "getAgentStates" -> GetAgentStates.validate,
    "getAgentDirectory" -> GetAgentDirectory.validate,
    "subscribeToQueueStats" -> SubscribeToQueueStats.validate,
    "subscribeToAgentStats" -> SubscribeToAgentStats.validate,
    "monitorPause" -> MonitorPause.validate,
    "monitorUnpause" -> MonitorUnpause.validate,
    "agentLogin" -> AgentLoginRequest.validate,
    "inviteConferenceRoom" -> InviteConferenceRoom.validate,
    "naFwd" -> NaForward.validate,
    "uncFwd" -> UncForward.validate,
    "busyFwd" -> BusyForward.validate,
    "subscribeToQueueCalls" -> SubscribeToQueueCalls.validate,
    "unSubscribeToQueueCalls" -> UnSubscribeToQueueCalls.validate,
    "getAgentCallHistory" -> GetAgentCallHistory.validate,
    "getUserCallHistory" -> GetUserCallHistory.validate,
    "setAgentGroup" -> SetAgentGroup.validate
  )

  def decodeCmd(message: JsValue): XucRequest = {
    message \ XucRequest.Cmd match {
      case JsString(cmd) if decoders.contains(cmd) =>
        decoders(cmd)(message) match {
          case s: JsSuccess[XucRequest] => s.get
          case e: JsError => InvalidRequest(s"${XucRequest.errors.validation} $e", message.toString())
        }
      case s: JsString => BrowserMessage(play.libs.Json.parse(message.toString()))
      case _ => InvalidRequest(XucRequest.errors.invalidNoCmd, message.toString())
    }
  }

  def decode(message: JsValue): XucRequest = {

    message \ XucRequest.ClassProp match {
      case JsString(XucRequest.PingClass) => Ping
      case JsString(XucRequest.WebClass) => decodeCmd(message)
      case _ => InvalidRequest(XucRequest.errors.invalid, message.toString())
    }
  }

}
