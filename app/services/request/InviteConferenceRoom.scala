package services.request

import play.api.libs.json.{Reads, JsValue, JsPath}

case class InviteConferenceRoom(userId: Int) extends XucRequest

object InviteConferenceRoom {
  def validate(json: JsValue) = json.validate[InviteConferenceRoom]

  implicit val reads: Reads[InviteConferenceRoom] = (JsPath \ "userId").read[Int].map(id => InviteConferenceRoom(id))
}