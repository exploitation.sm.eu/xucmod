package services.request

import services.XucAmiBus.{AmiMessage, SetVarActionRequest, SetVarRequest}
import services.request.PhoneKey.PhoneKey

object PhoneKey extends Enumeration {
  type PhoneKey = Value
  val Pause = Value
  val Logon = Value
}

object  KeyLightRequest {
  val LogonPrefix = "30"
  val PausePrefix = "34"
  val AmiVarSuffix = "DEVICE_STATE(Custom:***"
  val AmiLightOn = "INUSE"
  val AmiLightOff = "NOT_INUSE"
}

abstract class KeyLightRequest extends  XucRequest {
  def toAmi: AmiMessage
}


case class TurnOffKeyLight(phoneNb: String, key: PhoneKey) extends KeyLightRequest {
  import KeyLightRequest._
  def toAmi = key match {
    case PhoneKey.Pause => SetVarRequest (SetVarActionRequest (s"$AmiVarSuffix$PausePrefix$phoneNb)", AmiLightOff) )
    case PhoneKey.Logon => SetVarRequest (SetVarActionRequest (s"$AmiVarSuffix$LogonPrefix$phoneNb)", AmiLightOff) )
  }
}


case class TurnOnKeyLight(phoneNb: String, key: PhoneKey) extends KeyLightRequest {
  import KeyLightRequest._
  def toAmi = key match {
    case PhoneKey.Pause => SetVarRequest (SetVarActionRequest (s"$AmiVarSuffix$PausePrefix$phoneNb)", AmiLightOn) )
    case PhoneKey.Logon => SetVarRequest (SetVarActionRequest (s"$AmiVarSuffix$LogonPrefix$phoneNb)", AmiLightOn) )
  }
}
