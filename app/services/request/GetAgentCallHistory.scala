package services.request

import play.api.libs.json.{JsPath, Reads, JsValue}

case class GetAgentCallHistory(size: Int) extends XucRequest
case class AgentCallHistoryRequest(size: Int, ctiUserName: String) extends XucRequest

object GetAgentCallHistory {
  def validate(json: JsValue) = json.validate[GetAgentCallHistory]

  implicit val reads: Reads[GetAgentCallHistory] = (JsPath \ "size").read[Int].map(GetAgentCallHistory.apply)
}
