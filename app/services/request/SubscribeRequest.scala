package services.request

import play.api.libs.json._

class SubscribeRequest extends  XucRequest

case object SubscribeToAgentEvents extends SubscribeRequest {
  def validate(json: JsValue) = JsSuccess(SubscribeToAgentEvents)
}

case object SubscribeToQueueStats extends SubscribeRequest {
  def validate(json: JsValue) = JsSuccess(SubscribeToQueueStats)
}

case object SubscribeToAgentStats extends SubscribeRequest {
  def validate(json: JsValue) = JsSuccess(SubscribeToAgentStats)
}

case class SubscribeToQueueCalls(queueId: Long) extends SubscribeRequest

object SubscribeToQueueCalls {
  def validate(json: JsValue) = json.validate[SubscribeToQueueCalls]
  implicit val read : Reads[SubscribeToQueueCalls] = (JsPath \ "queueId").read[Long].map(SubscribeToQueueCalls.apply)
}

case class UnSubscribeToQueueCalls(queueId: Long) extends SubscribeRequest

object UnSubscribeToQueueCalls {
  def validate(json: JsValue) = json.validate[UnSubscribeToQueueCalls]
  implicit val read : Reads[UnSubscribeToQueueCalls] = (JsPath \ "queueId").read[Long].map(UnSubscribeToQueueCalls.apply)
}