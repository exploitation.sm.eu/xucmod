package services

import org.xivo.cti.message.UserStatusUpdate
import play.api.Logger
import play.api.libs.ws.WS
import play.api.libs.ws.WSResponse
import scala.concurrent.Future
import play.api.libs.ws.WSRequestHolder
import play.api.libs.json.Json
import org.xivo.cti.message.PhoneStatusUpdate
import xivo.xuc.XucConfig
import services.config.UserPhoneStatus
import play.api.Play.current

object StatusPublisher {
  val log = Logger(getClass.getName)

  def publishStatus(status: UserPhoneStatus) {
    val url = XucConfig.EVENT_URL
    val holder: WSRequestHolder = WS.url(url)

    log.debug("publishing status " + status.username + " status " + status.status.getHintStatus() + " to " + url)
    if (url != "") {
      try {
        val data = Json.obj(
          "username" -> status.username,
          "status" -> status.status.getHintStatus().toInt)
        val futureResponse: Future[WSResponse] = WS.url(url).post(data)
      } catch {
        case e: Throwable => log.error("Unable to send status " + e.getMessage)
      }
    }
    else {
      log.debug("no Url configured to publish user configuration")
    }
  }

}