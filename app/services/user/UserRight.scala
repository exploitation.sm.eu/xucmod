package services.user

import org.slf4j.LoggerFactory
import play.api.Play.current
import play.api.libs.functional.syntax._
import play.api.libs.json._
import play.api.libs.ws.WS
import services.XucStatsEventBus.AggregatedStatEvent
import services.agent.AgentStatistic
import services.config.ConfigDispatcher.{AgentList, AgentGroupList, AgentQueueMemberList, QueueList}
import services.config.ConfigRepository
import xivo.events.AgentState
import xivo.models.XivoObject.ObjectType
import xivo.xuc.XucConfig

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

abstract class UserRight {
  def filter(o: Any): Option[Any]
}
case class AdminRight() extends UserRight {
  override def filter(o: Any): Option[Any] = Some(o)
}
case class SupervisorRight(queueIds: List[Long], groupIds: List[Long]) extends UserRight {
  override def filter(o: Any): Option[Any] = o match {
    case ql: QueueList => filter(ql)
    case qmList: AgentQueueMemberList => filter(qmList)
    case stat: AggregatedStatEvent => filter(stat)
    case state: AgentState => filter(state)
    case stat: AgentStatistic => filter(stat)
    case list: AgentGroupList => filter(list)
    case list: AgentList => filter(list)
    case o => Some(o)
  }

  def filter(queueList: QueueList): Option[QueueList] = Some(QueueList(queueList.queues.filter(q => queueIds.contains(q.getId))))

  def filter(qmList: AgentQueueMemberList): Option[AgentQueueMemberList] = Some(AgentQueueMemberList(
    qmList.agentQueueMembers.filter(qm => queueIds.contains(qm.queueId)).filter(qm => hasRightsOnAgent(qm.agentId))))

  def filter(stat: AggregatedStatEvent): Option[AggregatedStatEvent] = stat.xivoObject.objectType match {
    case ObjectType.Queue => if(stat.xivoObject.objectRef.isDefined && queueIds.contains(stat.xivoObject.objectRef.get)) Some(stat)
      else None
    case _ => Some(stat)
  }

  private def hasRightsOnAgent(agentId: Long): Boolean = {
    val groupId = ConfigRepository.repo.groupIdForAgent(agentId)
    groupId.isDefined && groupIds.contains(groupId.get)
  }

  def filter(state: AgentState): Option[AgentState] = {
    if(hasRightsOnAgent(state.agentId)) Some(state)
    else None
  }

  def filter(stat: AgentStatistic): Option[AgentStatistic] = if(hasRightsOnAgent(stat.agentId)) Some(stat) else None

  def filter(list: AgentGroupList): Option[AgentGroupList] = Some(AgentGroupList(list.agentGroups.filter(g => groupIds.contains(g.id.get))))

  def filter(list: AgentList): Option[AgentList] = Some(AgentList(list.agents.filter(a => groupIds.contains(a.groupId))))
}

object UserRight {

  val logger = LoggerFactory.getLogger(getClass)

  private val EmptySupervisorRitghts = SupervisorRight(List(), List())

  def validate(json: JsValue): JsResult[UserRight] = (json \ "type").asOpt[String] match {
    case None => JsError("Missing attribute 'type'")
    case Some("admin") => JsSuccess(AdminRight())
    case Some("supervisor") => (json \ "data").validate[SupervisorRight]
    case Some(s) => JsError(s"Unknown right $s")
  }


  def getRights(username: String): Future[UserRight] = {
    if(XucConfig.rightsHost == "" || XucConfig.rightsPort == 0) {
      logger.info(s"No right server configured, giving admin rights to user $username")
      Future.successful(AdminRight())
    }
    else
      WS.url(s"http://${XucConfig.rightsHost}:${XucConfig.rightsPort}/rights/user/$username").get()
      .map(response => response.status match {
          case 200 => UserRight.validate(response.json) match {
            case JsSuccess(right, _) => logger.info(s"Giving right $right to user $username")
              right
            case JsError(e) =>
              logger.error(s"Unable to decode right management server answer",e)
              EmptySupervisorRitghts
          }
          case 404 => logger.info(s"User $username not found on rights server, giving empty supervision rights")
            EmptySupervisorRitghts
          case s => logger.error(s"Error status $s received from right server with message ${response.body}")
            EmptySupervisorRitghts
        }
      ).recover(
        {
          case e: Exception =>
            logger.error(s"Unable to contact right management server",e)
            EmptySupervisorRitghts
        })
  }

  implicit val supervisorReads = ((JsPath \ "queueIds").read[List[Long]] and
    (JsPath \ "groupIds").read[List[Long]])(SupervisorRight.apply _)
}
