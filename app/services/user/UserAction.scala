package services.user

import services.CtiRouter
import services.request.{BusyForward, NaForward, UncForward, ForwardRequest}

trait UserAction {
  def forward(request: ForwardRequest)
}

trait CtiUserAction extends UserAction {
  this:CtiRouter =>

  def forward(request: ForwardRequest) = {
    request match {
      case UncForward(destination, state)   => ctiLink ! messageFactory.createUnconditionnalForward(destination, state)
      case NaForward(destination, state)    => ctiLink ! messageFactory.createNaForward(destination, state)
      case BusyForward(destination, state)  => ctiLink ! messageFactory.createBusyForward(destination, state)
      case _  =>
    }

  }
}
