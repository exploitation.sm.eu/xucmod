package services.agent

import org.joda.time.{Interval, DateTime}
import play.api.libs.json.Writes
import services.AgentStateFSM._
import xivo.events.AgentState
import xivo.events.AgentState.CallDirection.CallDirection
import xivo.events.AgentState._
import play.api.libs.json._

abstract trait AgentStatCalculator{
  val name: String
  val collector: StatCollector

  def reset()
}

trait AgentStatCalculatorByEvent extends AgentStatCalculator{
  def processEvent(state: AgentState)
}

trait AgentStatCalculatorByTransition extends AgentStatCalculator{
  def processTransition(t: AgentTransition)
}


trait AgentStatCalculatorByStatValue extends AgentStatCalculator {
  def processStat(stats: AgentStatistic)
}

sealed trait StatValue
object StatValue {
  implicit val baseWrites: Writes[StatValue] =
    new Writes[StatValue]{
      def writes(o: StatValue): JsValue = o match {
        case s: StatDateTime => StatDateTime.writes.writes(s)
        case s: StatPeriod => StatPeriod.writes.writes(s)
        case s: StatTotal => StatTotal.writes.writes(s)
        case s: StatAverage => StatAverage.writes.writes(s)
      }
    }
}

case class StatDateTime(value: DateTime) extends StatValue
object StatDateTime {
  val writes = new Writes[StatDateTime] {
    def writes(c: StatDateTime): JsValue = {Json.toJson(c.value.toString())}
  }
}
case class StatPeriod(value: Long) extends StatValue
object StatPeriod {
  val writes = new Writes[StatPeriod] {
    def writes(c: StatPeriod): JsValue = {Json.toJson(c.value)}
  }
}

case class StatTotal(value: Long) extends StatValue
object StatTotal {
  val writes = new Writes[StatTotal] {
    def writes(c: StatTotal): JsValue = {Json.toJson(c.value)}
  }
}

case class StatAverage(value: Double) extends StatValue
object StatAverage {
  val writes = new Writes[StatAverage] {
    def writes(c: StatAverage): JsValue = {Json.toJson(c.value)}
  }
}

object LoginDateTime { val name = "LoginDateTime" }
case class LoginDateTime(override val name: String = LoginDateTime.name, collector: StatCollector) extends AgentStatCalculatorByEvent {
  override def processEvent(state: AgentState) = state match {
    case AgentLogin(_, dateTime,_,_,_) => collector.onStatCalculated(name, StatDateTime(dateTime))
    case _ =>
  }
  override def reset() = {}
}

object LogoutDateTime { val name = "LogoutDateTime" }
case class LogoutDateTime(override val name: String = LogoutDateTime.name, collector: StatCollector) extends AgentStatCalculatorByEvent {

  override def processEvent(state: AgentState) = state match {
    case AgentLoggedOut(_, dateTime,_,_,_) => collector.onStatCalculated(name, StatDateTime(dateTime))
    case _ =>
  }
  override def reset() = {}
}

trait TimeProvider{
  def getTime:DateTime
}
trait JodaTimeProvider extends TimeProvider {
  def getTime = new DateTime
}
trait AgentTotalTime extends TimeProvider {
  this: AgentStatCalculator with TimeProvider =>

  var period: Long = 0
  private var startTime: Option[DateTime] = None

  override def reset() = {
    period = 0
    startTime = startTime.map(_ => Some(getTime)).flatten
    collector.onStatCalculated(name, StatPeriod(0))
  }

  private[agent] def start(dateTime: DateTime) = startTime match {
    case None => startTime = Some(dateTime)
    case _ =>
  }

  private[agent] def stopAndPublish() =   {
    startTime match {
      case Some(time) =>
        collector.onStatCalculated(name, StatPeriod(accumulate))
        startTime = None
      case None =>
    }
  }

  private[agent] def accumulate(): Long = {
    period = startTime match {
      case Some(atime) =>
        if (atime.isBefore(getTime))  period + new Interval(atime, getTime).toDurationMillis / 1000
        else period
      case None => period
    }
    period
  }
}
object AgentReadyTotalTime { val name = "ReadyTime" }
case class AgentReadyTotalTime(override val name: String = AgentReadyTotalTime.name, collector: StatCollector) extends AgentStatCalculatorByEvent with AgentTotalTime with JodaTimeProvider {

  override def processEvent(state: AgentState) = state match {
    case AgentReady(_, dateTime,_,_,_) => start(dateTime)
    case _ => stopAndPublish()
  }

}
object AgentPausedTotalTime { val name = "PausedTime" }
case class AgentPausedTotalTime(override val name: String = AgentPausedTotalTime.name, collector: StatCollector) extends AgentStatCalculatorByEvent  with AgentTotalTime with JodaTimeProvider {

  override def processEvent(state: AgentState) = state match {
    case AgentOnPause(_, dateTime,_,_,_) => start(dateTime)
    case _ => stopAndPublish()
  }

}

object AgentWrapupTotalTime { val name = "WrapupTime"}
case class AgentWrapupTotalTime(override val name: String = AgentWrapupTotalTime.name, collector: StatCollector) extends AgentStatCalculatorByEvent  with AgentTotalTime  with JodaTimeProvider {

  override def processEvent(state: AgentState) = state match {
    case AgentOnWrapup(_, dateTime,_,_,_) => start(dateTime)
    case _ => stopAndPublish()
  }

}
object AgentPausedTotalTimeWithCause { val name = "PausedTimeWithCause" }
case class AgentPausedTotalTimeWithCause(override val name: String = AgentPausedTotalTimeWithCause.name, collector: StatCollector) extends AgentStatCalculatorByEvent  with AgentTotalTime with JodaTimeProvider {

  override def processEvent(state: AgentState) = state match {
    case AgentOnPause(_, dateTime,_,_,Some(this.name)) => start(dateTime)
    case _ => stopAndPublish()
  }

}

trait AgentTotalCalls {
  this: AgentStatCalculator =>
  private[agent] var total:Long = 0

  override def reset() = {
    total = 0
    collector.onStatCalculated(name, StatTotal(total))
  }
  def accAndPublish(): Unit = {
    total = total + 1
    collector.onStatCalculated(name, StatTotal(total))
  }
}

object AgentInboundTotalCalls { val name = "InbCalls" }
case class AgentInboundTotalCalls(override val name: String = AgentInboundTotalCalls.name, collector: StatCollector) extends AgentStatCalculatorByTransition
  with AgentTotalCalls with AgentTransitionQualifier  {

  override def processTransition(t: AgentTransition) = {
    if (isNewIncomingCall(t)) {
      accAndPublish()
    }
  }

}
trait AgentTransitionQualifier {

  def isNewIncomingCall(t: AgentTransition): Boolean = {
    (t.stateFrom, t.stateTo) match {
      case (MAgentReady | MAgentPaused | MAgentOnWrapup, MAgentRinging | MAgentRingingOnPause | MAgentOnCall | MAgentOnCallOnPause) =>
        true
      case (_, _) =>
        false
    }
  }

  def isNewAnsweredIncomingCall(t: AgentTransition): Boolean = {
    (t.stateFrom, t.stateTo) match {
      case (MAgentOnCall | MAgentOnCallOnPause, MAgentOnCall | MAgentOnCallOnPause) =>
        false
      case (_, MAgentOnCall | MAgentOnCallOnPause) =>
        (t.contextTo) match {
          case (AgentStateContext(AgentOnCall(_, _, _, CallDirection.Incoming, _, _, _, _, _, _), _, _, _, _)) =>
            true
          case _ =>
            false
        }
      case (_, _) =>
        false
    }
  }

  def  isNewAnsweredOutgoingCall(t: AgentTransition): Boolean = {
    (t.stateFrom, t.stateTo) match {
      case (MAgentOnCall | MAgentOnCallOnPause, MAgentOnCall | MAgentOnCallOnPause) =>
        false
      case (_, MAgentOnCall | MAgentOnCallOnPause) =>
        (t.contextTo) match {
          case ( AgentStateContext(AgentOnCall(_, _, _, CallDirection.Outgoing, _, _, _, _, _, _), _, _, _, _)) =>
            true
          case _ =>
            false
        }
      case (_, _) =>
        false
    }
  }

  def isHangup(t: AgentTransition): Boolean = {
    (t.stateFrom, t.stateTo) match {
      case (MAgentOnCall | MAgentOnCallOnPause, MAgentOnCall | MAgentOnCallOnPause) =>
        false
      case (MAgentOnCall | MAgentOnCallOnPause, _) =>
        true
      case (_, _) =>
        false
    }
  }

}
object AgentInboundAnsweredCalls { val name = "InbAnsCalls" }
case class AgentInboundAnsweredCalls(override val name: String = AgentInboundAnsweredCalls.name, collector: StatCollector) extends AgentStatCalculatorByTransition
  with AgentTotalCalls with AgentTransitionQualifier {

  override def processTransition(t: AgentTransition) = {
    if (isNewAnsweredIncomingCall(t)) {
      accAndPublish()
    }
  }

}


object AgentInboundTotalCallTime { val name = "InbCallTime" }
case class AgentInboundTotalCallTime(override val name: String = AgentInboundTotalCallTime.name, collector: StatCollector) extends AgentStatCalculatorByTransition
  with AgentTotalTime with JodaTimeProvider with AgentTransitionQualifier {

  override def processTransition(t: AgentTransition) = {
    if (isNewAnsweredIncomingCall(t)) {
      start(getTime)
    }
    if (isHangup(t)) {
      stopAndPublish()
    }
  }
}

object AgentOutboundTotalCalls { val name = "OutCalls"}
case class AgentOutboundTotalCalls(override val name: String = AgentOutboundTotalCalls.name, collector: StatCollector) extends AgentStatCalculatorByEvent
  with AgentTotalCalls {

  override def processEvent(state: AgentState) = state match {
    case s:AgentDialing => accAndPublish()
    case _ =>
  }
}

object AgentOutboundTotalCallTime { val name = "OutCallTime" }
case class AgentOutboundTotalCallTime(override val name: String = AgentOutboundTotalCallTime.name, collector: StatCollector) extends AgentStatCalculatorByTransition
with AgentTotalTime with JodaTimeProvider with AgentTransitionQualifier {

  override def processTransition(t: AgentTransition) = {
    if (isNewAnsweredOutgoingCall(t)) {
      start(getTime)
    }
    if (isHangup(t)) {
      stopAndPublish()
    }
  }
}


trait AgentAverageTime {
  this: AgentStatCalculator =>
  private[agent] var divisor:Option[Long] = None

  override def reset() = {
    divisor = None
    collector.onStatCalculated(name, StatTotal(0))
  }
  def storeDivisor(value: Long)= {
    divisor = Some(value)
  }
  def averageAndPublish(value: Long) = {
    divisor match {
      case Some(divisor) if (divisor > 0) => collector.onStatCalculated(name, StatTotal(value / divisor))
      case Some(0) => collector.onStatCalculated(name, StatTotal(0))
      case None =>
    }
  }
}

object AgentInboundAverageCallTime { val name = "InbAverCallTime"}

case class AgentInboundAverageCallTime(override val name: String = AgentInboundAverageCallTime.name, collector: StatCollector)
  extends AgentStatCalculatorByStatValue with AgentAverageTime {

  override def processStat(stats: AgentStatistic) = {
    stats match {
      case AgentStatistic(_, List(Statistic(AgentInboundAnsweredCalls.name, StatTotal(value)))) =>
        storeDivisor(value)
      case AgentStatistic(_, List(Statistic(AgentInboundTotalCallTime.name, StatPeriod(value)))) =>
        averageAndPublish(value)
      case any =>
    }

  }

}

object AgentInboundUnansweredCalls { val name = "InbUnansCalls"}
case class AgentInboundUnansweredCalls (override val name: String = AgentInboundUnansweredCalls.name, collector: StatCollector)
  extends AgentStatCalculatorByStatValue {
  var total:Long = 0
  var answered:Long = 0

  private def calc() = total-answered

  override def reset() = {
    total = 0
    answered = 0
    collector.onStatCalculated(name, StatTotal(calc))
  }

  override def processStat(stats: AgentStatistic) = {
    stats match {
      case AgentStatistic(_, List(Statistic(AgentInboundTotalCalls.name, StatTotal(value)))) =>
        total = value
        collector.onStatCalculated(name, StatTotal(calc))
      case AgentStatistic(_, List(Statistic(AgentInboundAnsweredCalls.name, StatTotal(value))))  =>
        answered = value
        if (total >= value) collector.onStatCalculated(name, StatTotal(calc))
      case any =>
    }
  }

}
object AgentInboundPercentUnansweredCalls { val name = "InbPercUnansCalls"}
case class AgentInboundPercentUnansweredCalls(val name: String = AgentInboundPercentUnansweredCalls.name, collector: StatCollector)
  extends AgentStatCalculatorByStatValue {
    var total:Long = 0
    var unanswered:Long = 0

    private def calc:Double = if(total > 0 && total >= unanswered) ((unanswered.toDouble / total.toDouble)*100.0) else {
      if (unanswered > 0)  100.0 else 0.0
    }

    override def reset() = {
      total = 0
      unanswered = 0
      collector.onStatCalculated(name, StatAverage(calc))
    }
    override def processStat(stats: AgentStatistic) = {
      stats match {
        case AgentStatistic(_, List(Statistic(AgentInboundTotalCalls.name, StatTotal(value)))) =>
          total = value
          collector.onStatCalculated(name, StatAverage(calc))
        case AgentStatistic(_, List(Statistic(AgentInboundUnansweredCalls.name, StatTotal(value)))) =>
          unanswered = value
          collector.onStatCalculated(name, StatAverage(calc))
        case any =>
      }
    }
  }
