package services.agent

import akka.actor.Props
import services.config.ConfigDispatcher._
import xivo.models.Agent


object  AgentInGroupAdder {

  def props(groupId: Long, fromQueueId : Long, fromPenalty: Int) = Props(new AgentInGroupAdder(groupId, fromQueueId, fromPenalty))

}


class AgentInGroupAdder(groupId: Long, fromQueueId : Long, fromPenalty: Int) extends  AgentInGroupAction(groupId, fromQueueId, fromPenalty) {


  override def actionOnAgent(agent: Agent, toQueueId: Option[Long], toPenalty: Option[Int]) = {
    context.actorSelection(configDispatcherURI) ! ConfigChangeRequest(self,SetAgentQueue(agent.id,toQueueId.get,toPenalty.get))
  }
}

