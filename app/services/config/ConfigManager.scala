package services.config

import org.xivo.cti.CtiMessage
import akka.actor.Actor
import akka.actor.ActorLogging
import akka.actor.ActorRef
import akka.actor.ActorSelection.toScala
import akka.actor.Props
import models.XucUser
import services.request.{AgentActionRequest, BaseRequest}
import services.{Start, ActorFactory, ProductionActorFactory}
import xivo.network.LoggedOn
import org.xivo.cti.message.{IpbxCommandResponse, AgentStatusUpdate}
import xivo.events.{AgentLoginError, AgentError, AgentState}
import xivo.websocket.LinkStatusUpdate

object ConfigManager {
  def props(user: XucUser, agentManager: ActorRef, create: IpbxCommandResponse=>AgentLoginError = AgentError.create): Props
    = Props(new ConfigManager(user, agentManager, create) with ProductionActorFactory)

  case object PublishUserPhoneStatuses
  case class InitConfig(ctiLink: ActorRef)
}
trait ServicesActors {
  private[config] val ctiLink:ActorRef
  private[config] val agentService:ActorRef

  def createServices

}

class ConfigManager(val user: XucUser, val agentManager: ActorRef,
                    create: IpbxCommandResponse=>AgentLoginError)
  extends Actor with ActorLogging {
  this: ActorFactory =>
  import ConfigManager.{ InitConfig, PublishUserPhoneStatuses }

  log.info(s"Starting ConfigManager ${self} : ${user}")

  val ctiLink: ActorRef = getCtiLink(user.ctiUsername)
  val agentActionService = getAgentActionService(ctiLink)

  def receive = {

    case ln: LoggedOn =>
      log.info(s"loggedOn ${ln.user}")
      context.actorSelection(configDispatcherURI) ! InitConfig(ctiLink)

    case agentStatus: AgentStatusUpdate =>
      log.debug(s"$agentStatus")
      agentManager ! AgentState.fromAgentStatusUpdate(agentStatus)

    case agust: AgentUserStatus => agentManager ! agust

    case agentError: IpbxCommandResponse => agentActionService ! create(agentError)

    case message: CtiMessage =>
      log.debug(s"CtiMessage ${message}")
      context.actorSelection(configDispatcherURI) ! message

    case PublishUserPhoneStatuses =>
      log.debug("Request publish user phone statuses")
      context.actorSelection(configDispatcherURI) ! PublishUserPhoneStatuses

    case userPhoneStatus: UserPhoneStatus =>
      log.debug("---------------------------->received : " + userPhoneStatus)
      context.actorSelection(statusPublishURI) ! userPhoneStatus

    case LinkStatusUpdate(up) => ctiLink ! Start(user)

    case BaseRequest(r: ActorRef, a: AgentActionRequest) => agentActionService ! BaseRequest(r, a)

    case unknown => log.info(s"Uknown message received: $unknown")
  }

}

