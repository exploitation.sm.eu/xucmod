package services

import akka.actor.{Actor, ActorRef}
import models.XucUser
import services.agent._
import services.config.{AgentManager, XucLineManager}
import system.MainRunner
import xivo.network.CtiLink

object ActorFactory {

  val AgentConfigId        = "AgentConfig"
  val AgentDeviceManagerId = "AgentDeviceManager"
  val AgentManagerId       = "AgentManager"
  val LineManagerId        = "LineManager"
  val AmiBusConnectorId    = "AmiBusConnector"
  val ConfigDispatcherId   = "ConfigDispatcher"
  val ConfigManagerId      = "ConfigManager"
  val CtiFilterId          = "CtiFilter"
  val CtiLinkId            = "CtiLink"
  val CtiRouterId          = "CtiRouter"
  val CtiStatsAggregatorId = "CtiStatsAggregator"
  val MainRunnerId         = "MainRunner"
  val StatusPublishId      = "StatusPublish"
  val CallHistoryManagerId = "CallHistoryManager"

  def filterPath(username:String) =  s"${username}CtiFilter"
  def routerPath(username:String) =  s"${username}CtiRouter"

}

trait ActorFactory {

  def getAgentManager: ActorRef = null
  def getLineManager: ActorRef = null
  def getCtiLink(username: String ): ActorRef = null
  def getAgentActionService(link:ActorRef):ActorRef = null
  def getFilter(user: XucUser, router:ActorRef): ActorRef = null
  def getMoveAgentInGroup(groupId : Long, queueId: Long, penalty : Int): ActorRef = null
  def getAddAgentInGroup(groupId : Long, queueId: Long, penalty : Int): ActorRef = null
  def getRemoveAgentGroupFromQueueGroup(groupId : Long, queueId: Long, penalty : Int): ActorRef = null
  def getAddAgentsNotInQueueFromGroupTo(groupId : Long, queueId: Long, penalty : Int): ActorRef = null
  def getAgentGroupSetter(): ActorRef = null

  val statusPublishURI: String = ""
  val configDispatcherURI: String = ""
  val agentConfigURI: String = ""
  val amiBusConnectorURI: String = ""
  val lineManagerURI: String = ""
  val configManagerURI: String = ""
  val callHistoryManagerURI = ""
}

trait ProductionActorFactory extends ActorFactory {
  this: Actor =>

  override def getFilter(user: XucUser, router:ActorRef) = context.actorOf(CtiFilter.props(router), ActorFactory.filterPath(user.ctiUsername))

  override def getCtiLink(username: String) = context.actorOf(CtiLink.props(username), s"$username${ActorFactory.CtiLinkId}")

  override def getAgentActionService(link: ActorRef) = context.actorOf(AgentAction.props(link))
  override def getAgentManager() = context.actorOf(AgentManager.props, ActorFactory.AgentManagerId)

  override def getLineManager() = context.actorOf(XucLineManager.props, ActorFactory.LineManagerId)

  override def getMoveAgentInGroup(groupId : Long, queueId: Long, penalty : Int) = context.actorOf(AgentInGroupMover.props(groupId,queueId,penalty))

  override def getAddAgentInGroup(groupId : Long, queueId: Long, penalty : Int) = context.actorOf(AgentInGroupAdder.props(groupId,queueId,penalty))
  override def getRemoveAgentGroupFromQueueGroup(groupId : Long, queueId: Long, penalty : Int): ActorRef = context.actorOf(AgentGroupRemover.props(groupId,queueId,penalty))
  override def getAddAgentsNotInQueueFromGroupTo(groupId : Long, queueId: Long, penalty : Int): ActorRef =  context.actorOf(AgentFromGroupNotInQueueAdder.props(groupId,queueId,penalty))
  override def getAgentGroupSetter(): ActorRef = context.actorOf(AgentGroupSetter.props())

  override val statusPublishURI = s"${MainRunner.MainRunnerPath}/${ActorFactory.StatusPublishId}"
  override val configDispatcherURI = s"${MainRunner.MainRunnerPath}/${ActorFactory.ConfigDispatcherId}"
  override val agentConfigURI = s"${MainRunner.MainRunnerPath}/${ActorFactory.AgentConfigId}"
  override val amiBusConnectorURI = s"${MainRunner.MainRunnerPath}/${ActorFactory.AmiBusConnectorId}"
  override val lineManagerURI = s"${MainRunner.MainRunnerPath}/${ActorFactory.LineManagerId}"
  override val configManagerURI = s"${MainRunner.MainRunnerPath}/${ActorFactory.ConfigManagerId}"
  override val callHistoryManagerURI = s"${MainRunner.MainRunnerPath}/${ActorFactory.CallHistoryManagerId}"
}
