package xivo.phonedevices

import akka.actor.ActorRef
import org.xivo.cti.model.Endpoint
import play.api.Logger
import services.XucAmiBus.{AmiActionRequest, DialActionRequest, OutBoundDialActionRequest}
import services.config.ConfigRepository
import xivo.models.{Line, Agent}
import xivo.xuc.XucConfig

trait RequestToAmi {
  def toAmi(): Option[AmiActionRequest]
}

case class OutboundDial(destination: String, agentId: Agent.Id, queueNumber: String, xivoHost: String) extends RequestToAmi{

  def toAmi() = Some(OutBoundDialActionRequest(destination,s"select_agent(agent=agent_$agentId)", queueNumber, xivoHost))
}

case class StandardDial(line:Line, calee: String, xivoHost: String) extends RequestToAmi {

  def toAmi() =   Some(DialActionRequest(s"${line.protocol}/${line.name}", line.context, calee, xivoHost))
}

trait DeviceAdapter {

  def dial(destination: String, sender: ActorRef)
  def answer(sender: ActorRef)
  def hangup(sender: ActorRef)
  def attendedTransfer(destination: String, sender: ActorRef)
  def completeTransfer(sender: ActorRef)
  def cancelTransfer(sender: ActorRef)
  def conference(sender: ActorRef)
  def hold(sender: ActorRef)

  def odial(destination: String, agentId: Agent.Id, queueNumber: String, sender: ActorRef) = {
    if (destination.length > XucConfig.outboundLength && queueNumber != "") {
      sender ! OutboundDial(destination, agentId, queueNumber, XucConfig.xivoHost)
    }
    else {
      dial(destination, sender)
    }
  }
}