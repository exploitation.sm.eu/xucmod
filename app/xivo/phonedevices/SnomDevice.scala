package xivo.phonedevices

import akka.event.slf4j.Logger
import play.api.libs.ws.WS
import akka.actor.ActorRef
import play.api.libs.ws.WSAuthScheme
import xivo.xuc.XucConfig
import org.xivo.cti.model.Endpoint
import play.api.Play.current

class SnomDevice(ip: String) extends DeviceAdapter {
  val LoggerName = getClass.getName

  def formatDialNumber(s:String)= s filter ("*#0123456789" contains _)

  override def dial(destination: String, sender: ActorRef) = doIt(s"number=${formatDialNumber(destination)}")
  override def answer(sender: ActorRef) = doIt("key=ENTER")
  override def hangup(sender: ActorRef) = doIt("key=CANCEL")
  override def attendedTransfer(destination: String, sender:ActorRef) = doIt(s"number=${formatDialNumber(destination)}")
  override def completeTransfer(sender: ActorRef) = doIt("key=TRANSFER")
  override def cancelTransfer(sender: ActorRef) = doIt("key=CANCEL")
  override def conference(sender: ActorRef) =  doIt("key=CONFERENCE")
  override def hold(sender: ActorRef) =  doIt("key=F_HOLD")

  private def doIt(command:String) = {
    try {
      WS.url(s"http://$ip/command.htm?$command")
        .withAuth(XucConfig.SnomUser, XucConfig.SnomPassword, WSAuthScheme.BASIC)
        .get
    }
    catch {
      case e:IllegalArgumentException => Logger(LoggerName).error(s"Unable to call service $command",e)
    }
  }
}