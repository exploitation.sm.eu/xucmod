package xivo.services

import akka.actor.{ActorLogging, Actor, Props}
import models.TokenRequest
import org.joda.time.DateTime
import play.api.libs.json.Json
import services.config.ConfigRepository
import xivo.network.XiVOWSdef
import models.Token
import xivo.services.XivoAuthentication.{Expired, Init, AuthUnknownUser, AuthTimeout}
import xivo.xuc.XucConfig
import scala.concurrent.Await
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration.{MILLISECONDS, FiniteDuration}


object XivoAuthentication {
  def props(repo: ConfigRepository, xivoWS: XiVOWSdef): Props =
    Props(new XivoAuthentication(repo, xivoWS))

  case object Init
  case class Init(tokens: Map[Long, Token])
  case class GetToken(userId: Long)
  case object AuthTimeout
  case object AuthUnknownUser
  case class Expired(t: Token, userId: Long)
}

class XivoAuthentication(repo: ConfigRepository, xivoWS: XiVOWSdef) extends Actor with ActorLogging {

  val renewTimeOverlap = 200

  override def preStart: Unit = {
    self ! Init
  }

  def receive: Receive = {
    case Init =>
      context.become(receiveWithCache(Map[Long, Token]()))
  }

  def receiveWithCache(tokens: Map[Long, Token]): Receive = {
    case Init(m) =>
      context.become(receiveWithCache(m))
    case XivoAuthentication.GetToken(userId) =>
      tokens.get(userId) match {
        case Some(t) =>
          sender ! t
        case None =>
          getToken(tokens, userId)
      }
    case Expired(t, userId) =>
      self ! XivoAuthentication.GetToken(userId)
      log.debug(s"Removing old token and asking for new for user: $userId")
      context.become(receiveWithCache(tokens - userId))

    case u =>
      log.debug(s"Unprocessed message: $u")
  }

  def getToken(tokens: Map[Long, Token], userId: Long): Unit = {
    repo.getUser(userId) match {
      case Some(user) =>
        val request = xivoWS.post(XucConfig.xivoHost, XucConfig.XivoAuth.getTokenURI,
          Json.toJson(TokenRequest(XucConfig.XivoAuth.backend, XucConfig.XivoAuth.defaultExpires)),
          user.username, user.password.getOrElse(""))
        val result = request.execute().map(_.json.validate[Token].get)
        val token = Await.result(result, XucConfig.defaultWSTimeout)
        token match {
          case t: Token =>
            sender ! t
            log.debug(s"Got token: $t")
            context.become(receiveWithCache(tokens + (userId -> t)))
            context.system.scheduler.scheduleOnce(
              new FiniteDuration(t.expiresAt.minus(new DateTime().getMillis + renewTimeOverlap).getMillis, MILLISECONDS),
              self,
              Expired(t, userId)
            )
          case _ =>
            log.debug(s"Timeout when getting token for user $userId")
            sender ! AuthTimeout
        }
      case None =>
        log.error("Requested token for unknown user")
        sender ! AuthUnknownUser
    }
  }
}
