package xivo.xuc

import models.authentication.AuthenticationProvider
import play.api.Play.current
import play.api._
import play.api.libs.concurrent.Akka
import services.CtiRouterFactory
import system.MainRunner

class XucPlugin(val app: Application) extends Plugin {
  var ctiRouterFactory: CtiRouterFactory = null

  override def onStart() {
    Logger.info("Starting xucmod .......")
    Logger.info("-------------parameters--------------------")
    Logger.info(s"host           : ${XucConfig.XIVOCTI_HOST}")
    Logger.info(s"port           : ${XucConfig.XIVOCTI_PORT}")
    Logger.info(s"wsHost         : ${XucConfig.XivoWs_host}")
    Logger.info(s"wsUser         : ${XucConfig.XivoWs_wsUser}")
    Logger.info(s"peers          : ${XucConfig.xucPeers}")
    Logger.info(s"outboundLength : ${XucConfig.outboundLength}")
    Logger.info(s"valid authent  : ${AuthenticationProvider.validAuthentications.length > 0}")

    if (AuthenticationProvider.validAuthentications.length == 0)
      Logger.warn("No authentication configured")

    if (XucConfig.EventUser!= "") startMainRunner
    else Logger.error("xivocti.eventUser missing unable to start")

  }

  override def onStop() {
    Logger.info("Xucmod shutdown...")
  }
  
  override def enabled = true

  def startMainRunner() {
    val runner = Akka.system.actorOf(MainRunner.props,"MainRunner")
  }
}