package xivo.websocket

import org.xivo.cti.message.Sheet
import play.api.libs.json._
//TODO sort by name
object WSMsgType extends Enumeration {

  type WSMsgType = Value
  val AgentLogin = Value
  val AgentLogout = Value
  val LoggedOn = Value
  val UsersStatuses = Value
  val UserStatusUpdate = Value
  val PhoneRinging = Value
  val PhoneCalling = Value
  val PhoneAvailable = Value
  val Sheet = Value(classOf[Sheet].getSimpleName)
  val DirectoryResult = Value
  val PhoneStatusUpdate = Value
  val QueueStatistics = Value
  val AgentReady = Value
  val AgentPaused = Value
  val QueueConfig = Value
  val QueueList = Value
  val QueueMember = Value
  val QueueMemberList = Value
  val AgentConfig = Value
  val AgentList = Value
  val AgentGroupList = Value
  val AgentError = Value
  val AgentStateEvent = Value
  val AgentStatistics = Value
  val AgentDirectory = Value
  val AgentListen = Value
  val LineStateEvent = Value
  val Error=Value
  val LinkStatusUpdate = Value
  val ConferenceList = Value
  val UserConfigUpdate = Value
  val VoiceMailStatusUpdate = Value
  val QueueCalls = Value
  val CallHistory = Value
}

object LinkState extends Enumeration {
  type LinkState = Value
  val down,up = Value
}
import xivo.websocket.LinkState._

case class LinkStatusUpdate(status: LinkState)

object LinkStatusUpdate {

  implicit val linkStatusWrites = new Writes[LinkStatusUpdate] {
    def writes(linkStatus: LinkStatusUpdate) = Json.obj(
      "status" -> linkStatus.status.toString
    )
  }
  def toJson(linkStatus : LinkStatusUpdate) =   Json.toJson(linkStatus)
}
