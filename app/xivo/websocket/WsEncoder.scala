package xivo.websocket

import models.QueueCallList
import org.xivo.cti.message.QueueConfigUpdate
import services.XucStatsEventBus.AggregatedStatEvent
import services.agent.AgentStatistic
import services.config.ConfigDispatcher._
import xivo.events.AgentState
import xivo.models.{CallHistory, Agent, AgentQueueMember}
import xivo.websocket.WsBus.WsContent

class WsEncoder {

  def encode(msg : Any): Option[WsContent] = msg match {
    case agentState : AgentState =>
      Some(WsContent(WebsocketEvent.createEvent(agentState)))

    case agent: Agent =>
      Some(WsContent(WebsocketEvent.createEvent(agent)))

    case agentQM: AgentQueueMember =>
      Some(WsContent(WebsocketEvent.createEvent(agentQM)))

    case queue: QueueConfigUpdate =>
      Some(WsContent(WebsocketEvent.createEvent(queue)))

    case QueueList(queues) =>
      Some(WsContent(WebsocketEvent.createQueuesEvent(queues)))

    case AgentList(agents) =>
      Some(WsContent(WebsocketEvent.createAgentListEvent(agents)))

    case AgentGroupList(agentGroups) =>
      Some(WsContent(WebsocketEvent.createAgentGroupsEvent(agentGroups)))

    case AgentQueueMemberList(agentQueueMembers) =>
      Some(WsContent(WebsocketEvent.createAgentQueueMembersEvent(agentQueueMembers)))

    case ad : AgentDirectory =>
      Some(WsContent(WebsocketEvent.createEvent(ad)))

    case aggregatedStat : AggregatedStatEvent =>
      Some(WsContent(WebsocketEvent.createEvent(aggregatedStat)))

    case MeetmeList(meetmeList) => Some(WsContent(WebsocketEvent.createConferenceListEvent(meetmeList)))

    case agentStat : AgentStatistic => Some(WsContent(WebsocketEvent.createEvent(agentStat)))

    case queueCalls: QueueCallList => Some(WsContent(WebsocketEvent.createEvent(queueCalls)))

    case history: CallHistory => Some(WsContent(WebsocketEvent.createEvent(history)))

    case unknown => None
  }

}
