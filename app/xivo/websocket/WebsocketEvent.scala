package xivo.websocket

import com.fasterxml.jackson.databind.JsonNode
import models.{QueueCallList, RichDirectoryResult}
import org.xivo.cti.message._
import org.xivo.cti.model.{Meetme, UserStatus, PhoneHintStatus}
import play.api.libs.json.{Writes}
import play.libs.Json
import services.XucStatsEventBus.{AggregatedStatEvent}
import services.agent.AgentStatistic
import services.config.ConfigDispatcher.AgentDirectory
import xivo.ami.AmiBusConnector.{AgentListenStopped, AgentListenStarted, LineEvent}
import xivo.events.AgentState
import xivo.models.{CallHistory, AgentGroup, Agent, AgentQueueMember}
import xivo.websocket.WSMsgType.WSMsgType

import scala.collection.JavaConversions._

object WebsocketEvent {
  val ErrorKey = "Error"

  val MsgType = "msgType"
  val CtiMessage  = "ctiMessage"
  class EventBuilder(msgType: WSMsgType) {
    val jsonEvent = Json.newObject
    jsonEvent.put(MsgType, msgType.toString)

    def asEvent = jsonEvent

    def asEvent[T](o:T)(implicit tjs: Writes[T]) = {
      val msg = tjs.writes(o).toString()
      jsonEvent.set(CtiMessage, Json.parse(msg))
      jsonEvent
    }

    def asEvent(payLoad: JsonNode) = {
      jsonEvent.set(CtiMessage, payLoad)
      jsonEvent
    }
  }

  def createLoggedOnEvent() = new EventBuilder(WSMsgType.LoggedOn) asEvent


  def createEvent(agent: Agent)                             = new EventBuilder(WSMsgType.AgentConfig)     asEvent(agent)

  def createEvent(agentDirectory: AgentDirectory)           = new EventBuilder(WSMsgType.AgentDirectory)  asEvent(agentDirectory)
  def createEvent(aqm: AgentQueueMember)                    = new EventBuilder(WSMsgType.QueueMember)     asEvent(aqm)
  def createEvent(aglStarted: AgentListenStarted)           = new EventBuilder(WSMsgType.AgentListen)     asEvent(aglStarted)
  def createEvent(aglStopped: AgentListenStopped)           = new EventBuilder(WSMsgType.AgentListen)     asEvent(aglStopped)
  def createEvent(agentState: AgentState)                   = new EventBuilder(WSMsgType.AgentStateEvent) asEvent(agentState)
  def createEvent(agentStatistic: AgentStatistic)           = new EventBuilder(WSMsgType.AgentStatistics) asEvent(agentStatistic)
  def createEvent(ls: LinkStatusUpdate): JsonNode           = new EventBuilder(WSMsgType.LinkStatusUpdate) asEvent(ls)
  def createEvent(rdResult : RichDirectoryResult)           = new EventBuilder(WSMsgType.DirectoryResult) asEvent(rdResult)
  def createEvent(statEvent : AggregatedStatEvent)          = new EventBuilder(WSMsgType.QueueStatistics) asEvent(statEvent)
  def createAgentListEvent(agents: List[Agent])             = new EventBuilder(WSMsgType.AgentList)       asEvent(agents)
  def createAgentGroupsEvent(agentGroups: List[AgentGroup]) = new EventBuilder(WSMsgType.AgentGroupList)  asEvent(agentGroups)
  def createAgentQueueMembersEvent(agentQueueMembers : List[AgentQueueMember])  = new EventBuilder(WSMsgType.QueueMemberList) asEvent(agentQueueMembers)
  def createEvent(userStatuses : java.util.List[UserStatus])  = new EventBuilder(WSMsgType.UsersStatuses) asEvent(Json.toJson(userStatuses))

  def createEvent(queue: QueueConfigUpdate)                   = new EventBuilder(WSMsgType.QueueConfig) asEvent(Json.toJson(queue))
  def createQueuesEvent(queues : List[QueueConfigUpdate])     = new EventBuilder(WSMsgType.QueueList) asEvent(Json.toJson(seqAsJavaList(queues)))
  def createEvent(st: Sheet)                                  = new EventBuilder(WSMsgType.Sheet)  asEvent(Json.toJson(st))
  def createEvent(statistic: QueueStatistics)                 = new EventBuilder(WSMsgType.QueueStatistics) asEvent(Json.toJson(statistic))
  def createEvent(ph: PhoneHintStatus)                        = new EventBuilder(WSMsgType.PhoneStatusUpdate) asEvent(Json.newObject().put("status",ph.toString))
  def createEvent(us: UserStatusUpdate)                       = new EventBuilder(WSMsgType.UserStatusUpdate)  asEvent(Json.newObject().put("status",us.getStatus))
  def createEvent(uc: UserConfigUpdate)                       = new EventBuilder(WSMsgType.UserConfigUpdate)  asEvent(Json.toJson(uc))
  def createEvent(le: LineEvent)                              = new EventBuilder(WSMsgType.LineStateEvent)  asEvent(le)
  def createConferenceListEvent(meetmes: List[Meetme])        = new EventBuilder(WSMsgType.ConferenceList) asEvent(Json.toJson(seqAsJavaList(meetmes)))
  def createEvent(voiceMailStatusUpdate: VoiceMailStatusUpdate) = new EventBuilder(WSMsgType.VoiceMailStatusUpdate) asEvent(Json.toJson(voiceMailStatusUpdate))
  def createEvent(queueCalls: QueueCallList): JsonNode        = new EventBuilder(WSMsgType.QueueCalls) asEvent(queueCalls)
  def createEvent(callHistory: CallHistory): JsonNode         = new EventBuilder(WSMsgType.CallHistory) asEvent(callHistory)

  def createEvent(agentError: IpbxCommandResponse) = {
    agentError.getError_string match {
      case "agent_login_invalid_exten" => createError(WSMsgType.AgentError, "PhoneNumberUnknown")
      case "agent_login_exten_in_use" => createError(WSMsgType.AgentError, "PhoneNumberAlreadyInUse")
      case _ => createError(WSMsgType.AgentError, s"Unknown:${agentError.getError_string}")
    }
  }

  def createError(errorType: WSMsgType, errorMsg: String, data: Map[String, String] = Map()) = {
    val errData = Json.newObject
    errData.put(ErrorKey, errorMsg)
    data.foreach { case (k, v) => errData.put(k, v) }
    new EventBuilder(errorType) asEvent(errData)
  }

}
