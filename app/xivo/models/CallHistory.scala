package xivo.models

import org.joda.time.format.{DateTimeFormat, PeriodFormatterBuilder}
import org.joda.time.{DateTime, Period}
import play.api.libs.functional.syntax._
import play.api.libs.json._

case class CallDetail(start: DateTime,
                      duration: Option[Period],
                      srcNum: String,
                      dstNum: String,
                      status: CallStatus.CallStatus)

object CallStatus extends Enumeration {
  type CallStatus = Value
  val Emitted = Value("emitted")
  val Answered = Value("answered")
  val Missed = Value("missed")
  val Ongoing = Value("ongoing")
}

object CallDetail {

  val format = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss")
  val periodFormat = new PeriodFormatterBuilder()
    .minimumPrintedDigits(2).printZeroAlways().appendHours().appendSeparator(":").appendMinutes().appendSeparator(":").appendSeconds().toFormatter

  def validate(json: JsValue) = json.validate[CallDetail]

  implicit val reads: Reads[CallDetail] =
    ((JsPath \ "start").read[String].map(format.parseDateTime) and
    (JsPath \ "duration").read[Option[String]].map(sOpt => sOpt.map(periodFormat.parsePeriod)) and
    (JsPath \ "src_num").read[String] and
    (JsPath \ "dst_num").read[String] and
    (JsPath \ "status").readNullable[String].map(s => CallStatus.withName(s.getOrElse("answered"))))(CallDetail.apply _)

  implicit val writes = new Writes[CallDetail] {
    def writes(call: CallDetail) = Json.obj(
      "start" -> call.start.toString(format),
      "duration" -> call.duration.map(_.toString(periodFormat)),
      "srcNum" -> call.srcNum,
      "dstNum" -> call.dstNum,
      "status" -> call.status.toString
    )
  }
}

case class CallHistory(calls: List[CallDetail])

object CallHistory {
  def validate(json: JsValue) = json.validate[List[CallDetail]].map(CallHistory.apply)

  implicit val writes = new Writes[CallHistory] {
    override def writes(ch: CallHistory) = JsArray(ch.calls.map(Json.toJson(_)))
  }
}