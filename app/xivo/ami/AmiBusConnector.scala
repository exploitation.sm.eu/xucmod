package xivo.ami

import akka.actor.{Actor, ActorLogging, ActorRef, Props}
import play.api.libs.json.Writes._
import play.api.libs.json._
import services.XucAmiBus
import services.XucAmiBus._
import services.config.ConfigRepository
import services.line.LineState
import services.line.LineState.LineState
import services.request.{KeyLightRequest, MonitorPause, MonitorUnpause}
import xivo.ami.AmiBusConnector.{AgentListenStarted, AgentListenStopped, CallData, LineEvent}
import xivo.events.AgentState.AgentLogin
import xivo.models.Agent
import xivo.phonedevices.RequestToAmi
import xivo.xucami.models.MonitorState.MonitorState
import xivo.xucami.models._
import xivo.xucami.userevents.UserEventAgentLogin


object AmiBusConnector {
  def props(configRepository: ConfigRepository, agentManager: ActorRef, lineManager: ActorRef, configManager: ActorRef) =
    Props(new AmiBusConnector(configRepository, agentManager, lineManager, configManager))

  case class CallData(callId: String, state: LineState, callerIdNb:String, variables:Map[String,String]=Map())
  implicit val callDataWrites =  new Writes[CallData] {
    def writes(cd: CallData) = Json.obj(
      "callId" -> cd.callId,
      "lineState" -> cd.state.toString
    )
  }
  object CallData {
    def apply(channel:Channel):CallData = CallData(channel.linkedChannelId.getOrElse(""), LineState(channel.state.id), channel.callerId.number, channel.variables)
  }

  case class LineEvent(val lineNumber: Int, call: CallData)
  implicit val lineEventWrites =  new Writes[LineEvent] {
    def writes(le: LineEvent) = Json.obj(
      "lineNumber" -> le.lineNumber,
      "callData" -> Json.toJson(le.call)
    )
  }

  abstract class AgentListenNotification(val phoneNumber: String, val agentId: Option[Agent.Id])

  case class AgentListenStarted(override val phoneNumber: String, override val agentId: Option[Agent.Id]) extends AgentListenNotification(phoneNumber, agentId)
  implicit val agentListenStartedWrites = new Writes[AgentListenStarted] {
    def writes(als: AgentListenStarted) = Json.obj (
      "started" -> true,
      "phoneNumber" -> als.phoneNumber,
      "agentId" -> als.agentId
    )
  }

  case class AgentListenStopped(override val phoneNumber: String, override val agentId: Option[Agent.Id]) extends AgentListenNotification(phoneNumber, agentId)
  implicit val agentListenStoppedWrites = new Writes[AgentListenStopped] {
    def writes(als: AgentListenStopped) = Json.obj (
      "started" -> false,
      "phoneNumber" -> als.phoneNumber,
      "agentId" -> als.agentId
    )
  }

}

case class AgentCallUpdate(val agentId: Agent.Id, val monitorState: MonitorState)

class AmiBusConnector(configRepository: ConfigRepository, agentManager: ActorRef, lineManager: ActorRef, configDispatcher: ActorRef, amiBus: XucAmiBus = XucAmiBus.amiEventBus)
  extends Actor with ActorLogging {

  amiBus.subscribe(self, AmiType.ChannelEvent)
  amiBus.subscribe(self, AmiType.UserEventAgent)
  amiBus.subscribe(self, AmiType.QueueEvent)

  def receive = {

    case ChannelEvent(channel) =>
      processChannel(channel)

    case AmiUserEventAgent(userEventAgentLogin: UserEventAgentLogin) =>
      log.debug(s"$userEventAgentLogin -> ${AgentLogin(userEventAgentLogin)}")
      agentManager ! AgentLogin(userEventAgentLogin)

    case SpyStarted(SpyChannels(spyerChannel, spyeeChannel)) =>
      log.debug(s"spy started $spyerChannel, $spyeeChannel")
      lineManager ! AgentListenStarted(spyeeChannel.callerId.number, configRepository.getAgentLoggedOnPhoneNumber(spyeeChannel.callerId.number))

    case SpyStopped(Channel(_,_,CallerId(_,number),_,_,_,_,_,_,_)) =>
      lineManager ! AgentListenStopped(number, configRepository.getAgentLoggedOnPhoneNumber(number))

    case pause: MonitorPause =>
      configRepository.getAgent(pause.id) match {
        case Some(agent) =>
          amiBus.publish(ChannelRequest(ChannelActionRequest(Some(agent.number), ChannelAction.PauseMonitor)))
        case None =>
          log.debug(s"Requested monitor pause on unknown agent: $pause")
      }

    case unpause: MonitorUnpause =>
      configRepository.getAgent(unpause.id) match {
        case Some(agent) =>
          amiBus.publish(ChannelRequest(ChannelActionRequest(Some(agent.number), ChannelAction.UnpauseMonitor)))
        case None =>
          log.debug(s"Requested monitor pause on unknown agent: $unpause")
      }
    case klReq : KeyLightRequest =>
      log.debug(s"$klReq")
      amiBus.publish(klReq.toAmi)

    case listenRequest: ListenRequest =>
      log.debug(s"$listenRequest")
      amiBus.publish(listenRequest)

    case request: RequestToAmi =>
      request.toAmi match {
        case Some(amiRequest) =>
          amiBus.publish(AmiRequest(amiRequest))
        case None =>
          log.debug("AMI Request could not be sent")
      }

    case e @ (_: EnterQueue | _: LeaveQueue) =>
      log.debug(s"EnterQueue received: $e")
      configDispatcher ! e

    case any =>
      log.debug(s"Received unprocessed message: $any")
  }

  private def processChannel(channel: Channel) {
      for {
      number <- channel.agentNumber
      agent <- configRepository.getAgent(number)
    } agentManager ! AgentCallUpdate(agent.id.toInt, channel.monitored)

    try {
      lineManager ! LineEvent(channel.callerId.number.toInt,CallData(channel))
      channel.connectedLineNb foreach  (number => lineManager ! LineEvent(number.toInt,CallData(channel)))

    } catch {
      case e: NumberFormatException =>
        log.debug(s"Discarding channel event: $channel because of invalid callerId number (not a number).")
    }
  }

}
