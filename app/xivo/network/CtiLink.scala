package xivo.network

import java.net.InetSocketAddress

import akka.actor.{Actor, ActorRef, Props, actorRef2Scala}
import akka.io.Tcp.{Connect, Event, Register, ResumeReading, SuspendReading, Write}
import akka.io.{IO, Tcp}
import akka.util.{ByteString, Timeout}
import org.json.JSONObject
import org.xivo.cti.message.{CtiResponseMessage, LoginCapasAck, LoginIdAck, LoginPassAck}
import play.api.Logger
import play.api.libs.concurrent.Execution.Implicits.defaultContext
import services.Start
import xivo.websocket.LinkState._
import xivo.websocket.LinkStatusUpdate
import xivo.xuc.XucConfig

import scala.concurrent.duration.{Duration, DurationInt, SECONDS}

object CtiLink {
  val DefaultReconnectTimer = 20 seconds
  val RestartReconnectTimer = 100 millis

  case object ConnectionRequest
  case object RestartRequest

  case class Restart(reason: Exception)

  class RemoteConnectionClosedException extends Exception("Remote connection closed")
  class ConnectionClosedException extends Exception("Connection closed")
  class ConnectionFailedException extends Exception("Connection failed")

  def props(username : String) = Props(new CtiLink(username, new MessageProcessor ))
}
trait CtiLinkLog {
  this: CtiLink =>
  val logger = Logger(getClass.getPackage().getName() + ".CtiLink." + self.path.name)
  private def logMessage(msg: String): String = s"[$username] $msg"
  object log {
    def debug(msg: String) = logger.debug(logMessage(msg))
    def info(msg: String) = logger.info(logMessage(msg))
    def error(msg: String) = logger.error(logMessage(msg))
    def warn(msg: String) = logger.warn(logMessage(msg))
  }
}

class CtiLink(val username: String, val messageProcessor: MessageProcessor) extends Actor with CtiLinkLog {
  import xivo.network.CtiLink._
  case object Ack extends Event

  import context.system
  implicit val timeout = Timeout(Duration(XucConfig.XivoCtiTimeout, SECONDS))
  protected[network] var reconnectTimer = DefaultReconnectTimer
  protected[network] var connection: ActorRef = null


  val endpoint = new InetSocketAddress(XucConfig.XIVOCTI_HOST, XucConfig.XIVOCTI_PORT.toInt)
  val manager = IO(Tcp)

  log.info(s"........Starting CtiLink actor for user $username")

  protected[network] var ctiLoginStep: ActorRef = null

  override def preStart(): Unit = {
    log.debug(s"preStart CtiLink actor for user $username ")
    ctiLoginStep = context.actorOf(CtiLoginStep.props, username + "CtiLoginStep")
    self ! ConnectionRequest
  }

  def processDecodedMessage(message: CtiResponseMessage[_]) = {
    message match {
      case message: LoginCapasAck =>
        ctiLoginStep ! message

      case message: LoginIdAck =>
        ctiLoginStep ! message

      case message: LoginPassAck =>
        ctiLoginStep ! message

      case _ =>
        context.parent ! message
    }
  }

  def commonReceive: Receive = {
    case Start(user) =>
      log.info(s"Starting link  user : ${user.ctiUsername}")
      ctiLoginStep ! StartLogin(user)

    case ConnectionRequest =>
      log.info("Connection request to " + XucConfig.XIVOCTI_HOST + " on port " + XucConfig.XIVOCTI_PORT);
      manager ! Tcp.Connect(endpoint)

    case Tcp.CommandFailed(_: Connect) ⇒
      log.error(s"Connection for user: [$username] failed")
      scheduleRestart(new ConnectionFailedException)

    case connected : Tcp.Connected =>
      log.info(s"Connection for user: [$username] established")
      reconnectTimer = DefaultReconnectTimer
      connection = sender
      context.become(receiveNoBuffer)
      connection ! Register(self, keepOpenOnPeerClosed = false)
      context.parent ! LinkStatusUpdate(up)

    case Tcp.CommandFailed(w: Write) ⇒
      log.error(s"Unable to write data $w")
      context stop self

    case ln: LoggedOn => onCtiServerLoggedOn(ln)

    case Tcp.PeerClosed =>
      context.become(receiveOnStart)
      onRemoteConnectionClosed
    case _: Tcp.ConnectionClosed =>
      context.become(receiveOnStart)
      onConnectionClosed
    case Tcp.Received(data) => processData(data)
    case RestartRequest => onRestartRequested
    case Restart(reason) => throw reason
    case Ack ⇒ acknowledge()

  }

  def receiveAndBufferize: Receive = {
    case ctiMessage: JSONObject =>
      log.debug(">>>Buffering : %s\n".format(ctiMessage.toString))
      buffer(ByteString("%s\n".format(ctiMessage.toString)))

    case unknown => log.error(s"receiveAndBufferize Uknown message received: $unknown")

  }

  def receiveNoBufferize: Receive = {
    case ctiMessage: JSONObject =>
      val data = ByteString("%s\n".format(ctiMessage.toString))
      log.debug(">>> %s\n".format(ctiMessage.toString))
      buffer(data)
      connection ! Tcp.Write(data, Ack)
      context.become(receiveWithBuffer)

    case unknown => log.error(s"receiveNoBufferize Uknown message received: $unknown ")

  }

  def receive = receiveOnStart

  def receiveOnStart = commonReceive
  def receiveWithBuffer = commonReceive orElse receiveAndBufferize
  def receiveNoBuffer = commonReceive orElse receiveNoBufferize

  override def postStop {
    log.info(s"Post stop cleaning tcp connection ${connection}")
    if (connection != null) connection ! Tcp.Close
  }

  private def onRestartRequested() = {
    log.info("restarting link")
    reconnectTimer = RestartReconnectTimer
    if (connection != null) connection ! Tcp.Close
  }

  private def onRemoteConnectionClosed = {
    log.info(s"CTI remote server closed connection sending message to ${context.parent} scheduling restart in ${reconnectTimer}")
    context.parent ! LinkStatusUpdate(down)
    closing = true
    scheduleRestart(new RemoteConnectionClosedException)
  }

  private def onConnectionClosed = {
    log.info(s"CTI socket closed scheduling restart in ${reconnectTimer}")
    scheduleRestart(new ConnectionClosedException)
  }

  private def onCtiServerLoggedOn(ln: LoggedOn) = {
    log.info(s"Logged on ${ln.user.ctiUsername} ${ln.userId}")
    context.parent ! ln
  }

  private def processData(data: ByteString) = {
    val receivedString = data.decodeString("US-ASCII")
    log.debug("<<< " + receivedString)
    val messages = messageProcessor.processBuffer(receivedString)
    messages.foreach((message: CtiResponseMessage[_]) => {
      log.debug(s"$message")
      processDecodedMessage(message)
    })
  }

  var storage = Vector.empty[ByteString]
  var stored = 0L
  var transferred = 0L
  var closing = false

  val maxStored = 100000000L
  val highWatermark = maxStored * 5 / 10
  val lowWatermark = maxStored * 3 / 10
  var suspended = false

  //#simple-helpers
  private def buffer(data: ByteString): Unit = {
    storage :+= data
    stored += data.size

    if (stored > maxStored) {
      log.error(s"drop connection (buffer overrun)")
      context stop self

    } else if (stored > highWatermark) {
      log.error(s"suspending reading")
      connection ! SuspendReading
      suspended = true
    }
  }

  private def acknowledge(): Unit = {
    require(storage.nonEmpty, "storage was empty")

    val size = storage(0).size
    stored -= size
    transferred += size

    storage = storage drop 1

    if (suspended && stored < lowWatermark) {
      log.debug("resuming reading")
      connection ! ResumeReading
      suspended = false
    }

    if (storage.isEmpty) {
      if (!closing) context.become(receiveNoBuffer)
    } else connection ! Write(storage(0), Ack)
  }

  private def scheduleRestart(ex: Exception) = {
    context.system.scheduler.scheduleOnce(reconnectTimer) {
      self ! Restart(ex)
    }
  }
}

