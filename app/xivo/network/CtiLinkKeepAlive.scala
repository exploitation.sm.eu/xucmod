package xivo.network

import akka.actor._
import org.json.JSONObject
import org.xivo.cti.MessageFactory
import xivo.network.CtiLinkKeepAlive.{StopKeepALive, StartKeepAlive}
import play.api.libs.concurrent.Execution.Implicits.defaultContext

import scala.concurrent.duration._

object CtiLinkKeepAlive {
  case class StartKeepAlive(userId: String, link: ActorRef)
  case object StopKeepALive

  def props(userName: String) = Props(new CtiLinkKeepAlive(userName))
}

class CtiLinkKeepAlive(userName: String) extends Actor with ActorLogging {
  val KeepAliveInterval = 10 minutes

  override def receive: Receive = {

    case StartKeepAlive(userId, link) =>
      val scheduler = context.system.scheduler.schedule(
        initialDelay = KeepAliveInterval,
        interval = KeepAliveInterval,
        receiver = self,
        message = new MessageFactory().createGetUserStatus(userId)
      )

      context.become(keepAlive(link, scheduler))

    case _ =>
  }


  def keepAlive(link: ActorRef, scheduler:Cancellable):Receive = {
    case  getUserStatus: JSONObject =>
      log.info(s"[$userName] keepalive : $getUserStatus")
      link ! getUserStatus

    case StopKeepALive => scheduler.cancel()
    case _ =>
  }


  override def postStop(): Unit = {
    self ! StopKeepALive
  }
}
