package xivo.network

import org.slf4j.LoggerFactory
import play.api.libs.json.{JsValue, Json}
import play.api.libs.ws.WS
import play.api.Play.current
import scala.concurrent.ExecutionContext.Implicits.global

import scala.concurrent.Future

class RecordingWS(host: String, port: Int, username: String, password: String) {

  val logger = LoggerFactory.getLogger(getClass)
  var cookie: Option[String] = None

  def getCallHistory(size: Int, agentNumber: String): Future[HistoryServerResponse] = {
    if (cookie.isEmpty)
      Future(throw new ForbiddenException)
      else {
        val json = Json.obj("agent" -> agentNumber)
        WS.url(s"http://$host:$port/records/search?page=1&pageSize=$size").withHeaders("Cookie" -> cookie.get).post(json).map(response => {
        if (response.status == 200) HistoryServerResponse(Json.parse(response.body) \ "records")
        else if (response.status == 403) throw new ForbiddenException
        else throw new WebServiceException(s"Request to history web service failed with status ${response.status} and message ${response.body}")
      })
    }
  }

  def getUserCallHistory(size: Int, interface: String): Future[HistoryServerResponse] = {
    if (cookie.isEmpty)
      Future(throw new ForbiddenException)
    else {
      val json = Json.obj("interface" -> interface)
      WS.url(s"http://$host:$port/history?size=$size").withHeaders("Cookie" -> cookie.get).post(json).map(response => {
        if (response.status == 200) HistoryServerResponse(Json.parse(response.body))
        else if (response.status == 403) throw new ForbiddenException
        else throw new WebServiceException(s"Request to history web service failed with status ${response.status} and message ${response.body}")
      })
    }
  }

  def login(): Future[HistoryServerResponse] = {
    val json = Json.obj("login" -> username, "mdp" -> password)
    WS.url(s"http://$host:$port/login").withFollowRedirects(false).post(json).map(response => {
        if (response.status >= 200 && response.status < 400) {
          cookie = response.header("Set-Cookie")
          HistoryServerResponse(Json.obj())
        }
        else if (response.status == 403) throw new ForbiddenException
        else throw new WebServiceException(s"Request to history web service failed with status ${response.status} and message ${response.body}")
    })
  }

}

case class HistoryServerResponse(json: JsValue)

class ForbiddenException extends Exception("Cannot login to history server, probably wrong login/password")
class WebServiceException(message: String) extends Exception(message: String)