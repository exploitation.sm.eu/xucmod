package models

import play.api.http.Status
import play.api.libs.json._
import xivo.network.{XiVOWS, XiVOWSdef}

import scala.concurrent.duration.DurationInt
import scala.concurrent.{Await, Future}

case class XivoUser(id: Long, firstname: String, lastname: String, username: String, password: Option[String])

case class XivoCtiProfile(user_id: Int, cti_profile_id: Option[Int], enabled: Boolean)

object XivoCtiProfile {
  implicit val xivoReads = Json.reads[XivoCtiProfile]
}

trait XivoUserDao {

  def getUsers() : Future[List[XivoUser]]
  def getUser(userId: Long) : Future[Option[XivoUser]]

}

trait XivoUserDaoWs extends XivoUserDao {
  def getUsers() : Future[List[XivoUser]] = XivoUser.getUsers()
  def getUser(userId: Long) : Future[Option[XivoUser]] = XivoUser.getUser(userId)
}

object XivoUser extends XivoUserDao {

  protected[models] var ws: XiVOWSdef = XiVOWS

  val timeout = 10 seconds

  implicit val context = scala.concurrent.ExecutionContext.Implicits.global
  implicit val xivoReads = Json.reads[XivoUser]

  def all(): List[XivoUser] =  Await.result(getUsers(), timeout)


  def filterNonCtiUsers(users: List[XivoUser]): List[XivoUser] = {
    users.filter(user => getUsersCtiProfile(user).enabled == true)
  }

  private def getUsersCtiProfile(user: XivoUser): XivoCtiProfile = {
    val responseFuture = ws.withWS(s"users/${user.id}/cti").get()
    val resultFuture = responseFuture map { response =>
      (response.json).validate[XivoCtiProfile].get
    }
    Await.result(resultFuture, timeout)
  }

  def getCtiProfile(userId:Long) : XivoCtiProfile = {
    val result = ws.withWS(s"users/$userId/cti").get.map ( _.json.validate[XivoCtiProfile].get)

    Await.result(result, timeout)
  }

  def getUser(userId: Long) : Future[Option[XivoUser]] =  ws.withWS(s"users/$userId").get.map { response =>
    response.status match {
      case Status.OK => Some(response.json.validate[XivoUser].get)
      case _ => None
    }
  }
  def getUsers() : Future[List[XivoUser]] = ws.withWS(s"users").get.map (response => (response.json \ "items").validate[List[XivoUser]].get)

}

