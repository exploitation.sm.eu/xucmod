package models

import org.xivo.cti.model.PhoneHintStatus
import org.xivo.cti.model.DirectoryEntry
import play.api.libs.json._
import play.api.libs.functional.syntax._
import scala.collection.mutable.Buffer


case class RichEntry(status: PhoneHintStatus, fields: Buffer[String])
object RichEntry {
  implicit val richEntryWrites = (
    (__ \ "status").write[Int].contramap { (a: PhoneHintStatus) => a.getHintStatus() } and
    (__ \ "entry").write[Buffer[String]])(unlift(RichEntry.unapply))
}

class RichDirectoryResult(val headers : List[String]) {
  var entries : List[RichEntry] = List()
  def getEntries(): List[RichEntry] = entries
  def add(entry: RichEntry) = {
    entries = entries :+ entry
  }
}

object RichDirectoryResult {
  implicit val rdrWrites = new Writes[RichDirectoryResult] {
    def writes(rdr: RichDirectoryResult): JsValue =
      JsObject(
          Seq("headers" -> Json.toJson(rdr.headers),
              "entries" -> Json.toJson(rdr.entries))
          )

  }

  def toJson(rdResult : RichDirectoryResult) = Json.toJson(rdResult)
}
