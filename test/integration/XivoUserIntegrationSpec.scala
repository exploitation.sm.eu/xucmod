package integration

import models.XivoUser
import org.scalatest.concurrent.ScalaFutures
import org.scalatestplus.play.OneAppPerSuite
import play.api.test.FakeApplication
import xuctest.{IntegrationTest, BaseTest}


class XivoUserIntegrationSpec extends BaseTest with OneAppPerSuite with ScalaFutures {

  implicit override lazy val app: FakeApplication =
    FakeApplication(
      additionalConfiguration = xivoIntegrationConfig
    )

  "xuc" should {

    "be able to get a list of users from xivo" taggedAs(IntegrationTest) in {
      val users = XivoUser.all
      users.length should be > 0
    }

    "be able to get a list of users with CtiProfile activated from xivo" taggedAs(IntegrationTest) in {
      val users = XivoUser.filterNonCtiUsers(XivoUser.all)
      users.length should be > 0
    }

    "be able to get a user from xivo" taggedAs(IntegrationTest) in {
      val fResult = XivoUser.getUser(142)

      fResult.futureValue should be (Some(XivoUser(142,"acd95","acd95","acd95",Some("0000"))))
    }
  }
}