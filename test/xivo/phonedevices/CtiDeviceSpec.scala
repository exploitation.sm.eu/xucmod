package xivo.phonedevices

import akkatest.TestKitSpec
import akka.testkit.TestProbe
import org.xivo.cti.MessageFactory
import org.json.JSONObject
import play.api.test.FakeApplication
import play.api.test.Helpers._
import xivo.models.Line
import scala.collection.JavaConverters.asScalaIteratorConverter
import org.xivo.cti.model.Endpoint.EndpointType
import org.xivo.cti.model.Endpoint

class CtiDeviceSpec extends TestKitSpec("CtiDeviceSpec") {

  val messageFactory = new MessageFactory()
  var ctiDevice: CtiDevice = _

  val testConfig: Map[String, Any] = { Map("xivohost" -> "xivoHostIp") }
  val line = Line(1, "default", "SIP", "callerLine", None)

  override def beforeAll {
    ctiDevice = new CtiDevice(line)
  }

  "send dial cti command upon receipt of dial websocket command message" in {
    running(FakeApplication(additionalConfiguration = testConfig)) {
      val destination = "3567"

      val sender = TestProbe()

      ctiDevice.dial(destination, sender.ref)

      sender.expectMsg(StandardDial(line, destination, "xivoHostIp"))
    }
  }

  "send hangup command upon receipt of hangup websocket command message" in {
    val expectedResponse = messageFactory.createHangup()

    val sender = TestProbe()
    ctiDevice.hangup(sender.ref)

    checkResponseMessage(expectedResponse, sender)
  }

  "send attended transfer cti command upon receipt of attended transfer websocket command message" in {
    val destination = "3568"
    val expectedResponse = messageFactory.createAttendedTransfer(destination)

    val sender = TestProbe()
    ctiDevice.attendedTransfer(destination, sender.ref)

    checkResponseMessage(expectedResponse, sender)
  }

  "send complete transfer cti command upon receipt of complete transfer websocket command message" in {
    val expectedResponse = messageFactory.createCompleteTransfer()

    val sender = TestProbe()
    ctiDevice.completeTransfer(sender.ref)

    checkResponseMessage(expectedResponse, sender)
  }

  "send cancel transfer cti command upon receipt of cancel transfer websocket command message" in {
    val expectedResponse = messageFactory.createCancelTransfer()

    val sender = TestProbe()
    ctiDevice.cancelTransfer(sender.ref)

    checkResponseMessage(expectedResponse, sender)
  }

  private def checkResponseMessage(expectedResponse: JSONObject, sender: TestProbe): Unit = {

    val response = sender.expectMsgClass(classOf[JSONObject])
    val keys: List[String] = response.keys().asScala.collect {
      case s: String => s
    }.toList

    keys.foreach {
      key =>
        if (key != "commandid")
          response.get(key).toString should be(expectedResponse.get(key).toString)
    }
  }

}