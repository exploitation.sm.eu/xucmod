package xivo.phonedevices

import akka.actor.ActorRef
import akka.testkit.TestProbe
import akkatest.TestKitSpec
import org.scalatest.mock.MockitoSugar
import org.xivo.cti.model.Endpoint
import play.api.test.FakeApplication
import play.api.test.Helpers._
import org.mockito.Mockito.{when, verify}
import services.XucAmiBus.DialActionRequest
import services.config.ConfigRepository
import xivo.models.Line

class DeviceAdapterSpec extends TestKitSpec("deviceAdapter") with MockitoSugar {

  class Helper(val outboundLength:Int) {

    val mockDial = mock[(String, ActorRef)=>Unit]

    class Device extends DeviceAdapter {
      override def dial(destination: String, sender: ActorRef): Unit = mockDial(destination, sender)
      override def conference(sender: ActorRef): Unit = {}
      override def attendedTransfer(destination: String, sender: ActorRef): Unit = {}
      override def completeTransfer(sender: ActorRef): Unit = {}
      override def hold(sender: ActorRef): Unit = {}
      override def hangup(sender: ActorRef): Unit = {}
      override def cancelTransfer(sender: ActorRef): Unit = {}
      override def answer(sender: ActorRef): Unit = {}
    }

    val sender = TestProbe()
    val outDevice = new Device()

    val xivoHost = "10.20.30.50"

    val fApp = FakeApplication(additionalConfiguration = Map("xuc.outboundLength" -> outboundLength, "xivohost" -> xivoHost))
  }

  "a device adapter" should {
    "return an outbound call if number length > configured length and outboutqueue configured" in new Helper(6) {
      val destination = "01234567"
      val agentId = 58
      val queueNumber = "3000"
      running(fApp) {
        outDevice.odial(destination, agentId, queueNumber, sender.ref)

        sender.expectMsg(OutboundDial(destination, agentId, queueNumber, xivoHost))
      }
    }

    "call adapter dial if destination length < configured length and outboutqueue configured" in new Helper(5) {
      val destination = "1250"
      val queueNumber = "3000"

      running(fApp) {
        outDevice.odial(destination, 58, queueNumber, sender.ref)

        verify(mockDial)(destination, sender.ref)
      }

    }
    "call adapter dial if destination length > configured length and outboutqueue not configured" in new Helper(6) {
      val destination = "12504567987"
      val queueNumber = ""

      running(fApp) {
        outDevice.odial(destination, 58, queueNumber, sender.ref)

        verify(mockDial)(destination, sender.ref)
      }
    }
  }

  "StandardDial" should {
    "return DialActionRequestion when the requested line exists" in {
      val line = Line(1, "default", "SIP", "callerLine", None)

      val sd = StandardDial(line, "0230210000", "xivoHost")

      sd.toAmi shouldBe Some(DialActionRequest("SIP/callerLine", "default", "0230210000", "xivoHost"))
    }

  }
}
