package xivo.network

import akka.actor.Props
import akka.io.Tcp
import akka.testkit.{TestActorRef, TestProbe}
import akka.util.ByteString
import akkatest.TestKitSpec
import models.XucUser
import org.mockito.Mockito.stub
import org.scalatest.mock.MockitoSugar
import org.scalatestplus.play.OneAppPerSuite
import org.xivo.cti.MessageFactory
import org.xivo.cti.message.{LoginCapasAck, PhoneStatusUpdate}
import org.xivo.cti.model.Endpoint
import org.xivo.cti.model.Endpoint.EndpointType
import play.api.test.FakeApplication
import play.api.test.Helpers.running
import services.Start
import xivo.websocket.LinkState._
import xivo.websocket.LinkStatusUpdate

import scala.concurrent.duration.DurationInt

class CtiLinkSpec extends TestKitSpec("CtiLinkSpec") with MockitoSugar with OneAppPerSuite {
  import xivo.network.CtiLink._

  val noLogs_and_noXivo = Map(
    "xivocti.host"->"192.0.0.0",
    "xivocti.port"->"9999",
    "akka.loglevel" -> "OFF",
    "logger.application" -> "OFF",
    "logger.play" -> "OFF",
    "akka.log-dead-letters" -> 0,
    "akka.log-dead-letters-during-shutdown" -> "off"
  )

  implicit override lazy val app: FakeApplication =
    FakeApplication(
      additionalConfiguration = noLogs_and_noXivo
    )

  class Helper {

    val messageProcessor: MessageProcessor = mock[MessageProcessor]
    val messageFactory = new MessageFactory()
    val parent = TestProbe()
    val ctiLoginStep = TestProbe()
    def actor(username : String) = {
      running(app) {
        val a = TestActorRef[CtiLink](Props(new CtiLink(username, messageProcessor)), parent.ref, "testCtiLink")
        a.underlyingActor.ctiLoginStep = ctiLoginStep.ref
        (a, a.underlyingActor)
      }
    }
  }
  "A CtiLink actor" should {

    "forward login messages to the ctiLoginStepActor" in new Helper {
        val (ref, ctiLink) = actor("loginmessage")

        val message = new LoginCapasAck()

        ctiLink.processDecodedMessage(message)
        ctiLoginStep.expectMsg(message)
    }

    "parse received messages and send them to its parent except first login messages" in new Helper {
        val (ref, _) = actor("firstlogin")
        val message = "{.... json phone status udpate ....}"
        val phoneStatus = new PhoneStatusUpdate()
        stub(messageProcessor.processBuffer(message)).toReturn(Array(phoneStatus))

        ref ! Tcp.Received(ByteString(message))

        parent.expectMsgClass(classOf[PhoneStatusUpdate])
    }

    "not send received cti objects before the network connection is initialized" in new Helper {
      val (ref, ctiLink) = actor("receive")
      val ctiMessage = messageFactory.createDial(new Endpoint(EndpointType.PHONE, "1234"))
      val connection = TestProbe()
      ctiLink.connection = connection.ref
      ref ! ctiMessage

      connection.expectNoMsg(100 millis)
    }

    "send received cti objects to the initialized network connection" in new Helper {
      val (ref, ctiLink) = actor("sendreceive")
      val ctiMessage = messageFactory.createDial(new Endpoint(EndpointType.PHONE, "1234"))
      val connection = TestProbe()
      ctiLink.connection = connection.ref
      ctiLink.receiveNoBufferize(ctiMessage)
      connection.expectMsg(Tcp.Write(ByteString("%s\n".format(ctiMessage.toString)), ctiLink.Ack))
    }

    "close connection on receiving restart message and set reconnect timer to restart timer" in new Helper {
      val (ref, ctiLink) = actor("closeconnection")
      val connection = TestProbe()
      ctiLink.connection = connection.ref

      ref ! RestartRequest

      ctiLink.reconnectTimer should be(CtiLink.RestartReconnectTimer)
      connection.expectMsg(Tcp.Close)

    }
    "send a linkstatus down message on peer disconnect" in new Helper {
      val (ref, ctiLink) = actor("linkstatusdown")

      ref ! Tcp.PeerClosed

      parent.expectMsg(LinkStatusUpdate(down))
    }

    "send a linkstatus up to parent on connect" in new Helper {
      val (ref, ctiLink) = actor("linkstatusup")

      ref ! Tcp.Connected(null,null)

      parent.expectMsg(LinkStatusUpdate(up))

    }
    "updates user field with user info received in start message and send start login to login step" in new Helper {
      val (ref, ctiLink) = actor("start")

      val user = XucUser("username","1234")
      ref ! Start(user)

      ctiLoginStep.expectMsg(StartLogin(user))

    }
  }

}
