package xivo.network

import akkatest.TestKitSpec
import org.json.JSONObject
import org.mockito.Mockito.stub
import org.mockito.Mockito.verify
import org.scalatest.BeforeAndAfterAll
import org.scalatest.WordSpec
import org.scalatest.matchers.ShouldMatchers
import org.scalatest.mock.MockitoSugar
import org.xivo.cti.MessageFactory
import org.xivo.cti.message.LoginCapasAck
import org.xivo.cti.message.LoginIdAck
import org.xivo.cti.message.LoginPassAck
import akka.actor.ActorRef
import akka.actor.ActorSystem
import akka.actor.Props
import akka.actor.actorRef2Scala
import akka.testkit.{TestActorRef, DefaultTimeout, ImplicitSender, TestKit}
import models.XucUser
import play.api.test.FakeApplication
import play.api.test.Helpers.running
import xivo.xuc.XucConfig
import org.xivo.cti.model.Capacities
import org.xivo.cti.model.UserStatus
import org.xivo.cti.model.Xlet

class CtiLoginStepSpec
  extends TestKitSpec("CtiLinkSpec")
  with MockitoSugar {
  val user = XucUser("thierry", "wer554.", Some(2002))
  val messageFactory = mock[MessageFactory]
  var ctiLoginStep: ActorRef = null

  class Helper {
    val messageFactory = mock[MessageFactory]
    def actor() = {
      running(FakeApplication()) {
        val a = TestActorRef(new CtiLoginStep(messageFactory))
        (a, a.underlyingActor)
      }
    }
  }

  override def beforeAll {
    running(FakeApplication()) {
      ctiLoginStep = system.actorOf(Props(new CtiLoginStep(messageFactory)), user.ctiUsername + "CtiLoginStep")
    }
  }

  "A CtiLoginStep actor" should {

    "send LoginId message to the Cti server when receiving StartLogin command message" in new Helper {
      val (ref,ctiLink) = actor()
      stub(messageFactory.createLoginId("Fred", "Fred" + XucConfig.XivoCtiIdentitySuffix)).toReturn(new JSONObject())

      ref ! StartLogin(XucUser("Fred","pwd"))
      expectMsgClass(classOf[JSONObject])
      verify(messageFactory).createLoginId("Fred", "Fred" + XucConfig.XivoCtiIdentitySuffix)
      ctiLink.user should be(XucUser("Fred","pwd"))
    }

    "send LoginPass message to the Cti server when receiving LoginIdAck message" in new Helper {
      val (ref,ctiLink) = actor()
      val user = XucUser("Joe","pwd")
      ctiLink.user = user
      val loginIdAck = new LoginIdAck()
      loginIdAck.sesssionId = "1564877"
      stub(messageFactory.createLoginPass(user.ctiPassword, loginIdAck.sesssionId)).toReturn(new JSONObject())

      ref ! loginIdAck
      expectMsgClass(classOf[JSONObject])
      verify(messageFactory).createLoginPass(user.ctiPassword, loginIdAck.sesssionId)

    }

    "send LoginCapas message to the Cti server when receiving LoginPassAck" in new Helper {
      val (ref,ctiLink) = actor()
      val user = XucUser("Joe","pwd",Some(12345))
      ctiLink.user = user
      val loginPassAck = new LoginPassAck()
      loginPassAck.capalist = new java.util.ArrayList[Integer]()
      val capaId = 3
      loginPassAck.capalist.add(capaId)
      stub(messageFactory.createLoginCapas(capaId, user.phoneNumber.toString)).toReturn(new JSONObject())

      ref ! loginPassAck

      expectMsgClass(classOf[JSONObject])
      verify(messageFactory).createLoginCapas(capaId, user.phoneNumber.toString)
    }

    "send LoggedOn message to its parent when receiving LoginCapasAck" in new Helper {
      val (ref,ctiLink) = actor()
      ctiLink.user = XucUser("thierry", "wer554.", Some(2002))

      val loginCapasAck = createLoginCapasWithUserStatues
      loginCapasAck.userId = "34"
      ref ! loginCapasAck
      expectMsg(LoggedOn(ctiLink.user, "34", loginCapasAck.capacities.getUsersStatuses()))
    }

  }

  private def createLoginCapasWithUserStatues: LoginCapasAck = {
    val loginCapasAck = new LoginCapasAck()

    loginCapasAck.capacities = new Capacities()
    val userStatuses: java.util.List[UserStatus] = java.util.Arrays.asList(new UserStatus("available"), new UserStatus("disconnected"))
    loginCapasAck.capacities.setUsersStatuses(userStatuses)
    val xlets = new java.util.LinkedList[Xlet]()
    loginCapasAck.xlets = xlets
    loginCapasAck
  }
}
