package xivo.services

import akka.testkit.TestActorRef
import akkatest.TestKitSpec
import models.{TokenRequest, Token, XivoUser}
import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat
import org.mockito.Mockito.when
import org.scalatest.mock.MockitoSugar
import play.api.libs.ws.{WSResponse, WSRequestHolder}
import play.api.test.FakeApplication
import play.api.test.Helpers._
import play.api.libs.json.Json
import services.config.ConfigRepository
import xivo.network.XiVOWSdef
import xivo.services.XivoAuthentication.Init

import scala.concurrent.Future

class XivoAuthenticationSpec extends TestKitSpec("XivoAuthenticationSpec")  with MockitoSugar {

  val appliConfig: Map[String, Any] = {
    Map(
      "xivohost" -> "xivoIP",
      "xivoAuthPort" -> 9497,
      "xivo.auth.defaultExpires" -> 1
    )
  }

  class Helper {

    val repo: ConfigRepository = mock[ConfigRepository]
    val xivoWS: XiVOWSdef = mock[XiVOWSdef]

    def actor() = {
      val a = TestActorRef(new XivoAuthentication(repo, xivoWS))
      (a, a.underlyingActor)
    }
  }

  "XivoAuthentication actor " should {
    "get token from xivo when requested " in new Helper {
      val fApp = FakeApplication(additionalConfiguration = appliConfig)
      running(fApp) {
        val (ref, _) = actor()
        val userId = 46
        val wsRequest = mock[WSRequestHolder]
        when(repo.getUser(46)).thenReturn(Some(XivoUser(46, "first", "last", "login", Some("pass"))))
        when(xivoWS.post("xivoIP", "0.1/token", Json.toJson(TokenRequest("xivo_users", 1)), "login", "pass"))
          .thenReturn(wsRequest)
        val response = mock[WSResponse]
        val date = new DateTime()
        val fmt = DateTimeFormat.forPattern("yyyy-MM-dd'T'HH:mm:ss.SSSSSS")
        when(response.json).thenReturn(
          Json.parse(
            s"""{"data":
              | {
              |   "issued_at": "${fmt.print(date)}",
              |   "token": "5ca1d522-3f6b-1db9-339a-731e61c38871",
              |   "auth_id": "authId",
              |   "expires_at": "${fmt.print(date.plusSeconds(1))}",
              |   "xivo_user_uuid": "userId"
              | }
              |}""".stripMargin))
        when(wsRequest.execute()).thenReturn(Future.successful(response))
        ref ! XivoAuthentication.GetToken(userId)

        expectMsg(
          Token("5ca1d522-3f6b-1db9-339a-731e61c38871",
                date.plusSeconds(1),
                date,
                "authId",
                "userId")
        )
      }
    }

    """use saved token""" in new Helper {
      val (ref, a) = actor()
      val date = new DateTime(2015, 11, 10, 1, 22, 3)
      val t = Token("token", date, date, "", "")
      ref ! Init(Map(46L -> t))
      ref ! XivoAuthentication.GetToken(46)

      expectMsg(Token("token", date, date, "", ""))
    }

    "renew token on expiration" in new Helper {
      val fApp = FakeApplication(additionalConfiguration = appliConfig)
      running(fApp) {
        val (ref, _) = actor()
        val userId = 46
        val wsRequest = mock[WSRequestHolder]
        when(repo.getUser(46)).thenReturn(Some(XivoUser(46, "first", "last", "login", Some("pass"))))
        when(xivoWS.post("xivoIP", "0.1/token", Json.toJson(TokenRequest("xivo_users", 1)), "login", "pass"))
          .thenReturn(wsRequest)
        val response = mock[WSResponse]
        val date = new DateTime()
        val fmt = DateTimeFormat.forPattern("yyyy-MM-dd'T'HH:mm:ss.SSSSSS")
        when(response.json)
          .thenReturn(
            Json.parse(
              s"""{"data":
                | {
                |   "issued_at": "${fmt.print(date)}",
                |   "token": "first",
                |   "auth_id": "authId",
                |   "expires_at": "${fmt.print(date.plusSeconds(1))}",
                |   "xivo_user_uuid": "userId"
                | }
                |}""".stripMargin))
          .thenReturn(
            Json.parse(
              s"""{"data":
                | {
                |   "issued_at": "${fmt.print(date)}",
                |   "token": "second",
                |   "auth_id": "authId",
                |   "expires_at": "${fmt.print(date.plusDays(1))}",
                |   "xivo_user_uuid": "userId"
                | }
                |}""".stripMargin))

        when(wsRequest.execute()).thenReturn(Future.successful(response))
        ref ! XivoAuthentication.GetToken(userId)
        expectMsgClass(classOf[Token])
        Thread.sleep(1000)
        ref ! XivoAuthentication.GetToken(userId)
        expectMsg(Token("second", date.plusDays(1), date, "authId", "userId"))

      }
    }


  }
}
