package xivo.models

import play.api.libs.json.Json
import play.api.test.{FakeApplication, PlaySpecification}
import xivo.data.dbunit.DBUtil


class AgentGroupSpec extends PlaySpecification {

  def xivoIntegrationConfig: Map[String, String] = {
    Map(
      "XivoWs.wsUser" -> "xivows",
      "XivoWs.wsPwd" -> "xivows",
      "XivoWs.host" -> "192.168.56.3",
      "XivoWs.port" -> "50051",
      "ws.acceptAnyCertificate" -> "true",
      "db.default.driver" -> "org.postgresql.Driver",
      "db.default.url" -> "jdbc:postgresql://127.0.0.1/testdata",
      "db.default.user" -> "test",
      "db.default.password" -> "test"
    )
  }

  "agent group" should {
    "be able to be retreived" in {
      running(FakeApplication(additionalConfiguration = xivoIntegrationConfig)) {
        DBUtil.setupDB("agentgroup.xml")

        val group1 = AgentGroup(Some(1), "group1", Some("First group"))

        val groups = AgentGroup.all()

        groups.length should be_==(2)
        groups should contain(group1)
      }
    }

    "be able to be transformed to json" in {
      val ag = AgentGroup(Some(34), "biggroup", Some("Big Group"))

      val agJson = Json.toJson(ag)

      agJson shouldEqual Json.obj(
        "id" -> 34,
        "name" -> "biggroup",
        "displayName" -> "Big Group")
    }
  }

}