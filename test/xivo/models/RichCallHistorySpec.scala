package xivo.models

import org.joda.time.{DateTime, Period}
import org.scalatest.mock.MockitoSugar
import org.mockito.Mockito.stub
import play.api.libs.json.{JsArray, Json}
import play.api.test.PlaySpecification
import services.config.ConfigRepository

class RichCallHistorySpec extends PlaySpecification with MockitoSugar {

  "A RichCallDetail" should {
    "be created from a CallDetail" in {
      ConfigRepository.repo = mock[ConfigRepository]
      stub(ConfigRepository.repo.userNameFromPhoneNb("4000")).toReturn(Some("j.doe"))
      stub(ConfigRepository.repo.userNameFromPhoneNb("4001")).toReturn(Some("l.luke"))

      val detail = CallDetail(new DateTime, Some(new Period(0 ,15, 12, 5)), "4000", "4001", CallStatus.Answered)

      RichCallDetail(detail) shouldEqual RichCallDetail(detail.start, detail.duration, "j.doe", "l.luke", detail.status)
    }
  }

  "A RichCallHistory" should {
    "be serialized to json" in {
      val date = RichCallDetail.format.parseDateTime("2014-01-01 08:00:00")
      val period = RichCallDetail.periodFormat.parsePeriod("00:12:14")
      val history = RichCallHistory(List(
        RichCallDetail(date, Some(period), "j.doe", "l.luke", CallStatus.Answered)
      ))

      Json.toJson(history) shouldEqual JsArray(List(
        Json.obj(
          "start" -> "2014-01-01 08:00:00",
          "duration" -> "00:12:14",
          "srcUsername" -> "j.doe",
          "dstUsername" -> "l.luke",
          "status" -> "answered"
        )
      ))
    }
  }

}
