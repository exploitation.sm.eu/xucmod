package controllers.xuc

import play.api.test.{FakeApplication, PlaySpecification, FakeRequest}

class WebSocketAppSpec extends PlaySpecification {

  def xivoIntegrationConfig: Map[String, String] = {
    Map(
      "XivoWs.wsUser" -> "xivows",
      "XivoWs.wsPwd" -> "xivows",
      "XivoWs.host" -> "xivo-integration",
      "XivoWs.port" -> "50051",
      "ws.acceptAnyCertificate" -> "true"
    )
  }

  "WebSocketApp" should {
    "reject request for empty username" in {
      running(FakeApplication(additionalConfiguration = xivoIntegrationConfig)) {

        implicit val fakeRequest = FakeRequest()

        val result = WebSocketApp.ctiChannel("", 0, "")

        result must not beNull
      }
    }
  }

}