package services

import akka.testkit.{TestFSMRef, TestProbe}
import akkatest.TestKitSpec
import models.XucUser
import org.joda.time.DateTime
import org.mockito.Mockito._
import org.scalatest.mock.MockitoSugar
import org.specs2.execute.Failure
import services.AgentStateFSM.{MAgentPaused, MAgentReady}
import services.agent.AgentStateTHandler
import services.config.AgentUserStatus
import services.config.ConfigDispatcher.LineConfigQueryByNb
import services.line.LineState
import xivo.ami.AgentCallUpdate
import xivo.ami.AmiBusConnector.{CallData, LineEvent}
import xivo.events.AgentState
import xivo.events.AgentState.{AgentDialing, AgentRinging}
import xivo.xucami.models.MonitorState
import xivo.xucami.models.MonitorState.MonitorState
import scala.concurrent.duration.DurationInt

class AgentStateFSMSpec extends TestKitSpec("FSMServiceTest")
  with MockitoSugar {
  import services.AgentStateFSM._
  import xivo.events.AgentState.CallDirection._
  import xivo.events.AgentState.CallType._
  import xivo.events.AgentState.{AgentLoggedOut, AgentLogin, AgentOnCall, AgentOnPause, AgentOnWrapup, AgentReady}

  trait Events {
    this: DefaultFsm =>

    def readyEvent      = AgentReady(agentId, time, phoneNb, queues, cause)
    def loginEvent      = AgentLogin(agentId,time,phoneNb,queues,cause)
    def incomingCallEvent = AgentOnCall(agentId, time, acd=false, Incoming, CallTypeUnknown, phoneNb, queues, onPause = false, cause)
    def incomingCallEventOnPause = AgentOnCall(agentId, time, acd=false, Incoming, CallTypeUnknown, phoneNb, queues,  onPause = true, cause)
    def incomingAcdCallEvent = AgentOnCall(agentId, time, acd=true, Incoming, CallTypeUnknown, phoneNb, queues, onPause = false, cause)
    def incomingAcdCallEventOnPause = AgentOnCall(agentId, time, acd=true, Incoming, CallTypeUnknown, phoneNb, queues, onPause = true, cause)
    def onOutGoingCallEvent = AgentOnCall(agentId, time, acd=false, Outgoing, CallTypeUnknown, phoneNb, queues, onPause = false, cause)
    def onOutGoingCallEventOnPause = AgentOnCall(agentId, time, acd=false, Outgoing, CallTypeUnknown, phoneNb, queues, onPause = true, cause)
    def onOutGoingAcdCallEvent = AgentOnCall(agentId, time, acd=true, Outgoing, CallTypeUnknown, phoneNb, queues, onPause = false, cause)
    def loggedOutEvent  = AgentLoggedOut(agentId, time, phoneNb, queues, cause)
    def ringingEvent    = AgentRinging(agentId, time, phoneNb, queues, cause)
    def dialingEvent    = AgentDialing(agentId, time, phoneNb, queues, cause)
    def pauseEvent      = AgentOnPause(agentId, time, phoneNb, queues, cause)
    def wrapupEvent     = AgentOnWrapup(agentId, time, phoneNb, queues, cause)


    def lineEventDialing = LineEvent(phoneNb.toInt, CallData("1301111.3", LineState.ORIGINATING, phoneNb))
    def lineEventEstablished = LineEvent(phoneNb.toInt, CallData("1305555.3", LineState.UP, phoneNb))
    def lineEventEstablishedMasquerade = LineEvent(phoneNb.toInt, CallData("1304444.450", LineState.UP, phoneNb,variables=Map("Masquerade" -> "1")))
    def lineEventEstablishedOtherEnd = LineEvent(phoneNb.toInt, CallData("1305555.3", LineState.UP, "88888"))
    def lineEventEstablishedOtherEndOutboundData = LineEvent(phoneNb.toInt, CallData("1305555.3", LineState.UP, "88888", variables=Map("XUC_OUTBOUND" -> "OtherEnd")))
    def lineEventAcdEstablished = LineEvent(phoneNb.toInt, CallData("1305555.3", LineState.UP, phoneNb, variables=Map("XIVO_QUEUENAME"->"yellow")))
    def lineEventHungup = LineEvent(phoneNb.toInt, CallData("1306666.3", LineState.HUNGUP, phoneNb))
    def lineEventHungupForOtherEnd = LineEvent(phoneNb.toInt, CallData("1306666.3", LineState.HUNGUP, "88888"))
    def lineEventRinging = LineEvent(phoneNb.toInt, CallData("1302222.3", LineState.RINGING, phoneNb))
    def lineEventRingingOtherEnd = LineEvent(phoneNb.toInt, CallData("1302222.3", LineState.RINGING, "88888"))
    def lineEventRingingOutBoundDial = LineEvent(phoneNb.toInt, CallData("1302222.3", LineState.RINGING, phoneNb, variables=Map("XUC_CALLTYPE" -> "OutboundOriginate")))
    def lineEventRingingOriginate = LineEvent(phoneNb.toInt, CallData("1302222.3", LineState.RINGING, phoneNb, variables=Map("XUC_CALLTYPE" -> "Originate")))
    def lineEventEstablishedOutBoundDial = LineEvent(phoneNb.toInt, CallData("1302222.3", LineState.UP, phoneNb, variables=Map("XUC_CALLTYPE" -> "OutboundOriginate")))
    def lineEventOtherEndEstablishedOutBoundDial = LineEvent(phoneNb.toInt, CallData("1302222.3", LineState.UP, "88888", variables=Map("XUC_OUTBOUND" -> "OtherEnd","XUC_CALLTYPE" -> "OutboundOriginate")))
    def lineEventOtherEndEstablishedOriginate = LineEvent(phoneNb.toInt, CallData("1302222.3", LineState.UP, "88888", variables=Map("XUC_CALLTYPE" -> "Originate","XUC_OTHER_END_UP" -> "1")))
    def lineEventEstablishedFromDialParty = LineEvent(phoneNb.toInt, CallData("1302222.3", LineState.UP, phoneNb, variables=Map("XUC_OUTBOUND" -> "OtherEnd")))
    def lineEventIncomingHungUp = LineEvent(phoneNb.toInt, CallData("1304444.3", LineState.HUNGUP, phoneNb))


  }
  abstract class DefaultFsm {
    val agentId: Int
    val phoneNb: String
    val time: DateTime = new DateTime
    val cause: Option[String]
    val queues: List[Int]

    val defaultUser = XucUser("agentun", "agentun", Some(1001))

    val configRequestMsg = LineConfigQueryByNb(defaultUser.phoneNumber.get.toString);

    val eventBus = mock[XucEventBus]

    val subscribeHandler = mock[()=>Unit]
    val publishHandler = mock[()=>Unit]

    trait TestTransitionHandler  extends AgentStateTHandler {
      this: AgentStateFSM =>

      class TLsub extends Lsubs {
        override def subscribe :TransitionHandler = {
          case _ => subscribeHandler()
        }
      }
      class TEPub extends EventPublisher {
        override def publish :TransitionHandler = {
          case _ => publishHandler()
        }
      }
      class TransPub extends TransitionPublisher {
        override def publish :TransitionHandler = {
          case _ => publishHandler()
        }
      }
      override lazy val lineEventSubs = new TLsub
      override lazy val eventPublisher: EventPublisher = new TEPub
      override lazy val transitionPublisher: TransitionPublisher = new TransPub

    }

    def fsm(state: Option[MAgentStates] = None, data: Option[MContext] = None) = {
      val agentStatusFSM = TestFSMRef(new AgentStateFSM(eventBus) with TestTransitionHandler)
      state.foreach(agentStatusFSM.setState(_))
      data.foreach(d => agentStatusFSM.setState(stateData = d))
      agentStatusFSM
    }

    def checkSavedAgentState(data: MContext, agentState: AgentState) =  data match {
        case AgentStateContext(state, _, _,_,_) => state should be(agentState)
        case _ => Failure("agent state not saved")
    }

    def checkSavedCause(data: MContext, cause: String) = {
      data match {
        case AgentStateContext(_, status, _,_,_) => status should be(cause)
        case _ => Failure("cause not saved")
      }
    }

    def checkSavedMonitorState(data: MContext, state: MonitorState) = {
      data match {
        case AgentStateContext(_, _, lastState,_,_) => lastState should be(state)
        case _ => Failure("monitor state not saved")
      }
    }

    def checkSavedCallId(data: MContext, callId: String) = {
      data match {
        case AgentStateContext(_, _, _,lastCallId,_) => lastCallId should be(callId)
        case _ => Failure("callId not saved")
      }
    }

    def checkSavedPhoneNumber(data: MContext, phoneNumber: String) = {
      data match {
        case AgentStateContext(_, _, _,_,phoneNb) => phoneNb should be(phoneNumber)
        case _ => Failure("phoneNumber")
      }

    }
  }

  class SimpleFsmTests(initState:MAgentStates, data:MContext) extends DefaultFsm with Events {
    val agentId = 56
    val phoneNb = "7788"
    val queues = List()
    val cause = Some("")



    private def check(state:MAgentStates, event:AgentState) = {
      val agentStatusMachine = fsm(Some(initState), Some(data))

      agentStatusMachine ! event
      agentStatusMachine.stateName should be(state)
      checkSavedAgentState(agentStatusMachine.stateData,  event)
    }

    def onEventReady =  "on event Ready save state change AgentReady" in (check(MAgentReady, readyEvent))

    def onEventWrapup =  "on event Wrapup save state change to wrapup" in (check(MAgentOnWrapup, wrapupEvent))

    def onEventLoggedOut =  "on event Logged out save state change to logged out" in (check(MAgentLoggedOut, loggedOutEvent))

    def onEventPaused = "on event Paused save state change to pause"  in (check(MAgentPaused, pauseEvent))

    def onEventLogin = "on event agent login save state change to login" in (check(MAgentLogin, loginEvent))

  }

  class GenericFsm extends DefaultFsm with Events {
    val agentId = 56
    val phoneNb = "7788"
    val queues = List()
    val cause = Some("")

    val tasm =  TestFSMRef(new AgentStateFSM(eventBus) with TestTransitionHandler)
    tasm.setState(MAgentOnCall)
  }

  "Agent state machine" should {
    "call line subscriber on any transition" in new GenericFsm {
      tasm ! readyEvent


      verify(subscribeHandler,atLeastOnce)()
    }
    "call event publisher on any transition" in new GenericFsm {
      tasm ! readyEvent

      verify(publishHandler,atLeastOnce)()
    }
  }
  //-----------------------------------------------------//
  //------------------ Init state -----------------------//
  //-----------------------------------------------------//
  "Agent State Machine in init state" should {

    class InitFsm(presence: String="", monitorState: MonitorState = MonitorState.DISABLED) extends DefaultFsm with Events {
      val agentId = 56
      val phoneNb = "7788"
      val queues = List()
      val cause = Some(presence)

      val agentStatusMachine = fsm()
    }

    """ on event login
        - change state to login
        - update context with phone number and event
    """ in new InitFsm {

      agentStatusMachine ! loginEvent

      agentStatusMachine.stateName shouldBe MAgentLogin
      checkSavedPhoneNumber(agentStatusMachine.stateData, loginEvent.phoneNb)
      checkSavedAgentState(agentStatusMachine.stateData, loginEvent)
    }
    """on event ready
        - change state to ready
        - update context with phone number and event""" in new InitFsm {
      agentStatusMachine ! readyEvent

      agentStatusMachine.stateName shouldBe MAgentReady
      checkSavedPhoneNumber(agentStatusMachine.stateData, readyEvent.phoneNb)
      checkSavedAgentState(agentStatusMachine.stateData, readyEvent)
    }

    """On event Pause
        - change state to pause
        - update context with phone number and event""" in new InitFsm {

      agentStatusMachine ! pauseEvent

      agentStatusMachine.stateName shouldBe MAgentPaused
      checkSavedPhoneNumber(agentStatusMachine.stateData, readyEvent.phoneNb)
      checkSavedAgentState(agentStatusMachine.stateData, pauseEvent)

    }
    """On event logged out
       - Change state to logged out
       - update context with event""" in new InitFsm {

      agentStatusMachine ! loggedOutEvent

      agentStatusMachine.stateName should be(MAgentLoggedOut)
      checkSavedAgentState(agentStatusMachine.stateData, loggedOutEvent)
    }
  }

  //-----------------------------------------------------//
  //----------------Login state -------------------------//
  //-----------------------------------------------------//
  "Agent State Machine in login state" should {

    val testMachine = new SimpleFsmTests(MAgentLogin, AgentStateContext(AgentLogin(12, new DateTime, "3000", List()), "outToLunch", MonitorState.ACTIVE, phoneNumber = "3000"))

    testMachine.onEventLoggedOut
    testMachine.onEventPaused
    testMachine.onEventReady

  }


  //-----------------------------------------------------//
  //----------------Ready state -------------------------//
  //-----------------------------------------------------//
  "Agent State Machine in ready state" should {
    class ReadyFsm(presence: String="", monitorState: MonitorState = MonitorState.DISABLED) extends DefaultFsm with Events {
      val agentId = 890
      val phoneNb = "9933"
      val queues = List()
      val cause = Some(presence)

      val agentStatusMachine = fsm(Some(MAgentReady), Some(AgentStateContext(AgentReady(agentId, new DateTime(), phoneNb, List()), presence, monitorState, phoneNumber = phoneNb)))
    }

    " on line event ring originated change state to agent dialing save state" in new ReadyFsm {
      agentStatusMachine ! lineEventDialing

      agentStatusMachine.stateName shouldBe MAgentDialing
      checkSavedAgentState(agentStatusMachine.stateData, dialingEvent)
    }

    "On line event ringing publish state" in new ReadyFsm {
      agentStatusMachine ! lineEventRinging

      agentStatusMachine.stateName shouldBe MAgentRinging
      checkSavedAgentState(agentStatusMachine.stateData, ringingEvent)
    }
    "on line event ringing other end stay" in new ReadyFsm {
      agentStatusMachine ! lineEventRingingOtherEnd

      agentStatusMachine.stateName shouldBe MAgentReady
    }

    "on line event oubound dial change state to dialing" in new ReadyFsm {
      agentStatusMachine ! lineEventRingingOutBoundDial

      agentStatusMachine.stateName shouldBe MAgentDialing
      checkSavedAgentState(agentStatusMachine.stateData, dialingEvent)
    }
    "on line event originate change state to dialing" in new ReadyFsm {
      agentStatusMachine ! lineEventRingingOriginate

      agentStatusMachine.stateName shouldBe MAgentDialing
      checkSavedAgentState(agentStatusMachine.stateData, dialingEvent)
    }

    """on event on call publish on call event and goto on call state""" in new ReadyFsm {
      agentStatusMachine ! lineEventEstablished

      agentStatusMachine.stateName shouldBe MAgentOnCall
      checkSavedAgentState(agentStatusMachine.stateData, incomingCallEvent)
    }

    """on event on call for another line do nothing""" in new ReadyFsm {
      agentStatusMachine ! lineEventEstablishedOtherEnd

      agentStatusMachine.stateName shouldBe MAgentReady
    }



    val testMachine = new SimpleFsmTests(MAgentReady, AgentStateContext(AgentReady(12, new DateTime, "3000", List()), "outToLunch", MonitorState.ACTIVE, phoneNumber = "3000"))

    testMachine.onEventLoggedOut
    testMachine.onEventPaused
    testMachine.onEventWrapup
  }
  //-----------------------------------------------------//
  //----------------Dialing state -------------------------//
  //-----------------------------------------------------//
  "Agent State Machine in dialing state" should {

    class DialingFsm(presence: String="", monitorState: MonitorState = MonitorState.DISABLED) extends DefaultFsm with Events {
      val agentId = 765
      val phoneNb = "1122"
      val queues = List()
      val cause = Some(presence)

      val agentStatusMachine =  fsm(Some(MAgentDialing), Some(AgentStateContext(dialingEvent, presence, monitorState, phoneNumber = phoneNb)))
    }

    """On event paused goto dialing on pause and stay""" in new DialingFsm {
      agentStatusMachine ! pauseEvent

      agentStatusMachine.stateName shouldBe MAgentDialingOnPause
      checkSavedAgentState(agentStatusMachine.stateData, dialingEvent)

    }

    """on event on call publish on call event and goto on call state""" in new DialingFsm {
      agentStatusMachine ! lineEventEstablished

      agentStatusMachine.stateName shouldBe MAgentOnCall
      checkSavedAgentState(agentStatusMachine.stateData, onOutGoingCallEvent)
    }
    "on event established other end stay" in new DialingFsm {
      agentStatusMachine ! lineEventEstablishedOtherEndOutboundData
      agentStatusMachine.stateName shouldBe MAgentDialing
    }
    """on event on call outbound orginate stay in dialing and do not publish state""" in new DialingFsm {
      agentStatusMachine ! lineEventEstablishedOutBoundDial

      agentStatusMachine.stateName shouldBe MAgentDialing

    }
    "on other end established for outbound dial save state to on call with acd true change state to on call" in new DialingFsm {
      agentStatusMachine ! lineEventOtherEndEstablishedOutBoundDial

      agentStatusMachine.stateName shouldBe MAgentOnCall
      checkSavedAgentState(agentStatusMachine.stateData, onOutGoingAcdCallEvent)
    }
    "on other end established for originate save state to on call with acd false change state to on call" in new DialingFsm {
      agentStatusMachine ! lineEventOtherEndEstablishedOriginate

      agentStatusMachine.stateName shouldBe MAgentOnCall
      checkSavedAgentState(agentStatusMachine.stateData, onOutGoingCallEvent)
    }
    "on dial party established for agent number stay" in new DialingFsm {
      agentStatusMachine ! lineEventEstablishedFromDialParty

      agentStatusMachine.stateName shouldBe MAgentDialing

    }
    "on line event hangup got back to ready and save state" in new DialingFsm {
      agentStatusMachine ! lineEventHungup

      agentStatusMachine.stateName shouldBe MAgentReady

      checkSavedAgentState(agentStatusMachine.stateData, readyEvent)
    }
    "on line event hangup other end stay" in new DialingFsm {
      agentStatusMachine ! lineEventHungupForOtherEnd

      agentStatusMachine.stateName shouldBe MAgentDialing

    }
    val testMachine = new SimpleFsmTests(MAgentDialing, AgentStateContext(AgentDialing(12, new DateTime, "3000", List()), "outToLunch", MonitorState.ACTIVE, phoneNumber = "3000"))

    testMachine.onEventLoggedOut

  }
  //-----------------------------------------------------//
  //----------------Ringing state------------------------//
  //-----------------------------------------------------//
  "Agent State Machine on ringing state" should {
    class RingingFsm(presence: String="", monitorState: MonitorState = MonitorState.DISABLED) extends DefaultFsm with Events {
      val agentId = 666
      val phoneNb = "3344"
      val queues = List()
      val cause = Some(presence)
      val agentStatusMachine =  fsm(Some(MAgentRinging), Some(AgentStateContext(ringingEvent, presence, monitorState, phoneNumber = phoneNb)))
    }

    " On event established change state publish state with MonitorState" in new RingingFsm(monitorState = MonitorState.ACTIVE) {
      agentStatusMachine ! lineEventEstablished

      agentStatusMachine.stateName shouldBe MAgentOnCall
      checkSavedAgentState(agentStatusMachine.stateData, incomingCallEvent)
    }
    "on event acd established change state to On call and publish event acd call event" in new RingingFsm(monitorState = MonitorState.PAUSED) {
      agentStatusMachine ! lineEventAcdEstablished

      agentStatusMachine.stateName shouldBe MAgentOnCall

      checkSavedAgentState(agentStatusMachine.stateData, incomingAcdCallEvent)

    }
    "on event hangup change state to ready and save state" in new RingingFsm() {
      agentStatusMachine ! lineEventHungup

      agentStatusMachine.stateName shouldBe MAgentReady

      checkSavedAgentState(agentStatusMachine.stateData,  readyEvent)

    }
    "on event pause change state to ringing on pause" in new RingingFsm() {
      agentStatusMachine ! pauseEvent

      agentStatusMachine.stateName shouldBe MAgentRingingOnPause
      checkSavedAgentState(agentStatusMachine.stateData, ringingEvent)
    }

    val testMachine = new SimpleFsmTests(MAgentRinging, AgentStateContext(AgentRinging(12, new DateTime, "3000", List()), "outToLunch", MonitorState.ACTIVE, phoneNumber = "3000"))

    testMachine.onEventLoggedOut

  }
  //-----------------------------------------------------//
  //----------------Ringing on pause state---------------//
  //-----------------------------------------------------//
  "Agent State Machine on ringing on pause state" should {
    class RingingOnPauseFsm(presence: String="", monitorState: MonitorState = MonitorState.DISABLED) extends DefaultFsm with Events {
      val agentId = 7650
      val phoneNb = "5566"
      val queues = List()
      val cause = Some(presence)

      val agentStatusMachine =  fsm(Some(MAgentRingingOnPause), Some(AgentStateContext(ringingEvent, presence, monitorState, phoneNumber = phoneNb)))
    }

    """On line event up and terminating
        - change state on call on pause
        - update last received state with call id""" in new RingingOnPauseFsm {

      agentStatusMachine ! lineEventEstablished

      agentStatusMachine.stateName shouldBe MAgentOnCallOnPause
      checkSavedCallId(agentStatusMachine.stateData,lineEventEstablished.call.callId)
      checkSavedAgentState(agentStatusMachine.stateData, incomingCallEvent)
    }

    "on line event up acd established change state on call on pause update callid save state" in new RingingOnPauseFsm() {

      agentStatusMachine ! lineEventAcdEstablished
      agentStatusMachine.stateName shouldBe MAgentOnCallOnPause
      checkSavedCallId(agentStatusMachine.stateData,lineEventAcdEstablished.call.callId)
      checkSavedAgentState(agentStatusMachine.stateData, incomingAcdCallEventOnPause)

    }
    "on line event hangup" in new RingingOnPauseFsm() {
      agentStatusMachine ! lineEventHungup

      agentStatusMachine.stateName shouldBe MAgentPaused

      checkSavedAgentState(agentStatusMachine.stateData, pauseEvent)

    }
    "on event ready go to ringing" in new RingingOnPauseFsm() {
      agentStatusMachine ! readyEvent

      agentStatusMachine.stateName shouldBe MAgentRinging
      checkSavedAgentState(agentStatusMachine.stateData, ringingEvent)
    }
    val testMachine = new SimpleFsmTests(MAgentRingingOnPause, AgentStateContext(AgentRinging(12, new DateTime, "3000", List()), "outToLunch", MonitorState.ACTIVE, phoneNumber = "3000"))

    testMachine.onEventLoggedOut


  }
  //-----------------------------------------------------//
  //----------------Call state --------------------------//
  //-----------------------------------------------------//
  "Agent State Machine on call state" should {

    class OnCallFsm(presence: String="", monitorState: MonitorState = MonitorState.DISABLED, callId:String = "") extends DefaultFsm with Events {
      val agentId = 1550
      val phoneNb = "7788"
      val queues = List()
      val cause = Some(presence)

      val agentStatusMachine =  fsm(Some(MAgentOnCall), Some(AgentStateContext(incomingCallEvent, presence, monitorState,callId, phoneNumber = phoneNb)))
    }

    "on line event call hungup up back to pause save state and clean callid from context" in new OnCallFsm() {

      override val agentStatusMachine =  fsm(Some(MAgentOnCall), Some(AgentStateContext(incomingCallEvent, "", MonitorState.DISABLED,lineEventHungup.call.callId, phoneNumber = phoneNb)))

      agentStatusMachine ! lineEventHungup

      agentStatusMachine.stateName shouldBe MAgentReady
      checkSavedAgentState(agentStatusMachine.stateData, readyEvent)
      checkSavedCallId(agentStatusMachine.stateData,"")

    }
    "On line event hungup and terminating with an other callid stay" in new OnCallFsm(callId="546546546.3") {

      val lineEventOtherLineHungUp = LineEvent(phoneNb.toInt, CallData("777888.3", LineState.HUNGUP, phoneNb))

      agentStatusMachine ! lineEventOtherLineHungUp

      agentStatusMachine.stateName shouldBe MAgentOnCall
    }

    "On line event hungup for other end stay" in new OnCallFsm() {

      override val agentStatusMachine =  fsm(Some(MAgentOnCall), Some(AgentStateContext(incomingCallEvent, "", MonitorState.DISABLED,lineEventHungup.call.callId, phoneNumber = phoneNb)))

      agentStatusMachine ! lineEventHungupForOtherEnd

      agentStatusMachine.stateName shouldBe MAgentOnCall

    }
    "On event pause goto on call on pause" in new OnCallFsm() {
      agentStatusMachine ! pauseEvent
      agentStatusMachine.stateName shouldBe MAgentOnCallOnPause
      checkSavedAgentState(agentStatusMachine.stateData, incomingCallEvent)
    }
    "on event line up (masquerade) update callid and stay" in new OnCallFsm() {
      agentStatusMachine ! lineEventEstablishedMasquerade

      agentStatusMachine.stateName shouldBe MAgentOnCall
      checkSavedCallId(agentStatusMachine.stateData,lineEventEstablishedMasquerade.call.callId)
    }

    val testMachine = new SimpleFsmTests(MAgentOnCall, AgentStateContext(AgentReady(12, new DateTime, "3000", List()), "outToLunch", MonitorState.ACTIVE, phoneNumber = "3000"))

    testMachine.onEventWrapup
    testMachine.onEventLoggedOut

  }
  //-----------------------------------------------------//
  //----------------On Call On Pause state --------------//
  //-----------------------------------------------------//
  "Agent State Machine on call on pause state" should {
    class OnCallOnPauseFsm(presence: String="", monitorState: MonitorState = MonitorState.DISABLED, callId:String = "") extends DefaultFsm with Events {
      val agentId = 1550
      val phoneNb = "7788"
      val queues = List()
      val cause = Some(presence)

      val agentStatusMachine =  fsm(Some(MAgentOnCallOnPause), Some(AgentStateContext(onOutGoingCallEvent, presence, monitorState,callId, phoneNumber = phoneNb)))
    }

    "On line event hungup and terminating with an other callid stay" in new OnCallOnPauseFsm(callId="546546546.3") {

      val lineEventOtherLineHungUp = LineEvent(phoneNb.toInt, CallData("777888.3", LineState.HUNGUP, phoneNb))

      agentStatusMachine ! lineEventOtherLineHungUp

      agentStatusMachine.stateName shouldBe MAgentOnCallOnPause
    }
    "on line event call hungup up back to pause save state and clean callid from context" in new OnCallOnPauseFsm {

      override val agentStatusMachine =  fsm(Some(MAgentOnCallOnPause), Some(AgentStateContext(onOutGoingCallEvent, "", MonitorState.DISABLED,lineEventHungup.call.callId, phoneNumber = phoneNb)))

      agentStatusMachine ! lineEventHungup

      agentStatusMachine.stateName shouldBe MAgentPaused
      checkSavedAgentState(agentStatusMachine.stateData, pauseEvent)
      checkSavedCallId(agentStatusMachine.stateData,"")

    }
    "on line event outgoing call hungup with an other callid stay" in new OnCallOnPauseFsm(callId="11111111111.3") {
      val lineEventOtherLineHungUp = LineEvent(phoneNb.toInt, CallData("2525252525.3", LineState.HUNGUP, phoneNb))

      agentStatusMachine ! lineEventOtherLineHungUp

      agentStatusMachine.stateName shouldBe MAgentOnCallOnPause

    }
    "On line event hungup for other end stay" in new OnCallOnPauseFsm() {

      override val agentStatusMachine =  fsm(Some(MAgentOnCallOnPause), Some(AgentStateContext(incomingCallEvent, "", MonitorState.DISABLED,lineEventHungup.call.callId, phoneNumber = phoneNb)))

      agentStatusMachine ! lineEventHungupForOtherEnd

      agentStatusMachine.stateName shouldBe MAgentOnCallOnPause

    }
    "On event ready goto on call" in new OnCallOnPauseFsm() {
      agentStatusMachine ! readyEvent
      agentStatusMachine.stateName shouldBe MAgentOnCall
      checkSavedAgentState(agentStatusMachine.stateData, onOutGoingCallEvent)

    }
    "on event line up (masquerade) update callid and stay" in new OnCallOnPauseFsm() {
      agentStatusMachine ! lineEventEstablishedMasquerade

      agentStatusMachine.stateName shouldBe MAgentOnCallOnPause
      checkSavedCallId(agentStatusMachine.stateData,lineEventEstablishedMasquerade.call.callId)
    }

    val testMachine = new SimpleFsmTests(MAgentOnCallOnPause, AgentStateContext(AgentOnCall(12, new DateTime, acd=false, Incoming, CallTypeUnknown, "3000", List(), onPause=true), "outToLunch", MonitorState.ACTIVE, phoneNumber = "3000"))

    testMachine.onEventLoggedOut

  }
  //-----------------------------------------------------//
  //----------------Pause state -------------------------//
  //-----------------------------------------------------//
  "Agent State Machine in pause state" should {

    class PauseFsm(presence: String="", monitorState: MonitorState = MonitorState.DISABLED) extends DefaultFsm with Events {
      val agentId = 649
      val phoneNb = "8899"
      val queues = List()
      val cause = Some(presence)

      val agentStatusMachine = fsm(Some(MAgentPaused), Some(AgentStateContext(pauseEvent, presence, monitorState, phoneNumber=phoneNb)))

    }
    val testMachine = new SimpleFsmTests(MAgentPaused, AgentStateContext(AgentOnPause(12, new DateTime, "3000", List()), "outToLunch", MonitorState.ACTIVE, phoneNumber = "3000"))

    testMachine.onEventReady
    testMachine.onEventLoggedOut
    testMachine.onEventWrapup


    "On line event ringing publish state"  in new PauseFsm("eating") {
      val lineEvent = LineEvent(1001, CallData("789456.2", LineState.RINGING, "8899"))

      agentStatusMachine ! lineEvent

      agentStatusMachine.stateName shouldBe MAgentRingingOnPause
      checkSavedAgentState(agentStatusMachine.stateData, ringingEvent)
    }

    "on line event riging other end stay"  in new PauseFsm("eating") {

      agentStatusMachine ! lineEventRingingOtherEnd

      agentStatusMachine.stateName shouldBe MAgentPaused
    }
    "on line event dialing change state do dialing on pause and publish event agentdialing" in new PauseFsm() {
      agentStatusMachine ! lineEventDialing

      agentStatusMachine.stateName shouldBe MAgentDialingOnPause
      checkSavedAgentState(agentStatusMachine.stateData, dialingEvent)
    }

    """on event on call publish on call event and goto on call state""" in new PauseFsm {
      agentStatusMachine ! lineEventEstablished

      agentStatusMachine.stateName shouldBe MAgentOnCallOnPause
      checkSavedAgentState(agentStatusMachine.stateData, incomingCallEvent)
    }

  }

  //-----------------------------------------------------//
  //----------------Dialing on Pause state --------------//
  //-----------------------------------------------------//
  "Agent State Machine in dialing on pause state" should {

    val testMachine = new SimpleFsmTests(MAgentDialingOnPause, AgentStateContext(AgentDialing(12, new DateTime, "3000", List()), "outToLunch", MonitorState.ACTIVE, phoneNumber = "3000"))

    testMachine.onEventLoggedOut
    testMachine.onEventPaused

    class DialingOnPauseFsm(presence: String="", monitorState: MonitorState = MonitorState.DISABLED) extends DefaultFsm with Events {
      val agentId = 789
      val phoneNb = "7745"
      val queues = List(1,2,3)
      val cause = Some(presence)
      val agentStatusMachine = fsm(Some(MAgentDialingOnPause), Some(AgentStateContext(dialingEvent, presence, monitorState, phoneNumber=phoneNb)))
    }

    "on event ready change to dialing state" in new DialingOnPauseFsm {
      agentStatusMachine ! readyEvent

    }
    "on event line originated established change state to on call on pause state and save state on call" in new DialingOnPauseFsm {
      agentStatusMachine ! lineEventEstablished

      agentStatusMachine.stateName should be(MAgentOnCallOnPause)
      checkSavedAgentState(agentStatusMachine.stateData, onOutGoingCallEvent)

    }
    "on event established other end stay" in new DialingOnPauseFsm {
      agentStatusMachine ! lineEventEstablishedOtherEnd
      agentStatusMachine.stateName should be(MAgentDialingOnPause)
    }
    """On event ready DO NOT save state and goto dialing""" in new DialingOnPauseFsm {
      agentStatusMachine ! readyEvent

      agentStatusMachine.stateName shouldBe MAgentDialing
      checkSavedAgentState(agentStatusMachine.stateData,  dialingEvent)
    }

  }


  //-----------------------------------------------------//
  //----------------Wrapup state ------------------------//
  //-----------------------------------------------------//
  "Agent State Machine on wrapup state" should {
    class WrapupFsm(presence: String="", monitorState: MonitorState = MonitorState.DISABLED) extends DefaultFsm with Events {
      val agentId = 890
      val phoneNb = "9933"
      val queues = List()
      val cause = Some(presence)

      val agentStatusMachine = fsm(Some(MAgentOnWrapup), Some(AgentStateContext(AgentOnWrapup(agentId, new DateTime(), phoneNb, List()), presence, monitorState, phoneNumber = phoneNb)))
    }

    val testMachine = new SimpleFsmTests(MAgentOnWrapup, AgentStateContext(AgentOnWrapup(24, new DateTime, "2000", List()), "wrapup", MonitorState.DISABLED, phoneNumber = "3000"))

    testMachine.onEventReady
    testMachine.onEventLoggedOut
    testMachine.onEventPaused

    " on line event dialing change state to agent dialing save state" in new WrapupFsm {
      agentStatusMachine ! lineEventDialing

      agentStatusMachine.stateName shouldBe MAgentDialing
      checkSavedAgentState(agentStatusMachine.stateData, dialingEvent)
    }
    "on line event oubound dial change state to dialing" in new WrapupFsm {
      agentStatusMachine ! lineEventRingingOutBoundDial

      agentStatusMachine.stateName shouldBe MAgentDialing
      checkSavedAgentState(agentStatusMachine.stateData, dialingEvent)
    }

    "On line event ringing change state to ringing and save state" in new WrapupFsm {
      agentStatusMachine ! lineEventRinging

      agentStatusMachine.stateName shouldBe MAgentRinging
      checkSavedAgentState(agentStatusMachine.stateData, ringingEvent)
    }
    "on line event ringing other end stay" in new WrapupFsm {
      agentStatusMachine ! lineEventRingingOtherEnd

      agentStatusMachine.stateName shouldBe MAgentOnWrapup
    }


  }

  //-----------------------------------------------------//
  //----------------Log out state -----------------------//
  //-----------------------------------------------------//
  "Agent State Machine in logout state" should {
    val testMachine = new SimpleFsmTests(MAgentLoggedOut, AgentStateContext(AgentReady(12, new DateTime, "3000", List()), "outToLunch", MonitorState.ACTIVE, phoneNumber = "3000"))

    testMachine.onEventLogin

  }

  //-----------------------------------------------------//
  //------------------ Unhandled event ------------------//
  //-----------------------------------------------------//
  "Agent State Machine receive an unhandled event" should {

    class UnhandledFsm(presence: String="") extends DefaultFsm with Events {
      val agentId = 3337
      val phoneNb = "30045"
      val queues:List[Int] = List()
      val cause = Some(presence)
      val oncallEvent = AgentOnCall(agentId, new DateTime(), true, Incoming, Internal, phoneNb, List(), onPause=true)
    }

      """ publish state if list of connected queue changes and preserve context """.stripMargin in new UnhandledFsm()  {
      override val queues = List(1,2,3)
      val agentStatusMachine = fsm(Some(MAgentReady), Some(AgentStateContext(readyEvent, "officework", MonitorState.ACTIVE,"6546644.23", phoneNumber="45000")))

      val readyEventWithNewQueues = AgentReady(agentId, time, phoneNb, queues:::List(8,9,10))

      agentStatusMachine ! readyEventWithNewQueues

      verify(eventBus).publish(readyEventWithNewQueues withPhoneNb "45000")
      agentStatusMachine.stateData should be(AgentStateContext(readyEventWithNewQueues,"officework", MonitorState.ACTIVE,"6546644.23", phoneNumber="45000"))
    }
    """ upon agent user status received
        publish agent status event on bus
        preserve monitor state, callid and phone number from context""" in new UnhandledFsm {

      val agentStatusMachine = fsm(Some(MAgentReady), Some(AgentStateContext(readyEvent, "coffeebreak",MonitorState.DISABLED,"7897797.66", phoneNumber="5012")))
      val savedReadyEvent = AgentReady(agentId, time, "", List(),cause=Some("eatingpancakes"))
      val agentUserStatus = new AgentUserStatus(agentId, "eatingpancakes")

      agentStatusMachine ! agentUserStatus

      verify(eventBus, times(1)).publish(savedReadyEvent withCause "eatingpancakes" withPhoneNb("5012"))
      agentStatusMachine.stateData should be(AgentStateContext(savedReadyEvent withPhoneNb("5012"),"eatingpancakes", MonitorState.DISABLED,"7897797.66","5012"))
    }

    """on last state request when agent is logged in
        - send it to requester
        - send event agent login to requester""" in new UnhandledFsm {
      val asm = fsm(Some(MAgentReady), Some(AgentStateContext(readyEvent, phoneNumber="")))
      val requester = TestProbe()
      
      asm ! GetLastReceivedState(requester.ref)

      requester.expectMsgAllOf(loginEvent, readyEvent)
    }
    """on last state request when agent is NOT logged in
        - send it to requester
        - do not send event agent login to requester""" in new UnhandledFsm {
      val asm = fsm(Some(MAgentReady), Some(AgentStateContext(loggedOutEvent, phoneNumber="")))
      val requester = TestProbe()
      
      asm ! GetLastReceivedState(requester.ref)

      requester.expectMsg(loggedOutEvent)
      requester.expectNoMsg(100 millis)
    }

    """on AgentCallUpdate while in AgentOnCall state
        - publish state with monitor details and keep context""" in new UnhandledFsm {
      val asm = fsm(Some(MAgentOnCall), Some(AgentStateContext(onOutGoingCallEvent, "outToLunch", MonitorState.UNKNOWN,"7899545.2", phoneNumber="779955")))

      asm ! AgentCallUpdate(221, MonitorState.ACTIVE)

      verify(eventBus, times(1)).publish(onOutGoingCallEvent withCause "outToLunch" withMonitorState MonitorState.ACTIVE withPhoneNb "779955")

      asm.stateData should be(AgentStateContext(onOutGoingCallEvent withCause "outToLunch" withMonitorState MonitorState.ACTIVE,"outToLunch", MonitorState.ACTIVE,"7899545.2", phoneNumber="779955"))

    }

    """on AgentCallUpdate when in other state
        - save monitor state in the fsm context and preserve context""" in new UnhandledFsm {
      val asm = fsm(Some(MAgentReady), Some(AgentStateContext(readyEvent, "outToBath", MonitorState.PAUSED, "46546654.2", "1100")))

      asm ! AgentCallUpdate(331, MonitorState.ACTIVE)
      asm.stateData should be(AgentStateContext(readyEvent,"outToBath", MonitorState.ACTIVE,"46546654.2", "1100"))
    }
    "Do not publish agent call update if monitor state is the same" in new UnhandledFsm() {
      val asm = fsm(Some(MAgentOnCall), Some(AgentStateContext(onOutGoingCallEvent, "outToLunch", MonitorState.ACTIVE, "7899545.2", phoneNumber = "779955")))
      asm ! AgentCallUpdate(331, MonitorState.ACTIVE)

      verify(eventBus, never).publish(onOutGoingCallEvent withCause "outToLunch" withMonitorState MonitorState.ACTIVE withPhoneNb "779955")
    }
  }
}
