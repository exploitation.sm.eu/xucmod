package services.agent

import akka.actor.Props
import akka.testkit.{TestActorRef, TestProbe}
import akkatest.TestKitSpec
import services.ActorFactory
import services.agent.AgentInGroupAction.{AgentsDestination, RemoveAgents}
import services.config.ConfigDispatcher._
import xivo.models.Agent


class AgentFromGroupNotInQueueAdderSpec extends TestKitSpec("AgentFromGroupNotInQueueAdderSpec") {


   class Helper {
     val configDispatcher = TestProbe()

     trait TestActorFactory extends  ActorFactory {
       override val configDispatcherURI = configDispatcher.ref.path.toString
     }

     def actor(groupId: Long, queueId: Long, penalty : Int) = {
       val a = TestActorRef(Props(new AgentFromGroupNotInQueueAdder(groupId, queueId, penalty) with TestActorFactory))
       (a,a.underlyingActor)
     }
   }



   "an agent from group not in queue" should {
     "request agents from config dispatcher" in new Helper {
       val groupId = 78
       val (toQueueId, toPenalty) = (85,9)
       val (ref,_) = actor(groupId,toQueueId,toPenalty)

       ref ! AgentsDestination(toQueueId,toPenalty)


       configDispatcher.expectMsg(RequestConfig(ref,GetAgentsNotInQueue(groupId, toQueueId)))
     }


     "should add agents to queue with penalty on agent list received" in new Helper {

       val (groupId, queueId, penalty) = (87,78,3)
       val (ref,_) = actor(groupId,queueId,penalty)

       val agents = List(Agent(1,"Louis","Ostech","7889","charles",groupId))

       ref ! AgentsDestination(queueId,penalty)

       ref !  AgentList(agents)

       configDispatcher.expectMsgAllOf(RequestConfig(ref,GetAgentsNotInQueue(groupId, queueId)),
                                        ConfigChangeRequest(ref,SetAgentQueue(1,queueId,penalty))
                                       )

     }
   }
 }
