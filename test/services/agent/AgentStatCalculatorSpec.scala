package services.agent

import org.joda.time.DateTime
import org.mockito.Matchers
import org.scalatest.mock.MockitoSugar
import services.AgentStateFSM._
import xivo.events.AgentState._
import xivo.xucami.models.MonitorState
import xuctest.BaseTest
import org.mockito.Mockito.{never, times, verify, verifyZeroInteractions, atLeastOnce}

class AgentStatCalculatorSpec extends  BaseTest with MockitoSugar {

  class Helper {
    trait TestTimeProvider extends TimeProvider {
      var dateTime = new DateTime
      def getTime = dateTime
    }

    case class AgentEventTotalTime(override val name: String, collector: StatCollector) extends AgentStatCalculator  with AgentTotalTime with TestTimeProvider{
    }

  }

  class AgentDateTimeHelper {
    val statCollector = mock[StatCollector]

  }
  "agent login time" should {
    "be calculated from login event date time" in new AgentDateTimeHelper {
      val loginDateTime = LoginDateTime("loginDateTime", statCollector)
      val loginDate = new DateTime(2005, 3, 26, 11, 0, 0, 0)

      val eventLogin = AgentLogin(19,loginDate,"1000",List() )

      loginDateTime.processEvent(eventLogin)

      verify(statCollector).onStatCalculated("loginDateTime", StatDateTime(loginDate))
    }
    "be calculated from logout event date time" in new AgentDateTimeHelper {
      val logoutDateTime = LogoutDateTime("logoutDateTime", statCollector)
      val logoutDate = new DateTime(2006, 1, 16, 22, 0, 0, 0)

      val eventLogout = AgentLoggedOut(21,logoutDate,"1010",List() )

      logoutDateTime.processEvent(eventLogout)

      verify(statCollector).onStatCalculated("logoutDateTime", StatDateTime(logoutDate))
    }
  }
  "agentTotalTime" should {
    "only intialize once start time" in new Helper {

      val total = AgentEventTotalTime("test", mock[StatCollector])

      val startTime = new DateTime(2005, 3, 26, 11, 0, 0, 0)
      total.start(startTime)
      total.start(startTime.plusSeconds(20))
      total.dateTime = startTime.plusSeconds(600)
      total.accumulate().toInt should be (600)
    }

    "should not accumulate any more after stop and publish" in new Helper {

      val total = AgentEventTotalTime("test", mock[StatCollector])

      val startTime = new DateTime(2005, 3, 26, 11, 0, 0, 0)
      total.start(startTime)
      total.dateTime = startTime.plusSeconds(60)
      total.stopAndPublish()
      total.dateTime = startTime.plusSeconds(120)
      total.accumulate().toInt should be (60)

    }

    "reset period to 0 and restart from reset date if started" in new Helper {
      val statCollector = mock[StatCollector]
      val total = AgentEventTotalTime("test", statCollector)
      val startTime = new DateTime(2015, 4, 2, 11, 0, 0, 0)
      total.start(startTime)
      total.dateTime = new DateTime(2015, 4, 3, 0, 0, 0, 0)
      total.reset()
      total.accumulate().toInt should be(0)
      verify(statCollector).onStatCalculated("test", StatPeriod(0))

    }
    "should reset period to 0 and do not reset date if not started" in new Helper {
      val statCollector = mock[StatCollector]

      val total = AgentEventTotalTime("test",statCollector)

      val startTime = new DateTime(2005, 3, 26, 11, 0, 0, 0)
      total.start(startTime)
      total.dateTime =startTime.plusSeconds(10)
      total.reset()
      total.dateTime =total.dateTime.plusSeconds(30)
      total.stopAndPublish()

      verify(statCollector).onStatCalculated("test", StatPeriod(30))
      verify(statCollector).onStatCalculated("test", StatPeriod(0))

    }

    "should publish 0 on reset" in new Helper {
      val statCollector = mock[StatCollector]
      val total = AgentEventTotalTime("test",statCollector)

      val startTime = new DateTime(2005, 3, 26, 11, 0, 0, 0)
      total.start(startTime)
      total.dateTime =startTime.plusSeconds(200)
      total.reset()

      verify(statCollector).onStatCalculated("test", StatPeriod(0))

    }
    "on reset publish value for stopped statistics" in new Helper {
      val statCollector = mock[StatCollector]
      val total = AgentEventTotalTime("test",statCollector)

      total.reset()

      verify(statCollector).onStatCalculated("test", StatPeriod(0))

    }
    "do not publish stat if not started" in new Helper {
      val statCollector = mock[StatCollector]
      val total = AgentEventTotalTime("test",statCollector)

      total.stopAndPublish()

      verify(statCollector, never).onStatCalculated("test", StatPeriod(0))

    }
    "publish 0 if end time is before start time machine time desynchronized" in new Helper {
      val statCollector = mock[StatCollector]
      val total = AgentEventTotalTime("test",statCollector)
      val startTime = new DateTime(2005, 3, 26, 11, 0, 0, 0)
      total.start(startTime)
      total.dateTime =startTime.plusSeconds(-2)

      total.stopAndPublish()

      verify(statCollector).onStatCalculated("test", StatPeriod(0))

    }
  }

  class TotalCallHelper {
    val statCollector = mock[StatCollector]

    case class AgentAnyTotalCalls(override val name: String, collector: StatCollector) extends AgentStatCalculator  with AgentTotalCalls {
    }

    val totalCalls = AgentAnyTotalCalls("testTotalCalls", statCollector)
  }
  "Agent total call" should {
    "be set to 0 on creation" in new TotalCallHelper {

      totalCalls.total should be(0)
    }

    "reset total to 0 " in new TotalCallHelper {
      totalCalls.reset()
      totalCalls.total should be(0)
    }
    "publish 0 on reset" in new TotalCallHelper {
      totalCalls.total = 58
      totalCalls.reset()
      totalCalls.total should be(0)

      verify(statCollector).onStatCalculated("testTotalCalls", StatTotal(0))
    }
  }
  "Agent inbound total call" should {
    "increment and publish on agent ringing" in {

      val statCollector = mock[StatCollector]
      val totalInbound = AgentInboundTotalCalls("totalInbound", statCollector)

      val answerTransition: AgentTransition = AgentTransition(MAgentReady, MEmptyContext, MAgentOnCall,
        AgentStateContext(AgentOnCall(1, new DateTime, false, CallDirection.Incoming, CallType.CallTypeUnknown, "123", List(), onPause=false), phoneNumber=""))

      totalInbound.processTransition(answerTransition)

      verify(statCollector).onStatCalculated("totalInbound", StatTotal(1))

      val ringingTransition: AgentTransition = AgentTransition(MAgentReady, MEmptyContext, MAgentRinging,
        AgentStateContext(AgentOnCall(1, new DateTime, false, CallDirection.Incoming, CallType.CallTypeUnknown, "123", List(), onPause=false), phoneNumber=""))

      totalInbound.processTransition(ringingTransition)

      verify(statCollector).onStatCalculated("totalInbound", StatTotal(2))

      val ringingOnPauseTransition: AgentTransition = AgentTransition(MAgentPaused, MEmptyContext, MAgentRingingOnPause,
        AgentStateContext(AgentOnCall(1, new DateTime, false, CallDirection.Incoming, CallType.CallTypeUnknown, "123", List(), onPause=false), phoneNumber=""))

      totalInbound.processTransition(ringingOnPauseTransition)

      verify(statCollector).onStatCalculated("totalInbound", StatTotal(3))

      val answerOnPauseTransition: AgentTransition = AgentTransition(MAgentPaused, MEmptyContext, MAgentOnCallOnPause,
        AgentStateContext(AgentOnCall(1, new DateTime, false, CallDirection.Incoming, CallType.CallTypeUnknown, "123", List(), onPause=false), phoneNumber=""))

      totalInbound.processTransition(answerOnPauseTransition)

      verify(statCollector).onStatCalculated("totalInbound", StatTotal(4))

      val ringingOnWrapupTransition: AgentTransition = AgentTransition(MAgentOnWrapup, MEmptyContext, MAgentOnCallOnPause,
        AgentStateContext(AgentOnCall(1, new DateTime, false, CallDirection.Incoming, CallType.CallTypeUnknown, "123", List(), onPause=false), phoneNumber=""))

      totalInbound.processTransition(ringingOnWrapupTransition)

      verify(statCollector).onStatCalculated("totalInbound", StatTotal(5))

    }
  }
  "agent inboudd answered calls" should {
    "increment and publish on transition to agentOnCall" in {
      val statCollector = mock[StatCollector]
      val answeredInbound = AgentInboundAnsweredCalls("answeredInbound", statCollector)

      val answerTransition: AgentTransition = AgentTransition(MAgentRinging, MEmptyContext, MAgentOnCall,
        AgentStateContext(AgentOnCall(1, new DateTime, false, CallDirection.Incoming, CallType.CallTypeUnknown, "123", List(), onPause=false), phoneNumber=""))

      answeredInbound.processTransition(answerTransition)

      verify(statCollector).onStatCalculated("answeredInbound", StatTotal(1))

      val answerOnPauseTransition: AgentTransition = AgentTransition(MAgentRingingOnPause, MEmptyContext, MAgentOnCallOnPause,
        AgentStateContext(AgentOnCall(1, new DateTime, false, CallDirection.Incoming, CallType.CallTypeUnknown, "123", List(), onPause=false), phoneNumber=""))

      answeredInbound.processTransition(answerOnPauseTransition)

      verify(statCollector).onStatCalculated("answeredInbound", StatTotal(2))
    }
  }

  "Agent Inbound Total Call Time" should {
    "accumulate conversation time" in {
      val statCollector = mock[StatCollector]
      val timeInbound = AgentInboundTotalCallTime("timeInbound", statCollector)
      val answer: AgentTransition = AgentTransition(MAgentRinging, MEmptyContext, MAgentOnCall,
        AgentStateContext(AgentOnCall(1, new DateTime, false, CallDirection.Incoming, CallType.CallTypeUnknown, "123", List(), onPause=false), phoneNumber="")
      )
      val hangup: AgentTransition = AgentTransition(MAgentOnCall, MEmptyContext, MAgentReady, MEmptyContext)
      timeInbound.processTransition(answer)
      timeInbound.processTransition(hangup)
      verify(statCollector).onStatCalculated(Matchers.eq("timeInbound"), StatPeriod(Matchers.anyLong()))
    }
  }

  "agent outbound total calls" should {
    "increment and publish on agent dialing" in {
      val statCollector = mock[StatCollector]
      val totalOutbound = AgentOutboundTotalCalls("totalOutbound", statCollector)

      val agDialing = AgentDialing(51, new DateTime,"1400",List())

      totalOutbound.processEvent(agDialing)

      verify(statCollector).onStatCalculated("totalOutbound", StatTotal(1))
    }
  }

  "Agent Outbound Total Call Time" should {
    "accumulate conversation time from ready" in {
      val statCollector = mock[StatCollector]
      val timeInbound = AgentOutboundTotalCallTime("timeOutBound", statCollector)
      val answer: AgentTransition = AgentTransition(MAgentDialing, MEmptyContext, MAgentOnCall,
        AgentStateContext(AgentOnCall(1, new DateTime, false, CallDirection.Outgoing, CallType.CallTypeUnknown, "123", List(), onPause=false), phoneNumber="")
      )
      val hangup: AgentTransition = AgentTransition(MAgentOnCall, MEmptyContext, MAgentReady, MEmptyContext)
      timeInbound.processTransition(answer)
      timeInbound.processTransition(hangup)
      verify(statCollector).onStatCalculated(Matchers.eq("timeOutBound"), StatPeriod(Matchers.anyLong()))
    }
    "accumulate conversation time from pause" in {
      val statCollector = mock[StatCollector]
      val timeInbound = AgentOutboundTotalCallTime("timeOutBound", statCollector)
      val answer: AgentTransition = AgentTransition(MAgentDialingOnPause, MEmptyContext, MAgentOnCallOnPause,
        AgentStateContext(AgentOnCall(1, new DateTime, false, CallDirection.Outgoing, CallType.CallTypeUnknown, "123", List(), onPause=false), phoneNumber="")
      )
      val hangup: AgentTransition = AgentTransition(MAgentOnCallOnPause, MEmptyContext, MAgentPaused, MEmptyContext)
      timeInbound.processTransition(answer)
      timeInbound.processTransition(hangup)
      verify(statCollector).onStatCalculated(Matchers.eq("timeOutBound"), StatPeriod(Matchers.anyLong()))
    }
  }

  "AgentTransitionQualifier" should {
    "provide a function detecting Answer of an incoming call" in {
      object TestAgentTransitionQualifier extends AgentTransitionQualifier
      val answer: AgentTransition = AgentTransition(MAgentRinging, MEmptyContext, MAgentOnCall,
        AgentStateContext(AgentOnCall(1, new DateTime, false, CallDirection.Incoming, CallType.CallTypeUnknown, "123", List(), onPause=false), phoneNumber=""))
      TestAgentTransitionQualifier.isNewAnsweredIncomingCall(answer) shouldBe true

      val stay: AgentTransition = AgentTransition(MAgentOnCall, MEmptyContext, MAgentOnCall,
        AgentStateContext(AgentOnCall(1, new DateTime, false, CallDirection.Incoming, CallType.CallTypeUnknown, "123", List(), onPause=false), phoneNumber=""))
      TestAgentTransitionQualifier.isNewAnsweredIncomingCall(stay) shouldBe false

      val outgoing: AgentTransition = AgentTransition(MAgentOnCall, MEmptyContext, MAgentOnCall,
        AgentStateContext(AgentOnCall(1, new DateTime, false, CallDirection.Outgoing, CallType.CallTypeUnknown, "123", List(), onPause=false), phoneNumber=""))
      TestAgentTransitionQualifier.isNewAnsweredIncomingCall(outgoing) shouldBe false

      val unknown: AgentTransition = AgentTransition(MAgentOnCall, MEmptyContext, MAgentOnCall,
        AgentStateContext(AgentOnCall(1, new DateTime, false, CallDirection.DirectionUnknown, CallType.CallTypeUnknown, "123", List(), onPause=false), phoneNumber=""))
      TestAgentTransitionQualifier.isNewAnsweredIncomingCall(unknown) shouldBe false

    }
    "provide a function detecting Answer of an outging call" in {
      object TestAgentTransitionQualifier extends AgentTransitionQualifier
      val answer: AgentTransition = AgentTransition(MAgentRinging, MEmptyContext, MAgentOnCall,
        AgentStateContext(AgentOnCall(1, new DateTime, false, CallDirection.Outgoing, CallType.CallTypeUnknown, "123", List(), onPause=false), phoneNumber=""))
      TestAgentTransitionQualifier.isNewAnsweredOutgoingCall(answer) shouldBe true

      val stay: AgentTransition = AgentTransition(MAgentOnCall, MEmptyContext, MAgentOnCall,
        AgentStateContext(AgentOnCall(1, new DateTime, false, CallDirection.Outgoing, CallType.CallTypeUnknown, "123", List(), onPause=false), phoneNumber=""))
      TestAgentTransitionQualifier.isNewAnsweredOutgoingCall(stay) shouldBe false

      val incoming: AgentTransition = AgentTransition(MAgentRinging,
        AgentStateContext(AgentRinging(1L,new DateTime,"1001",List(1),Some("available")),"available",MonitorState.DISABLED,"1001", "1001"),
        MAgentOnCall,
        AgentStateContext(AgentOnCall(1,new DateTime,true,CallDirection.Incoming,CallType.CallTypeUnknown,"1001",List(1),onPause=false, Some("available"),MonitorState.UNKNOWN),"available",MonitorState.DISABLED,"1433426562.14","1001"))
      TestAgentTransitionQualifier.isNewAnsweredOutgoingCall(incoming) shouldBe false
    }
    "provide a function detecting call hangup" in {
      object TestAgentTransitionQualifier extends AgentTransitionQualifier
      val answer: AgentTransition = AgentTransition(MAgentOnCall, MEmptyContext, MAgentReady, MEmptyContext)
      TestAgentTransitionQualifier.isHangup(answer) shouldBe true

      val ready: AgentTransition = AgentTransition(MAgentReady, MEmptyContext, MAgentReady, MEmptyContext)
      TestAgentTransitionQualifier.isHangup(ready) shouldBe false

      val stay: AgentTransition = AgentTransition(MAgentOnCall, MEmptyContext, MAgentOnCall, MEmptyContext)
      TestAgentTransitionQualifier.isHangup(stay) shouldBe false

    }

  }

  "Agent Inbound Average Call Time" should {
    class AgtInbAverHelper {
      val statCollector = mock[StatCollector]
      val inboundAverageCallTime = AgentInboundAverageCallTime(collector = statCollector)

      def inbTotalCallTime(nb:Long) =  AgentStatistic(44, List(Statistic(AgentInboundTotalCallTime.name, StatPeriod(nb))))
      def answeredCalls(nb:Long) = AgentStatistic(44, List(Statistic(AgentInboundAnsweredCalls.name, StatTotal(nb))))
    }
    "increment and publish rounded value on new agent inbound total call time" in new AgtInbAverHelper {

      inboundAverageCallTime.processStat(answeredCalls(3L))
      inboundAverageCallTime.processStat(inbTotalCallTime(10L))

      verify(statCollector).onStatCalculated(AgentInboundAverageCallTime.name, StatTotal(3))
    }
    "on 0 answered calls, should publish 0 as average" in new AgtInbAverHelper {

      inboundAverageCallTime.processStat(answeredCalls(0L))
      inboundAverageCallTime.processStat(inbTotalCallTime(10L))

      verify(statCollector).onStatCalculated(AgentInboundAverageCallTime.name, StatTotal(0))

    }
  }

  "Agent Total unanswered calls" should {
    class AgtUnansCallsHelper {
      val statCollector = mock[StatCollector]
      val inboundUnansweredCalls = AgentInboundUnansweredCalls(collector = statCollector)

      def totalCalls(nb:Long) =  AgentStatistic(44, List(Statistic(AgentInboundTotalCalls.name, StatTotal(nb))))
      def answeredCalls(nb:Long) = AgentStatistic(44, List(Statistic(AgentInboundAnsweredCalls.name, StatTotal(nb))))
    }

    "on first total calls publish total calls" in new AgtUnansCallsHelper {

      inboundUnansweredCalls.processStat(totalCalls(12L))
      verify(statCollector).onStatCalculated(AgentInboundUnansweredCalls.name, StatTotal(12L))

    }
    "publish total calls minus answered calls on last answered calls received" in new AgtUnansCallsHelper {

      inboundUnansweredCalls.processStat(totalCalls(12L))
      inboundUnansweredCalls.processStat(answeredCalls(10L))
      verify(statCollector, atLeastOnce()).onStatCalculated(AgentInboundUnansweredCalls.name, StatTotal(2L))

    }
    "do not publish if more answered calls than received calls could be after reset" in new AgtUnansCallsHelper {

      inboundUnansweredCalls.processStat(answeredCalls(22L))

      verifyZeroInteractions(statCollector)

    }
    "publish on last total calls total minus last answerd calls" in  new AgtUnansCallsHelper {

      inboundUnansweredCalls.processStat(answeredCalls(5L))
      inboundUnansweredCalls.processStat(totalCalls(12L))
      verify(statCollector, atLeastOnce()).onStatCalculated(AgentInboundUnansweredCalls.name, StatTotal(7L))

    }
    "Reset all values to 0 on reset and publish" in  new AgtUnansCallsHelper {

      inboundUnansweredCalls.processStat(totalCalls(12L))
      inboundUnansweredCalls.processStat(answeredCalls(10L))
      inboundUnansweredCalls.reset()
      verify(statCollector, atLeastOnce()).onStatCalculated(AgentInboundUnansweredCalls.name, StatTotal(0L))

    }

  }
  "Agent unanswered calls percentage" should {
    class AgtUnanswHelper {
      val statCollector = mock[StatCollector]
      val inboundUnansweredCalls = AgentInboundPercentUnansweredCalls(collector = statCollector)

      def totalCalls(nb:Long) =  AgentStatistic(44, List(Statistic(AgentInboundTotalCalls.name, StatTotal(nb))))
      def unAnsweredCalls(nb:Long) = AgentStatistic(44, List(Statistic(AgentInboundUnansweredCalls.name, StatTotal(nb))))
    }
    "on first total calls publish 0 %" in new AgtUnanswHelper {

      inboundUnansweredCalls.processStat(totalCalls(12L))

      verify(statCollector).onStatCalculated(AgentInboundPercentUnansweredCalls.name, StatAverage(0.0))

    }
    "on first unanswered calls publish 100%" in new AgtUnanswHelper {

      inboundUnansweredCalls.processStat(unAnsweredCalls(40L))

      verify(statCollector).onStatCalculated(AgentInboundPercentUnansweredCalls.name, StatAverage(100.0))

    }

    "publish percentage of unanswered calls" in new AgtUnanswHelper {
      inboundUnansweredCalls.processStat(totalCalls(12L))
      inboundUnansweredCalls.processStat(unAnsweredCalls(6L))
      inboundUnansweredCalls.processStat(totalCalls(24L))

      verify(statCollector).onStatCalculated(AgentInboundPercentUnansweredCalls.name, StatAverage(0.0))
      verify(statCollector).onStatCalculated(AgentInboundPercentUnansweredCalls.name, StatAverage(50.0))
      verify(statCollector).onStatCalculated(AgentInboundPercentUnansweredCalls.name, StatAverage(25.0))
    }
    "publish max 100% if answered calls > total calls (period transition)" in new AgtUnanswHelper {
      inboundUnansweredCalls.processStat(totalCalls(5L))
      inboundUnansweredCalls.processStat(unAnsweredCalls(6L))
      verify(statCollector).onStatCalculated(AgentInboundPercentUnansweredCalls.name, StatAverage(0.0))
      verify(statCollector).onStatCalculated(AgentInboundPercentUnansweredCalls.name, StatAverage(100.0))

    }
    "publish O% on reset and restart calculation" in new AgtUnanswHelper {
      inboundUnansweredCalls.processStat(totalCalls(12L))
      inboundUnansweredCalls.processStat(unAnsweredCalls(6L))
      inboundUnansweredCalls.reset()
      inboundUnansweredCalls.processStat(totalCalls(10L))
      inboundUnansweredCalls.processStat(unAnsweredCalls(2L))

      verify(statCollector,times(3)).onStatCalculated(AgentInboundPercentUnansweredCalls.name, StatAverage(0.0))
      verify(statCollector).onStatCalculated(AgentInboundPercentUnansweredCalls.name, StatAverage(50.0))
      verify(statCollector,atLeastOnce()).onStatCalculated(AgentInboundPercentUnansweredCalls.name, StatAverage(20.0))
    }
  }
}
