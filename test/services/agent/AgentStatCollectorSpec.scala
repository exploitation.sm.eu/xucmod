package services.agent

import akka.actor.FSM
import akka.actor.FSM.{Transition, UnsubscribeTransitionCallBack, SubscribeTransitionCallBack}
import akka.actor.{ActorRef, PoisonPill, Props}
import akka.testkit.{TestProbe, TestActorRef}
import akkatest.TestKitSpec
import org.joda.time.DateTime
import org.mockito.Mockito._
import org.scalatest.mock.MockitoSugar
import play.api.test.FakeApplication
import play.api.test.Helpers._
import services.AgentStateFSM.{MEmptyContext, AgentStateContext, MAgentOnCall, MAgentRinging}
import services.{CronScheduler, XucEventBus}
import stats.Statistic.ResetStat
import us.theatr.akka.quartz.AddCronSchedule
import xivo.events.AgentState
import xivo.events.AgentState.{CallType, CallDirection, AgentOnCall, AgentOnPause}

class AgentStatCollectorSpec extends TestKitSpec("AgentStatCollector") with MockitoSugar {


  class Helper() {
    val eventBus = mock[XucEventBus]
    val agentId = 54
    val cronScheduler = TestProbe()
    val agentFSM = TestProbe()

    val mockRegister = mock[(AgentState,StatCollector)=>Unit]

    trait testQuartzScheduler extends CronScheduler {
      override val scheduler = cronScheduler.ref
    }
    var calculators:List[AgentStatCalculator] = List()

    trait TestStatRegistrar extends StatRegistrar {
      override val getStats =  calculators
      override def register(astate:AgentState,statCollector:StatCollector) = mockRegister(astate, statCollector)
    }

    def actor(calcs:List[AgentStatCalculator] = List()) = {
      calculators = calcs
      val fApp = FakeApplication(additionalConfiguration = Map("xucstats.resetSchedule" -> "0 32 0 * * ?"))
      running(fApp) {
        val a = TestActorRef[AgentStatCollector](Props(new AgentStatCollector(agentId, agentFSM.ref, eventBus) with testQuartzScheduler with TestStatRegistrar))
        (a, a.underlyingActor)
      }
    }
  }

  "an agent stat collector" should {
    """self register to quartz scheduler
       subscribe to event bus on agent state
       subscribe to event bus on agent transition
       subscribe to event bus on agent statistics""" in new Helper {
        val (ref, _) = actor()

        val cronSchedule = AddCronSchedule(ref, "0 32 0 * * ?", ResetStat)

        cronScheduler.expectMsg(cronSchedule)
        verify(eventBus).subscribe(ref,XucEventBus.agentEventTopic(agentId))
        verify(eventBus).subscribe(ref,XucEventBus.agentTransitionTopic(agentId))
        verify(eventBus).subscribe(ref,XucEventBus.statEventTopic(agentId))
    }

    "register stat calculator" in new Helper {
      val (ref, statCollector) = actor()
      val agentStatCalculator = mock[AgentStatCalculator]
      stub(agentStatCalculator.name).toReturn("testName")

    }

    "register and forward any events to agent stat calculator" in new Helper {
      val stateEvent = mock[AgentState]
      val agentStatCalculator = mock[AgentStatCalculatorByEvent]
      stub(agentStatCalculator.name).toReturn("testName")
      val (ref, statCollector) = actor(List(agentStatCalculator))

      ref ! stateEvent

      verify(agentStatCalculator).processEvent(stateEvent)
      verify(mockRegister)(stateEvent, statCollector)

    }

    "forward transitions to agent stat calculator" in new Helper {
      val toContext = AgentStateContext(AgentOnCall(1, new DateTime, false, CallDirection.Incoming,
        CallType.CallTypeUnknown, "123", List(), onPause=false), phoneNumber="")
      val transition = AgentTransition(MAgentRinging, MEmptyContext, MAgentOnCall, toContext)
      val agentStatCalculator = mock[AgentStatCalculatorByTransition]
      stub(agentStatCalculator.name).toReturn("testName")
      val (ref, statCollector) = actor(List(agentStatCalculator))

      ref ! transition

      verify(agentStatCalculator).processTransition(transition)
    }

    "on statcalculator changed publish event on bus" in new Helper {
      val (ref, statCollector) = actor()
      val agentStatCalculator = mock[AgentStatCalculator]

      val statValue = mock[StatValue]

      statCollector.onStatCalculated("statname", statValue)

      verify(eventBus).publish(AgentStatistic(agentId, List(Statistic("statname", statValue))))

    }

    "call reset stat on reset event" in new Helper {
      val agentStatCalculator = mock[AgentStatCalculator]
      val (ref, statCollector) = actor(List(agentStatCalculator))

      ref ! ResetStat

      verify(agentStatCalculator).reset()
    }
  }

  class RegistrarHelper {
    val statCollector = mock[AgentStatCollector]
    class Reg extends StatRegistrar {
    }
    val aRegistrar = new Reg()
  }

  "a registrar" should {
    "register a stat for event on pause with a cause" in new RegistrarHelper {

      aRegistrar.register(AgentOnPause(34,new DateTime,"1000", List(), Some("outtolunch")), statCollector)

      aRegistrar.getStats() should contain(AgentPausedTotalTimeWithCause("outtolunch", statCollector))
    }
    "no register twice with same cause" in new RegistrarHelper {
      val ap1 = AgentOnPause(34,new DateTime,"1000", List(), Some("outtolunch"))
      aRegistrar.register(ap1, statCollector)
      val calc1 = aRegistrar.getStats()(0)
      val ap2 = AgentOnPause(34,new DateTime().plusSeconds(100),"1000", List(), Some("outtolunch"))
      aRegistrar.register(ap2, statCollector)
      val calc2 = aRegistrar.getStats()(0)

      calc2 should be theSameInstanceAs(calc1)
    }
  }
}
