package services.config

import akka.actor.{ActorRef, Props}
import akka.testkit.{TestActorRef, TestProbe}
import akkatest.TestKitSpec
import models.XucUser
import org.xivo.cti.message.{AgentStatusUpdate, PhoneStatusUpdate}
import org.xivo.cti.model.{AgentStatus, Availability, PhoneHintStatus, StatusReason}
import services.config.ConfigManager.{InitConfig, PublishUserPhoneStatuses}
import services.request.{AgentLogout, BaseRequest}
import services.{ActorFactory, Start}
import xivo.events.{AgentError, AgentLoginError, AgentState}
import xivo.network.LoggedOn
import xivo.websocket.LinkState.up
import xivo.websocket.LinkStatusUpdate
import java.util.Date
import org.xivo.cti.message.IpbxCommandResponse

class ConfigManagerSpec extends TestKitSpec("ConfigManagerSpec") {

  class Helper {
    val testLink = TestProbe()
    val agentManager = TestProbe()
    val configDispatcher = TestProbe()
    val statusPublisher = TestProbe()
    val agentActionService = TestProbe()


    trait TestActorFactory extends ActorFactory {
      override def getCtiLink(username: String) = testLink.ref
      override def getAgentActionService(link: ActorRef) = agentActionService.ref
      override def getAgentManager = agentManager.ref
      override val statusPublishURI = (statusPublisher.ref.path).toString
      override val configDispatcherURI = (configDispatcher.ref.path).toString
    }

    def actor(user: XucUser, create: IpbxCommandResponse=>AgentLoginError = AgentError.create) = {
      val a = TestActorRef[ConfigManager](Props(new ConfigManager(user, agentManager.ref, create) with TestActorFactory))
      (a, a.underlyingActor)
    }
  }

  "Config manager" should {
    "send start message to ctilink on link status up" in new Helper {
      val user = XucUser("configManagerStart", "4etr,wqd")
      val (ref, configManager) = actor(user)

      ref ! LinkStatusUpdate(up)
      testLink.expectMsg(Start(user))

    }
    "on LoggedOn send init config to configDispatcher" in new Helper {
      val user = XucUser("configManagerTest", "4etr,wqd")
      val (ref, configManager) = actor(user)

      ref ! LoggedOn(user, "4", null)

      configDispatcher.expectMsg(InitConfig(testLink.ref))
    }

    "Send agentstate from agentStatus message to agent manager" in new Helper {
      val (ref, configManager) = actor(XucUser("configManagerTest", "4etr,wqd"))
      val agentStatus = new AgentStatusUpdate(23, new AgentStatus("2547", Availability.AVAILABLE, StatusReason.NONE))

      ref ! agentStatus

      agentManager.expectMsg(AgentState.fromAgentStatusUpdate(agentStatus))

    }
    "forward CtiMessage to configDispatcher" in new Helper {
      val (ref, configManager) = actor(XucUser("configManagerTest", "4etr,wqd"))
      val ctiMessage = new PhoneStatusUpdate()
      ref ! ctiMessage

      configDispatcher.expectMsg(ctiMessage)
    }

    "send user phone status to publisher" in new Helper {
      val (ref, configManager) = actor(XucUser("statusPublisher", "4etr,wqd"))

      val phoneStatus = UserPhoneStatus("user", PhoneHintStatus.getHintStatus(0))

      ref ! phoneStatus

      statusPublisher.expectMsg(phoneStatus)
    }
    "send user phone status refresh request to config dispatcher" in new Helper {
      val (ref, configManager) = actor(XucUser("userphoneStatusRefresh", "4etr,wqd"))

      ref ! PublishUserPhoneStatuses

      configDispatcher.expectMsg(PublishUserPhoneStatuses)

    }
    "send agent user status to agent manager" in new Helper {
      val (ref, _) = actor(XucUser("agentuserstatus", "4etr,wqd"))
      val agus = AgentUserStatus(367, "coffee")
      ref ! agus

      agentManager.expectMsg(agus)
    }
    "forward base requests to agent action service" in new Helper {
      val (ref, _) = actor(XucUser("agentLogout", "4rrrre"))

      ref ! BaseRequest(self, AgentLogout("1011"))

      agentActionService.expectMsg(BaseRequest(self, AgentLogout("1011")))
    }
    "transform IpbxCommandResponse to AgentError and send it to agentActionService" in new Helper {
      def create(s: IpbxCommandResponse): AgentLoginError = { AgentLoginError("testCause")}
      val (ref, _) = actor(XucUser("agentLoginErro", "feerre"), create)

      ref ! new IpbxCommandResponse("testCause", new Date())

      agentActionService.expectMsg(AgentLoginError("testCause"))

    }

  }
}
