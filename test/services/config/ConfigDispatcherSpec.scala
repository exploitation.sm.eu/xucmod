package services.config

import akka.testkit.{TestActorRef, TestProbe}
import akkatest.TestKitSpec
import models.{QueueCallList, XivoUserDaoWs, RichDirectoryResult}
import org.joda.time.DateTime
import org.json.JSONObject
import org.mockito.Mockito.{never, stub, verify, when}
import org.scalatest.mock.MockitoSugar
import org.xivo.cti.MessageFactory
import org.xivo.cti.message._
import org.xivo.cti.model._
import services.XucEventBus.XucEvent
import services.XucStatsEventBus.{Stat, StatUpdate}
import services.agent.{StatPeriod, Statistic, AgentStatistic}
import services.config.ConfigDispatcher.ObjectType._
import services.config.ConfigDispatcher._
import services.config.ConfigManager.PublishUserPhoneStatuses
import services.{XucEventBus, XucStatsEventBus}
import xivo.events.AgentState.{AgentOnPause, AgentReady}
import xivo.models.XivoObject.{ObjectDefinition, ObjectType => StatObjectType}
import xivo.models._
import xivo.websocket.WebsocketEvent
import java.util.Date
import xivo.xucami.models.{LeaveQueue, QueueCall, EnterQueue}

import scala.collection.JavaConversions._
import scala.concurrent.duration.DurationInt

class ConfigDispatcherSpec extends TestKitSpec("CtiRouterSpec")
  with MockitoSugar {

  var configRepository: ConfigRepository = null
  var configDispatcherRef: TestActorRef[ConfigDispatcher] = null
  var configDispatcher: ConfigDispatcher = null
  var agentManager: TestProbe = null
  var lineManager: TestProbe = null

  override def beforeAll = {
    configRepository = mock[ConfigRepository]
    agentManager = TestProbe()
    lineManager = TestProbe()
    configDispatcherRef = TestActorRef(new ConfigDispatcher(configRepository, lineManager.ref, agentManager.ref) with ConfigInitializer with XivoUserDaoWs with AgentLoginStatusDaoDb)
    configDispatcher = configDispatcherRef.underlyingActor
  }

  class Helper {
    val configRepository = mock[ConfigRepository]
    val lineManager = TestProbe()
    val eventBus = mock[XucEventBus]
    val agentManager = TestProbe()
    val agentQueueMemberFactory = mock[AgentQueueMemberFactory]
    val messageFactory = new MessageFactory
    val myFactory = mock[MessageFactory]
    val agentGroupFactory = mock[AgentGroupFactory]
    val statsBus = mock[XucStatsEventBus]
    def actor = {
      val a = TestActorRef(
        new ConfigDispatcher(configRepository, lineManager.ref, agentManager.ref, eventBus, statsBus, agentQueueMemberFactory,
                             agentGroupFactory) with ConfigInitializer with XivoUserDaoWs with AgentLoginStatusDaoDb)
      (a, a.underlyingActor)
    }
  }

  "A config dispatcher actor" should {

    "subscribe to eventBus on start" in new Helper {
      val (ref, _) = actor

      verify(eventBus).subscribe(ref, XucEventBus.allAgentsEventsTopic)
      verify(eventBus).subscribe(ref, XucEventBus.allAgentsStatsTopic)
    }


    "send back userPhoneStatus on PublishUserPhoneStatuses request" in new Helper {
      val (ref, _) = actor

      val uph = UserPhoneStatus("user1", PhoneHintStatus.getHintStatus(Integer.valueOf(0)))
      stub(configRepository.getAllUserPhoneStatuses).toReturn(List(uph))

      ref ! PublishUserPhoneStatuses

      expectMsg(uph)
    }
    "send back AgentUserStatus on User status update" in new Helper {
      var (ref, _) = actor

      val userStatus = new UserStatusUpdate
      userStatus.setStatus("outToLunch")
      val agentUserStatus = AgentUserStatus(34, "outToLunch")
      stub(configRepository.getAgentUserStatus(userStatus)).toReturn(Some(agentUserStatus))

      ref ! userStatus

      expectMsg(agentUserStatus)
    }

    "send back queues on getconfig(queue) message received" in {
      val gcf = GetConfig("queue")
      val queueConfigRequest = new JSONObject()

      val qConfig1 = new QueueConfigUpdate
      val qConfig2 = new QueueConfigUpdate

      val requester = TestProbe()

      stub(configRepository.getQueues()).toReturn(List(qConfig1, qConfig2))

      configDispatcherRef ! RequestConfig(requester.ref, gcf)

      requester.expectMsgAllOf(qConfig1, qConfig2)
    }

    "send back agents on getconfig(agent) message received" in {
      val gcf = GetConfig("agent")
      val agentConfigRequest = new JSONObject()
      val ag1 = Agent(1, "John", "Doe", "2250", "default")
      val ag2 = Agent(1, "Bill", "Door", "2260", "sales")
      stub(configRepository.getAgents()).toReturn(List(ag1, ag2))

      val requester = TestProbe()

      configDispatcherRef ! RequestConfig(requester.ref, gcf)

      requester.expectMsgAllOf(ag1, ag2)
    }
    "send back queue members on getconfig(queuemember) message received" in {
      val gcf = GetConfig("queuemember")
      val aqmc = AgentQueueMember(55, 77, 12)
      stub(configRepository.getAgentQueueMembers()).toReturn(List(aqmc))

      val requester = TestProbe()

      configDispatcherRef ! RequestConfig(requester.ref, gcf)

      requester.expectMsgAllOf(aqmc)
    }

    "send list of queues on getList(queue) message received" in new Helper {
      val (ref, _) = actor
      val requester = TestProbe()
      val qConfig = new QueueConfigUpdate

      stub(configRepository.getQueues()).toReturn(List(qConfig))

      ref ! RequestConfig(requester.ref, GetList("queue"))

      requester.expectMsg(QueueList(List(qConfig)))

    }
    "send list of agents on getList(agent) message received" in new Helper {
      val (ref, _) = actor
      val requester = TestProbe()
      val agents = List(Agent(1, "John", "Doe", "2250", "default"))

      stub(configRepository.getAgents()).toReturn(agents)

      ref ! RequestConfig(requester.ref, GetList("agent"))

      requester.expectMsg(AgentList(agents))

    }
    "send list of agents on getAgents for groupId, queueId, penalty reveiced" in new Helper {
      val (ref, _) = actor
      val requester = TestProbe()
      val agents = List(Agent(1, "John", "Doe", "2250", "default"))
      val (groupId, queueId, penalty) = (5,25,6)

      stub(configRepository.getAgents(groupId, queueId, penalty)).toReturn(agents)

      ref ! RequestConfig(requester.ref, GetAgents(groupId, queueId, penalty))

      requester.expectMsg(AgentList(agents))

    }
    "send list of agent on get agents from a group not in queue received" in new Helper {
      val (ref, _) = actor
      val requester = TestProbe()
      val agents = List(Agent(7, "Bob", "Marley", "7540", "basic"))
      val (groupId, queueId, penalty) = (8,32,2)

      stub(configRepository.getAgentsNotInQueue(groupId, queueId)).toReturn(agents)

      ref ! RequestConfig(requester.ref, GetAgentsNotInQueue(groupId, queueId))

      requester.expectMsg(AgentList(agents))

    }
    "send list of queue members on getList(queuemember) message received" in new Helper {
      val (ref, _) = actor
      val requester = TestProbe()
      val queueMembers = List(AgentQueueMember(55, 77, 12))

      stub(configRepository.getAgentQueueMembers()).toReturn(queueMembers)

      ref ! RequestConfig(requester.ref, GetList("queuemember"))

      requester.expectMsg(AgentQueueMemberList(queueMembers))

    }
    "send back agent satus on requet status for agent" in new Helper {
      import services.config.ConfigDispatcher.ObjectType.TypeAgent
      import xivo.events.AgentState.AgentReady
      val (ref, _) = actor
      val requester = TestProbe()
      val rs = RequestStatus(requester.ref, 37, TypeAgent)
      val agentState = AgentReady(3, new DateTime(), "1001", List())
      stub(configRepository.getAgentState(37)).toReturn(Some(agentState))

      ref ! rs

      requester.expectMsg(agentState)
    }

    "send back user satus on requet status for user" in new Helper {
      import services.config.ConfigDispatcher.ObjectType.TypeUser
      val (ref, _) = actor
      val requester = TestProbe()
      val rs = RequestStatus(requester.ref, 59, TypeUser)

      val userStatusUpdate = new UserStatusUpdate

      stub(configRepository.getUserStatus(59)).toReturn(Some(userStatusUpdate))

      ref ! rs

      requester.expectMsg(userStatusUpdate)
    }
    "send back phone status on request status for phone" in new Helper {
      import services.config.ConfigDispatcher.ObjectType.TypePhone
      val (ref, _) = actor
      val requester = TestProbe()
      val rs = RequestStatus(requester.ref, 12, TypePhone)

      val phoneStatusUpdate = new PhoneStatusUpdate

      stub(configRepository.getPhoneStatus(12)).toReturn(Some(phoneStatusUpdate))

      ref ! rs

      requester.expectMsg(phoneStatusUpdate)
    }

    "update config repository agent status on agent state received" in new Helper {
      val (ref, _) = actor
      val agentState = AgentReady(34,new DateTime, "3200", List())
      ref ! agentState
      verify(configRepository).onAgentState(agentState)
    }

    "forward phone config message to the realtime config repository when received" in {
      val phoneConfigUpdate = new PhoneConfigUpdate

      configDispatcherRef ! phoneConfigUpdate

      verify(configRepository).onPhoneConfigUpdate(phoneConfigUpdate)
    }

    "send a CreateLine to the LineManager on phone config message" in {
      val phoneConfigUpdate = new PhoneConfigUpdate
      phoneConfigUpdate.setNumber("13")

      configDispatcherRef ! phoneConfigUpdate

      lineManager.expectMsg(CreateLine(13))
    }


    "forward phone status message to the realtime config repository when received" in {
      var phoneStatusUpdate = new PhoneStatusUpdate

      when(configRepository.getUserPhoneStatus(phoneStatusUpdate)).thenReturn(None)

      configDispatcherRef ! phoneStatusUpdate

      verify(configRepository).onPhoneStatusUpdate(phoneStatusUpdate)
    }

    "return a phone status message updated with username to sender" in {
      var phoneStatusUpdate = new PhoneStatusUpdate

      when(configRepository.getUserPhoneStatus(phoneStatusUpdate)).thenReturn(
        Some(
          UserPhoneStatus("user", PhoneHintStatus.getHintStatus(0))))

      configDispatcherRef ! phoneStatusUpdate

      expectMsgType[UserPhoneStatus]
    }

    "forward directory result to the config repository to be enriched and send the result back to the sender" in {

      val directoryResult = new DirectoryResult
      val returnedResult = new RichDirectoryResult(List())

      stub(configRepository.addStatuses(directoryResult)).toReturn(returnedResult)

      configDispatcherRef ! directoryResult

      verify(configRepository).addStatuses(directoryResult)

      expectMsg(WebsocketEvent.createEvent(returnedResult))
    }

    "Send back line configuration on line config request by number if exists" in {
      val lcr = LineConfigQueryByNb("9000")
      val lineConfig = LineConfig("32", "9000")

      val requester = TestProbe()

      stub(configRepository.getLineConfig(lcr)).toReturn(Some(lineConfig))

      configDispatcherRef ! RequestConfig(requester.ref, lcr)

      requester.expectMsg(lineConfig)
    }

    "Send back line configuration on line config request by id if exists" in {
      val lcr = LineConfigQueryById(55)
      val lineConfig = LineConfig("55", "9041")

      val requester = TestProbe()

      stub(configRepository.getLineConfig(lcr)).toReturn(Some(lineConfig))

      configDispatcherRef ! RequestConfig(requester.ref, lcr)

      requester.expectMsg(lineConfig)
    }

    "Do not send anything if line config doest not exists on request" in {
      val lcr = LineConfigQueryByNb("9000")

      val requester = TestProbe()

      stub(configRepository.getLineConfig(lcr)).toReturn(None)

      configDispatcherRef ! RequestConfig(requester.ref, lcr)

      requester.expectNoMsg(100 millis)

    }

    "send agent directory on request get agent directory" in {
      val requester = TestProbe()
      stub(configRepository.getAgentDirectory).toReturn(List())

      configDispatcherRef ! RequestConfig(requester.ref, GetAgentDirectory)

      verify(configRepository).getAgentDirectory

      requester.expectMsg(AgentDirectory(List()))
    }

    "send agent states on get agent states request" in {
      val agentStateA = AgentOnPause(54,null,"98978",List())

      val agentStateB = AgentReady(32,null,"9898",List())

      val requester = TestProbe()

      stub(configRepository.getAgentStates).toReturn(List(agentStateA, agentStateB))

      configDispatcherRef ! RequestConfig(requester.ref, GetAgentStates)

      verify(configRepository).getAgentStates

      requester.expectMsgAllOf(agentStateA,agentStateB)
    }

    "send a list of Meetme when GetList(meetme) message received" in {
      val meetmeA = new Meetme("test", "4000", true, new Date(), List[MeetmeMember]())
      val meetmeB = new Meetme("test2", "4002", true, new Date(), List[MeetmeMember]())
      val requester = TestProbe()
      stub(configRepository.getMeetmeList).toReturn(List(meetmeA, meetmeB))

      configDispatcherRef ! RequestConfig(requester.ref, GetList("meetme"))

      verify(configRepository).getMeetmeList
      requester.expectMsg(MeetmeList(List(meetmeA, meetmeB)))
    }

    "get agent config when create on agent" in {
      import services.config.ConfigDispatcher.ObjectType._
      import services.config.ConfigDispatcher._

      val msg = GetConfig("agent")
      msg should be(GetConfig(TypeAgent))
    }
    """set agent to queue on config change request
       and publish new queue member to bus
    """ in new Helper {
      import services.config.ConfigDispatcher.ObjectType.TypeQueueMember
      val (ref, configDispatcher) = actor

      stub(agentQueueMemberFactory.setAgentQueue(14,210,4)).toReturn(Some(AgentQueueMember(14,210,4)))

      ref ! ConfigChangeRequest(ref,SetAgentQueue(14,210,4))

      verify(agentQueueMemberFactory).setAgentQueue(14,210,4)

      verify(eventBus).publish(XucEvent(XucEventBus.configTopic(TypeQueueMember), AgentQueueMember(14,210,4)))

    }
    "remove agent from queue on config change request" in new Helper  {
      val (ref, configDispatcher) = actor

      ref ! ConfigChangeRequest(ref,RemoveAgentFromQueue(12,56))

      verify(agentQueueMemberFactory).removeAgentFromQueue(12,56)

    }
    "Get agent groups" in new Helper {
      val (ref, _) = actor
      var agentGroups = List(AgentGroup(Some(1),"group1"))
      val requester = TestProbe()

      stub(agentGroupFactory.all()).toReturn(agentGroups)

      ref ! RequestConfig(requester.ref, GetList("agentgroup"))

      requester.expectMsg(AgentGroupList(agentGroups))

    }
    "update received queue statistics and forward it to the aggregator" in new Helper {
      val (ref, _) = actor
      val msg = new QueueStatistics()
      msg.setQueueId(1)
      msg.addCounter(new Counter(StatName.EWT, 2))

      val expected = StatUpdate(ObjectDefinition(StatObjectType.Queue, Some(1)), List(Stat("EWT", 2)))

      ref ! msg

      verify(statsBus).publish(expected)
    }
    "on agentQueueMember if not exists in repo, update repo and publish to bus" in new Helper {
      import services.config.ConfigDispatcher.ObjectType.TypeQueueMember
      val (ref, _) = actor
      val agQueueMember = AgentQueueMember(1,2,3)

      stub(configRepository.queueMemberExists(agQueueMember)).toReturn((false))

      ref ! agQueueMember

      verify(configRepository).updateOrAddQueueMembers(agQueueMember)
      verify(eventBus).publish(XucEvent(XucEventBus.configTopic(TypeQueueMember), agQueueMember))

    }
    "do not publish or update queue member if already received" in new Helper {
      import services.config.ConfigDispatcher.ObjectType.TypeQueueMember
      val (ref, _) = actor
      val agQueueMember = AgentQueueMember(1,2,3)

      stub(configRepository.queueMemberExists(agQueueMember)).toReturn((true))

      ref ! agQueueMember

      verify(configRepository,never()).updateOrAddQueueMembers(agQueueMember)
      verify(eventBus,never()).publish(XucEvent(XucEventBus.configTopic(TypeQueueMember), agQueueMember))
    }

    "update config repository meetme list and publish on the bus on meetme list received" in new Helper {
      val (ref, _) = actor
      val meetmeA = new Meetme("test", "4000", true, new Date(), List[MeetmeMember]())
      val meetmeB = new Meetme("test2", "4002", true, new Date(), List[MeetmeMember]())

      ref ! new MeetmeUpdate(List(meetmeA, meetmeB))

      verify(configRepository).onMeetmeUpdate(List(meetmeA, meetmeB))
      verify(eventBus).publish(XucEvent(XucEventBus.configTopic(TypeMeetme), MeetmeList(List(meetmeA, meetmeB))))
    }

    "update config repository on agent statistics received" in new Helper {

      val (ref, _) = actor

      val agStat = AgentStatistic(34,List(Statistic("stat1", StatPeriod(1))))

      ref ! agStat

      verify(configRepository).updateAgentStatistic(agStat)

    }

    "send back statistics on request" in new Helper {
      val agStat1 = AgentStatistic(34,List(Statistic("stat1", StatPeriod(1))))
      val agStat2 = AgentStatistic(53,List(Statistic("stat2", StatPeriod(34))))

      val stats = List(agStat1, agStat2)
      stub(configRepository.getAgentStatistics).toReturn(stats)

      val (ref, _) = actor
      val requester= TestProbe()

      ref ! RequestConfig(requester.ref, GetAgentStatistics)

      requester.expectMsgAllOf(agStat1, agStat2)
    }

    "send back outbound queue number" in new Helper {
      val (ref, _) = actor
      val requester= TestProbe()
      val queueIds = List(2,7,9)
      stub(configRepository.getOutboundQueueNumber(queueIds)).toReturn(Some("3000"))

      ref ! RequestConfig(requester.ref, OutboundQueueNumberQuery(queueIds))

      requester.expectMsg(OutboundQueueNumber("3000"))
    }

    "add a queue call in repository and publish on the bus when EnterQueue event received" in new Helper {
      val (ref, _) = actor
      val queueCall = QueueCall(2, "bar", "3331545", new DateTime())
      val queueCalls = QueueCallList(12, List(queueCall))
      val enterQueue = EnterQueue("foo", "123456.789", queueCall)
      stub(configRepository.getQueueCalls("foo")).toReturn(Some(queueCalls))

      ref ! enterQueue

      verify(configRepository).onQueueCallReceived("foo", "123456.789", queueCall)
      verify(eventBus).publish(queueCalls)
    }

    "remove a queue call from the repository and publish on the bus when LeaveQueue event received" in new Helper {
      val (ref, _) = actor
      val queueCall = QueueCall(2, "bar", "3331545", new DateTime())
      val queueCalls = QueueCallList(12, List(queueCall))
      val leaveQueue = LeaveQueue("foo", "123456.789")
      stub(configRepository.getQueueCalls("foo")).toReturn(Some(queueCalls))

      ref ! leaveQueue

      verify(configRepository).onQueueCallFinished("foo", "123456.789")
      verify(eventBus).publish(queueCalls)
    }

    "send back queue calls" in new Helper {
      val (ref, _) = actor
      val requester= TestProbe()
      val queueId = 3
      val (c1, c2) = (QueueCall(1, "John Doe", "335687", new DateTime()), QueueCall(2, "Jack Smith", "4458796", new DateTime()))
      stub(configRepository.getQueueCalls(queueId)).toReturn(QueueCallList(15, List(c1, c2)))

      ref ! RequestConfig(requester.ref, GetQueueCalls(queueId))

      requester.expectMsg(QueueCallList(15, List(c1, c2)))
    }

    "reload the specified agent and publish it on the bus" in new Helper {
      val (ref, _) = actor
      val agentId = 74
      val agent = Agent(agentId, "John", "Doe", "1000", "default", 1)
      stub(configRepository.getAgent(agentId)).toReturn(Some(agent))

      ref ! RefreshAgent(agentId)

      verify(configRepository).loadAgent(agentId)
      verify(eventBus).publish(agent)
    }
  }
}