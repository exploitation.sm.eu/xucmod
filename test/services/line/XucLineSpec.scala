package services.line

import akka.testkit.TestActorRef
import akkatest.TestKitSpec
import org.mockito.Mockito._
import org.scalatest.mock.MockitoSugar
import services.XucEventBus
import xivo.ami.AmiBusConnector.{AgentListenNotification, AgentListenStarted, CallData, LineEvent}

class XucLineSpec extends TestKitSpec("LineTest")
with MockitoSugar {

  class Helper() {
    val eventBus = mock[XucEventBus]

    def actor(number: Int) = {
      val sa = TestActorRef(new XucLine(number, eventBus))
      (sa, sa.underlyingActor)
    }
  }


  "XucLine actor" should {
    "publish lineState on the XucEventBus on LineEvent" in new Helper() {
      val (ref, a) = actor(1100)
      val lineEvent = LineEvent(100, CallData("callId1", LineState.BUSY,"100"))
      ref ! lineEvent

      verify(eventBus).publish(lineEvent)
    }

    "publish agentListen  notification on lineEvent" in new Helper() {
      val (ref, a) = actor(1200)

      val agentListenNotif = mock[AgentListenNotification]

      ref ! agentListenNotif

      verify(eventBus).publish(agentListenNotif)

    }
  }
}
